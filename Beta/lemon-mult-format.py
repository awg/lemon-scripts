from pyspark import SparkContext, SparkConf, SQLContext, HiveContext
from pyspark.sql.types import *
import json
import time

"""App Name: Any string"""
APP_NAME = "Sample Lemon Application"

""" CPU_METRIC_IDs is list of metric_id to be aggregated. """
CPU_METRIC_IDs = ["9011", "9028"]

"""DATAFILE: Input file from hdfs"""
DATA_FILE = "/project/itmon/archive/lemon/bi/2016-03/"

"""RESULT_PATH: Output filename"""
RESULT_PATH = "result201603"

""" choose from 'hourly', 'daily' or 'weekly' """
INTERVAL = "hourly"

def main(sc):
    sqlCtxt = HiveContext(sc)
    
    fileschema = StructType([
        StructField("aggregated", StringType()),
        StructField("body", StringType()),
        StructField("entity", StringType()),
        StructField("metric_id", StringType()),
        StructField("metric_name", StringType()),
        StructField("producer", StringType()),
        StructField("submitter_environment", StringType()),
        StructField("submitter_host", StringType()),
        StructField("submitter_hostgroup", StringType()),
        StructField("timestamp", StringType()),
        StructField("toplevel_hostgroup", StringType()),
        StructField("type", StringType()),
        StructField("version", StringType())
    ])
    
    fulldata = sqlCtxt.read.json(DATA_FILE, fileschema)
    fulldata.registerTempTable("fulldata")
    

    def convertts(x):
        if INTERVAL == "weekly":
            #### Timestamp is getting truncated in the SQL to date. Other useful functions are, weekofyear(), 
            tsformat = "%Y-%m %U"
        elif INTERVAL == "hourly":
            ##########  date string + ',' + hour
            tsformat = "%Y-%m-%d %H"
        elif INTERVAL == "daily":
            ##########  date string
            tsformat = "%Y-%m-%d"
        else:
            tsformat = "%Y-%m-%d"
        tsformat += " +0000"
        return {'ts':time.strftime(tsformat, time.gmtime(x.ts)), 'body':x.body, 'entity':x.entity, 'metric_id':x.metric_id}
	   
    def singleValueReduce(x, y, avgIndex, sdIndex, countIndex):
        num = x[countIndex] + y[countIndex]
        avg = x[avgIndex] + (y[avgIndex] - x[avgIndex])/num
        sd = x[sdIndex] + (y[avgIndex] - x[avgIndex]) * (y[avgIndex] - avg)
        return (num, avg, sd)

    
    
    def reduceSumPerc(x,y):
        """
            RETURN VALUES
            index 0: PercIdle min_percidle
            index 1: PercIdle max_percidle
            index 2: PercIdle average
            index 3: PercIdle Varience total
            index 4: Total number of samples
            index 5: PercIdle min_perciowait
            index 6: PercIdle max_perciowait
            index 7: PercIOWait average
            index 8: PercIOWait Varience Total
        """
        num, percIdleAvg, percIdleSD = singleValueReduce(x, y, 2, 3, 4)
        num, percIOWaitAvg, percIOWaitSD = singleValueReduce(x, y, 7, 8, 4)
        #num = x[4] + y[4]
        #percIdleAvg = x[2] + (y[2] - x[2])/num
        #percIdleSD = x[3] + (y[2] - x[2]) * (y[2] - percIdleAvg)
        #percIOWaitAvg = x[7] + (y[7] - x[7])/num
        #percIOWaitSD = x[8] + (y[7] - x[7]) * (y[7] - percIOWaitAvg)
        return (min(x[0], y[0]), max(x[1], y[1]), percIdleAvg, percIdleSD,
                num,
                min(x[5], y[5]), max(x[6], y[6]), percIOWaitAvg, percIOWaitSD)
    
    def mapfunc(x):
        bodyjson = json.loads(x['body'])
        if x['metric_id'] == "9011":
            percIdlevalue = bodyjson["PercIdle"]
            percIOWaitvalue = bodyjson['PercIOWait']
            return ((x['entity'], x['ts'], x['metric_id'], '',),
                    ####  Min, Max, Average, Varience Total
                    ###   Algorithm being followed here is by Knuth in Art of Programming Vol. 2 
                    ###   (https://en.wikipedia.org/wiki/Algorithms_for_calculating_variance#Online_algorithm)
                    (percIdlevalue, percIdlevalue, percIdlevalue, 0,
                    1,
                    percIOWaitvalue, percIOWaitvalue, percIOWaitvalue, 0))
        elif x['metric_id'] == "9208":
            interfacename = bodyjson["InterfaceName"]
            readavg = bodyjson["NumKBReadAvg"]
            writeavg = bodyjson["NumKBWriteAvg"]
            return ((x['entity'], x['ts'], x['metric_id'], interfacename),
                    (readavg, readavg, readavg, 0,
                    1,
                    writeavg, writeavg, writeavg, 0))

    def map2func(x):
        return ((x[0][0], x[0][1]),(x[0][2], x[0][3]) + x[1])

    def reduce2func(x, y):
        if x[0] != CPU_METRIC_IDs[0]:
            (x, y) = (y, x)
        return x + y


    def flatten(x):
        result = []
        for i in x:
            for y in i:
                result.append(str(y))
        return result

   
    newcpulists = sqlCtxt.sql("""SELECT `timestamp`/1000 as ts, body, entity, metric_id 
            FROM fulldata 
            where metric_id='9011'
            """)

    reducedresult = (newcpulists.
                        map(convertts).
                        map(mapfunc).
                        reduceByKey(reduceSumPerc).
                        map(map2func).
                        reduceByKey(reduce2func).
                        map(flatten).
                        map(lambda x: ",".join(x))
                    )
    
    reducedresult.coalesce(1, True).saveAsTextFile(RESULT_PATH)
    
    #sample = reducedresult.take(10)
    #print sample
    
if __name__ == "__main__":
    conf = (SparkConf().setAppName(APP_NAME)
        .setMaster("yarn-client")
        #.set("spark.dynamicAllocation.enabled", "true").
        #.set("spark.dynamicAllocation.minExecutors", "1").
        #.set("spark.dynamicAllocation.maxExecutors", "400").
        #.set("spark.dynamicAllocation.initialExecutors", "1").
        #.set("spark.shuffle.service.enabled", "true")
        )
    # see http://spark.apache.org/docs/1.3.0/configuration.html#runtime-environment
    # --driver-class-path '/usr/lib/hive/lib/*' 
    # --driver-java-options '-Dspark.executor.extraClassPath=/usr/lib/hive/lib/*'
    sc = SparkContext()
    main(sc)

