import scala.util.parsing.json._
import org.apache.spark.sql.Row
import scala.math._
import org.apache.spark.sql.types._
import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf

val APP_NAME = "Sample Lemon Application"
val CPU_METRIC_ID = 9011
val DATA_FILE = "/project/itmon/archive/lemon/bi/2016-02"
val RESULT_PATH = "percIdleResult201602"

def lemon-scala(){
    def main(args: String){
        val sqlCtxt = new org.apache.spark.sql.hive.HiveContext(sc)

        val schema = StructType(Array(
                    StructField("aggregated",StringType,true),
                    StructField("body",StringType,true),
                    StructField("entity",StringType,true),
                    StructField("metric_id",StringType,true),
                    StructField("metric_name",StringType,true),
                    StructField("producer",StringType,true),
                    StructField("submitter_environment",StringType,true),
                    StructField("submitter_host",StringType,true),
                    StructField("submitter_hostgroup",StringType,true),
                    StructField("timestamp",StringType,true),
                    StructField("toplevel_hostgroup",StringType,true),
                    StructField("type",StringType,true),
                    StructField("version",StringType,true))
        )
        val fulldata = sqlCtxt.jsonFile(DATA_FILE, schema)
        fulldata.registerTempTable("fulldata")

        val newcpulists = sqlCtxt.sql("""SELECT to_date(cast(`timestamp`/1000 as timestamp)) as ts, body, metric_id, entity FROM fulldata where metric_id="9011" or metric_id="9208"""")
       
        def singleValueReduce(x:List[Double], y:List[Double], avgIndex: Integer, sdIndex: Integer, countIndex: Integer): Tuple3[Double, Double, Double] = {
            val num = x(countIndex) + y(countIndex)
            val avg = x(avgIndex) + (y(avgIndex) - x(avgIndex))/num
            val sd = x(sdIndex) + (y(avgIndex) - x(avgIndex)) * (y(avgIndex) - avg)
            return (num, avg, sd)
        }
        
        def reduceSumPerc(x:List[Double], y:List[Double]): List[Double] = {
            val (num1, percIdleAvg, percIdleSD) = singleValueReduce(x, y, 2, 3, 4)
            val (num2, percIOWaitAvg, percIOWaitSD) = singleValueReduce(x, y, 7, 8, 4)
            return List(min(x(0), y(0)), max(x(1), y(1)), percIdleAvg, percIdleSD,
                    num1,
                    min(x(5), y(5)), max(x(6), y(6)), percIOWaitAvg, percIOWaitSD)

        }

        def mapfunc(x:Row): List[List[Any]] = { ----------------------------------------->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            val json = JSON.parseFull(x.getString(1)).get.asInstanceOf[Map[String, String]]
            if (x(2) == "9011"){
                val percIdle = json("PercIdle").asInstanceOf[Double]
                val percIOWait = json("PercIOWait").asInstanceOf[Double]
                return List(List(x(2), x(0), x(3), ""),
                        List(percIdle, percIdle, percIdle, 0,
                        1,
                        percIOWait, percIOWait, percIOWait, 0))
            }else if (x(2) == "9208"){
                val interfacename = json("InterfaceName")
                val readavg = json("NumKBReadAvg").asInstanceOf[Double]
                val writeavg = json("NumKBWriteAvg").asInstanceOf[Double]
                return List(List(x(2), x(0), x(3), interfacename),
                        List(readavg, readavg, readavg, 0,
                        1,
                        writeavg, writeavg, writeavg, 0))
            }
            return List(List(""))
        }

        def map2func(x: List[]): List[] = {
            return List(List(x[0][0], x[0][1]),List(x[0][2], x[0][3]) + x[1])
        }

        def reduce2func(x: List[], y:List[]): List[] = {
            return List(x, y)
        }
        
        reducedresult = (newcpulists.
                map(mapfunc).
                reduceByKey(reduceSumPerc).
                map(map2func).
                reduceByKey(reduce2func).
                map(lambda first: ','.join((','.join(map(str, first[0])),
                    ','.join(map(str, first[1]))))
                )
        )
    
        reducedresult.coalesce(1, True).saveAsTextFile(RESULT_PATH)
    }
}
