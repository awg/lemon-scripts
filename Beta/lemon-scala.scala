import scala.util.parsing.json._
import org.apache.spark.sql.Row
import scala.math._
import org.apache.spark.sql.types._
import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf

val APP_NAME = "Sample Lemon Application"
val CPU_METRIC_ID = 9011
val DATA_FILE = "/project/itmon/archive/lemon/bi/2016-02"
val RESULT_PATH = "percIdleResult201602"

def lemon-scala(){
    def main(args: String){
        val sqlCtxt = new org.apache.spark.sql.SQLContext(sc)

        val schema = StructType(Array(
                    StructField("aggregated",StringType,true),
                    StructField("body",StringType,true),
                    StructField("entity",StringType,true),
                    StructField("metric_id",StringType,true),
                    StructField("metric_name",StringType,true),
                    StructField("producer",StringType,true),
                    StructField("submitter_environment",StringType,true),
                    StructField("submitter_host",StringType,true),
                    StructField("submitter_hostgroup",StringType,true),
                    StructField("timestamp",StringType,true),
                    StructField("toplevel_hostgroup",StringType,true),
                    StructField("type",StringType,true),
                    StructField("version",StringType,true))
        )
        val fulldata = sqlCtxt.jsonFile(DATA_FILE, schema)
        fulldata.registerTempTable("fulldata")

        val newcpulists = sqlCtxt.sql("""select entity, body FROM fulldata where metric_id="9011"""")
        def mapfunc(x:Row):Tuple2[String, List[Double]] = {
            val json = JSON.parseFull(x.getString(1)).get.asInstanceOf[Map[String, Double]]
            val percIdle = json("PercIdle")
            val percIO = json("PercIOWait")
            return Tuple2(x.getString(0), List(percIdle, percIdle, percIdle, 0, 
                    1, 
                    percIO, percIO, percIO, 0))
        }
        
        def reducefunc(x:List[Double], y:List[Double]):List[Double] = {
            val xlist = x.asInstanceOf[List[Double]]
            val ylist = y.asInstanceOf[List[Double]]
            val num = xlist(4) + ylist(4)
            val percIdleAvg = xlist(2) + (ylist(2) - xlist(2))/num
            val percIdleSD = xlist(3) + (y(2) - x(2)) * (ylist(2) - percIdleAvg)

            val percIOWaitAvg = xlist(7) + (ylist(7) - xlist(7))/num
            val percIOWaitSD = xlist(8) + (ylist(7) - xlist(7)) * (ylist(7) - percIOWaitAvg)

            return List(min(xlist(0), ylist(0)), max(xlist(1), ylist(1)), percIdleAvg, percIdleSD, num, min(xlist(5),ylist(5)), max(xlist(6), ylist(6)), percIOWaitAvg, percIOWaitSD)
        }

        val tt = newcpulists.map(mapfunc).reduceByKey(reducefunc)

        tt.saveAsTextFile(RESULT_PATH)
