## Snapshot for ddmendpoint

```python
dfagis = sqlContext.read.json("/project/monitoring/archive/agis/raw/ddmendpoint/2016/09/*")
dfagis.select("data.experiment_site","data.federation","data.is_tape","data.name","data.se","data.site_state","data.state","data.token","metadata.producer","metadata.topic","metadata.type","metadata.type_prefix").show(100)

```

### Example output


```text
+--------------------+----------+-------+--------------------+--------------------+----------+--------+--------------------+--------+--------------------+-----------+-----------+
|     experiment_site|federation|is_tape|                name|                  se|site_state|   state|               token|producer|               topic|       type|type_prefix|
+--------------------+----------+-------+--------------------+--------------------+----------+--------+--------------------+--------+--------------------+-----------+-----------+
|               AGLT2|  US-AGLT2|  false|     AGLT2_CALIBDISK|srm://head01.aglt...|    ACTIVE|  ACTIVE|      ATLASCALIBDISK|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|               AGLT2|  US-AGLT2|  false|      AGLT2_DATADISK|srm://head01.aglt...|    ACTIVE|  ACTIVE|       ATLASDATADISK|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|               AGLT2|  US-AGLT2|  false|       AGLT2_HOTDISK|srm://head01.aglt...|    ACTIVE|DISABLED|        ATLASHOTDISK|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|               AGLT2|  US-AGLT2|  false|AGLT2_LOCALGROUPDISK|srm://head01.aglt...|    ACTIVE|  ACTIVE| ATLASLOCALGROUPDISK|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|               AGLT2|  US-AGLT2|  false|    AGLT2_PERF-MUONS|srm://head01.aglt...|    ACTIVE|  ACTIVE|      ATLASGROUPDISK|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|               AGLT2|  US-AGLT2|  false|    AGLT2_PHYS-HIGGS|srm://head01.aglt...|    ACTIVE|  ACTIVE|      ATLASGROUPDISK|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|               AGLT2|  US-AGLT2|  false|       AGLT2_PHYS-SM|srm://head01.aglt...|    ACTIVE|  ACTIVE|      ATLASGROUPDISK|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|               AGLT2|  US-AGLT2|  false|      AGLT2_PRODDISK|srm://head01.aglt...|    ACTIVE|DISABLED|       ATLASPRODDISK|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|               AGLT2|  US-AGLT2|  false|   AGLT2_SCRATCHDISK|srm://head01.aglt...|    ACTIVE|  ACTIVE|    ATLASSCRATCHDISK|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|               AGLT2|  US-AGLT2|  false|     AGLT2_SUPERDISK|gsiftp://dcdmsu02...|    ACTIVE|  ACTIVE|    ATLASSCRATCHDISK|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|               AGLT2|  US-AGLT2|  false|      AGLT2_USERDISK|srm://head01.aglt...|    ACTIVE|  ACTIVE|       ATLASUSERDISK|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|        AM-04-YERPHI| NL-Tier3s|  false|AM-04-YERPHI_LOCA...|srm://se.yerphi-c...|    ACTIVE|  ACTIVE| ATLASLOCALGROUPDISK|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|        AM-04-YERPHI| NL-Tier3s|  false|AM-04-YERPHI_PROD...|srm://se.yerphi-c...|    ACTIVE|  ACTIVE|       ATLASPRODDISK|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|        AM-04-YERPHI| NL-Tier3s|  false|AM-04-YERPHI_SCRA...|srm://se.yerphi-c...|    ACTIVE|  ACTIVE|    ATLASSCRATCHDISK|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|           BNL-ATLAS| US-T1-BNL|  false|         AMAZON-0_ES|s3://s3.amazonaws...|    ACTIVE|  ACTIVE|        eventservice|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|           BNL-ATLAS| US-T1-BNL|  false|       AMAZON-0_LOGS|s3://s3.amazonaws...|    ACTIVE|  ACTIVE|                logs|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|           BNL-ATLAS| US-T1-BNL|  false|         AMAZON-1_ES|s3://s3-us-west-1...|    ACTIVE|  ACTIVE|        eventservice|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|           BNL-ATLAS| US-T1-BNL|  false|       AMAZON-1_LOGS|s3://s3-us-west-1...|    ACTIVE|  ACTIVE|                logs|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|           BNL-ATLAS| US-T1-BNL|  false|         AMAZON-2_ES|s3://s3-us-west-2...|    ACTIVE|  ACTIVE|        eventservice|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|           BNL-ATLAS| US-T1-BNL|  false|       AMAZON-2_LOGS|s3://s3-us-west-2...|    ACTIVE|  ACTIVE|                logs|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|              ANLASC| US-Tier3s|  false|  ANL-ATLAS-GRIDFTP1|gsiftp://atlasgri...|    ACTIVE|  ACTIVE| ATLASLOCALGROUPDISK|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|              ANLASC| US-Tier3s|  false|ANL-ATLAS_LOCALGR...|gsiftp://atlasgri...|    ACTIVE|  ACTIVE| ATLASLOCALGROUPDISK|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|              ANLASC| US-Tier3s|  false|ANLASC2_LOCALGROU...|gsiftp://rucio01....|    ACTIVE|  ACTIVE| ATLASLOCALGROUPDISK|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|              ANLASC| US-Tier3s|  false| ANLASC2_SCRATCHDISK|gsiftp://rucio01....|    ACTIVE|  ACTIVE|    ATLASSCRATCHDISK|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|              ANLASC| US-Tier3s|  false|     ANLASC_DATADISK|srm://atlasgridft...|    ACTIVE|  ACTIVE|       ATLASDATADISK|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|              ANLASC| US-Tier3s|  false|      ANLASC_HOTDISK|srm://atlasgridft...|    ACTIVE|DISABLED|        ATLASHOTDISK|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|              ANLASC| US-Tier3s|  false|     ANLASC_PRODDISK|srm://atlasgridft...|    ACTIVE|DISABLED|       ATLASPRODDISK|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|              ANLASC| US-Tier3s|  false|  ANLASC_SCRATCHDISK|srm://atlasgridft...|    ACTIVE|  ACTIVE|    ATLASSCRATCHDISK|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|              ANLASC| US-Tier3s|  false|     ANLASC_USERDISK|srm://atlasgridft...|    ACTIVE|  ACTIVE|       ATLASUSERDISK|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|     Australia-ATLAS|  AU-ATLAS|  false|AUSTRALIA-ATLAS_D...|srm://agh3.atlas....|    ACTIVE|  ACTIVE|       ATLASDATADISK|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|     Australia-ATLAS|  AU-ATLAS|  false|AUSTRALIA-ATLAS_H...|srm://agh3.atlas....|    ACTIVE|DISABLED|        ATLASHOTDISK|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|     Australia-ATLAS|  AU-ATLAS|  false|AUSTRALIA-ATLAS_L...|srm://agh3.atlas....|    ACTIVE|  ACTIVE| ATLASLOCALGROUPDISK|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|     Australia-ATLAS|  AU-ATLAS|  false|AUSTRALIA-ATLAS_P...|srm://agh3.atlas....|    ACTIVE|  ACTIVE|       ATLASDATADISK|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|     Australia-ATLAS|  AU-ATLAS|  false|AUSTRALIA-ATLAS_P...|srm://agh3.atlas....|    ACTIVE|DISABLED|       ATLASPRODDISK|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|     Australia-ATLAS|  AU-ATLAS|  false|AUSTRALIA-ATLAS_S...|srm://agh3.atlas....|    ACTIVE|  ACTIVE|    ATLASSCRATCHDISK|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|     Australia-ATLAS|  AU-ATLAS|  false|AUSTRALIA-ATLAS_S...|srm://agh3.atlas....|    ACTIVE|DISABLED|      ATLASGROUPDISK|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|     Australia-ATLAS|  AU-ATLAS|  false|AUSTRALIA-ATLAS_T...|srm://rcsrm.atlas...|    ACTIVE|  ACTIVE|T2ATLASLOCALGROUP...|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|        BEIJING-LCG2|   CN-IHEP|  false|BEIJING-LCG2_DATA...|srm://ccsrm.ihep....|    ACTIVE|  ACTIVE|       ATLASDATADISK|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|        BEIJING-LCG2|   CN-IHEP|  false|BEIJING-LCG2_HOTDISK|srm://ccsrm.ihep....|    ACTIVE|DISABLED|        ATLASHOTDISK|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|        BEIJING-LCG2|   CN-IHEP|  false|BEIJING-LCG2_LOCA...|srm://ccsrm.ihep....|    ACTIVE|  ACTIVE| ATLASLOCALGROUPDISK|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|        BEIJING-LCG2|   CN-IHEP|  false|BEIJING-LCG2_PERF...|srm://ccsrm.ihep....|    ACTIVE|DISABLED|       ATLASDATADISK|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|        BEIJING-LCG2|   CN-IHEP|  false|BEIJING-LCG2_PROD...|srm://ccsrm.ihep....|    ACTIVE|DISABLED|       ATLASPRODDISK|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|        BEIJING-LCG2|   CN-IHEP|  false|BEIJING-LCG2_SCRA...|srm://ccsrm.ihep....|    ACTIVE|  ACTIVE|    ATLASSCRATCHDISK|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|        BEIJING-LCG2|   CN-IHEP|  false|BEIJING-LCG2_SOFT...|srm://ccsrm.ihep....|    ACTIVE|DISABLED|      ATLASGROUPDISK|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
| BELLARMINE-ATLAS-T3| US-Tier3s|  false|BELLARMINE-ATLAS-...|gsiftp://atlasgri...|    ACTIVE|DISABLED| ATLASLOCALGROUPDISK|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
| BELLARMINE-ATLAS-T3| US-Tier3s|  false|BELLARMINE-T3_DAT...|srm://tier3-atlas...|    ACTIVE|DISABLED|       ATLASDATADISK|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
| BELLARMINE-ATLAS-T3| US-Tier3s|  false|BELLARMINE-T3_LOC...|srm://tier3-atlas...|    ACTIVE|DISABLED| ATLASLOCALGROUPDISK|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
| BELLARMINE-ATLAS-T3| US-Tier3s|  false|BELLARMINE-T3_PRO...|srm://tier3-atlas...|    ACTIVE|DISABLED|       ATLASPRODDISK|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
| BELLARMINE-ATLAS-T3| US-Tier3s|  false|BELLARMINE-T3_SCR...|srm://tier3-atlas...|    ACTIVE|DISABLED|    ATLASSCRATCHDISK|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
| BELLARMINE-ATLAS-T3| US-Tier3s|  false|BELLARMINE-T3_USE...|srm://tier3-atlas...|    ACTIVE|DISABLED|       ATLASUSERDISK|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|           BNL-ATLAS| US-T1-BNL|  false|        BNL-ATLAS_ES|s3://cephgw.usatl...|    ACTIVE|  ACTIVE|        eventservice|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|           BNL-ATLAS| US-T1-BNL|  false|    BNL-ATLAS_ES_NEW|s3://cephgw-test....|    ACTIVE|  ACTIVE|        eventservice|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|           BNL-ATLAS| US-T1-BNL|  false|      BNL-ATLAS_LOGS|s3://cephgw.usatl...|    ACTIVE|  ACTIVE|                logs|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|           BNL-ATLAS| US-T1-BNL|  false|  BNL-ATLAS_LOGS_NEW|s3://cephgw-test....|    ACTIVE|  ACTIVE|                logs|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|           BNL-ATLAS| US-T1-BNL|  false|  BNL-ATLAS_LOGS_old|s3://cephgw.usatl...|    ACTIVE|DISABLED|           ATLASOSES|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|         BNL-AWSEAST| US-T1-BNL|  false|BNL-AWSEAST_DATADISK|srm://bestse01.us...|    ACTIVE|DISABLED|       ATLASDATADISK|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|         BNL-AWSEAST| US-T1-BNL|  false|BNL-AWSEAST_PRODDISK|srm://bestse01.us...|    ACTIVE|DISABLED|       ATLASPRODDISK|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|         BNL-AWSEAST| US-T1-BNL|  false|BNL-AWSEAST_USERDISK|srm://bestse01.us...|    ACTIVE|DISABLED|       ATLASUSERDISK|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|        BNL-AWSWEST2| US-T1-BNL|  false|BNL-AWSWEST2_DATA...|srm://bestse03.us...|    ACTIVE|DISABLED|       ATLASDATADISK|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|         BNL-AWSWEST| US-T1-BNL|  false|BNL-AWSWEST_DATADISK|srm://bestse02.us...|    ACTIVE|DISABLED|       ATLASDATADISK|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|           BNL-ATLAS| US-T1-BNL|  false|   BNL-OSG2_DATADISK|srm://dcsrm.usatl...|    ACTIVE|  ACTIVE|       ATLASDATADISK|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|           BNL-ATLAS| US-T1-BNL|   true|   BNL-OSG2_DATATAPE|srm://dcsrm.usatl...|    ACTIVE|  ACTIVE|       ATLASDATATAPE|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|           BNL-ATLAS| US-T1-BNL|  false|    BNL-OSG2_DDMTEST|srm://dcsrm.usatl...|    ACTIVE|DISABLED|    ATLASSCRATCHDISK|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|           BNL-ATLAS| US-T1-BNL|  false|   BNL-OSG2_DET-SLHC|srm://dcsrm.usatl...|    ACTIVE|  ACTIVE|      ATLASGROUPDISK|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|           BNL-ATLAS| US-T1-BNL|   true|  BNL-OSG2_GROUPTAPE|srm://dcsrm.usatl...|    ACTIVE|  ACTIVE|         ATLASMCTAPE|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|           BNL-ATLAS| US-T1-BNL|  false|    BNL-OSG2_HOTDISK|srm://dcsrm.usatl...|    ACTIVE|DISABLED|        ATLASHOTDISK|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|           BNL-ATLAS| US-T1-BNL|  false|BNL-OSG2_LOCALGRO...|srm://dcsrm.usatl...|    ACTIVE|  ACTIVE| ATLASLOCALGROUPDISK|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|           BNL-ATLAS| US-T1-BNL|   true|     BNL-OSG2_MCTAPE|srm://dcsrm.usatl...|    ACTIVE|  ACTIVE|         ATLASMCTAPE|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|           BNL-ATLAS| US-T1-BNL|  false|BNL-OSG2_PERF-EGAMMA|srm://dcsrm.usatl...|    ACTIVE|  ACTIVE|      ATLASGROUPDISK|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|           BNL-ATLAS| US-T1-BNL|  false|BNL-OSG2_PERF-FLA...|srm://dcsrm.usatl...|    ACTIVE|  ACTIVE|      ATLASGROUPDISK|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|           BNL-ATLAS| US-T1-BNL|  false|  BNL-OSG2_PERF-JETS|srm://dcsrm.usatl...|    ACTIVE|  ACTIVE|      ATLASGROUPDISK|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|           BNL-ATLAS| US-T1-BNL|  false| BNL-OSG2_PERF-MUONS|srm://dcsrm.usatl...|    ACTIVE|  ACTIVE|      ATLASGROUPDISK|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|           BNL-ATLAS| US-T1-BNL|  false|    BNL-OSG2_PHYS-HI|srm://dcsrm.usatl...|    ACTIVE|  ACTIVE|      ATLASGROUPDISK|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|           BNL-ATLAS| US-T1-BNL|  false|    BNL-OSG2_PHYS-SM|srm://dcsrm.usatl...|    ACTIVE|  ACTIVE|      ATLASGROUPDISK|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|           BNL-ATLAS| US-T1-BNL|  false|   BNL-OSG2_PHYS-TOP|srm://dcsrm.usatl...|    ACTIVE|  ACTIVE|      ATLASGROUPDISK|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|           BNL-ATLAS| US-T1-BNL|  false|   BNL-OSG2_PRODDISK|srm://dcsrm.usatl...|    ACTIVE|DISABLED|       ATLASPRODDISK|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|           BNL-ATLAS| US-T1-BNL|  false|BNL-OSG2_SCRATCHDISK|srm://dcsrm.usatl...|    ACTIVE|  ACTIVE|    ATLASSCRATCHDISK|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|           BNL-ATLAS| US-T1-BNL|  false|   BNL-OSG2_TRIG-DAQ|srm://dcsrm.usatl...|    ACTIVE|  ACTIVE|      ATLASGROUPDISK|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|           BNL-ATLAS| US-T1-BNL|  false|   BNL-OSG2_USERDISK|srm://dcsrm.usatl...|    ACTIVE|  ACTIVE|       ATLASUSERDISK|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|CA-ALBERTA-WESTGR...|CA-WEST-T2|  false|CA-ALBERTA-WESTGR...|srm://sehn02.atla...|  DISABLED|DISABLED|       ATLASDATADISK|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|CA-ALBERTA-WESTGR...|CA-WEST-T2|  false|CA-ALBERTA-WESTGR...|srm://sehn02.atla...|  DISABLED|DISABLED|        ATLASHOTDISK|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|CA-ALBERTA-WESTGR...|CA-WEST-T2|  false|CA-ALBERTA-WESTGR...|srm://sehn02.atla...|  DISABLED|DISABLED| ATLASLOCALGROUPDISK|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|CA-ALBERTA-WESTGR...|CA-WEST-T2|  false|CA-ALBERTA-WESTGR...|srm://sehn02.atla...|  DISABLED|DISABLED|       ATLASPRODDISK|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|CA-ALBERTA-WESTGR...|CA-WEST-T2|  false|CA-ALBERTA-WESTGR...|srm://sehn02.atla...|  DISABLED|DISABLED|    ATLASSCRATCHDISK|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
| CA-MCGILL-CLUMEQ-T2|CA-EAST-T2|  false|CA-MCGILL-CLUMEQ-...|srm://storm02.clu...|    ACTIVE|  ACTIVE|       ATLASDATADISK|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
| CA-MCGILL-CLUMEQ-T2|CA-EAST-T2|  false|CA-MCGILL-CLUMEQ-...|srm://storm02.clu...|    ACTIVE|DISABLED|        ATLASHOTDISK|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
| CA-MCGILL-CLUMEQ-T2|CA-EAST-T2|  false|CA-MCGILL-CLUMEQ-...|srm://storm02.clu...|    ACTIVE|  ACTIVE| ATLASLOCALGROUPDISK|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
| CA-MCGILL-CLUMEQ-T2|CA-EAST-T2|  false|CA-MCGILL-CLUMEQ-...|srm://storm02.clu...|    ACTIVE|  ACTIVE|      ATLASGROUPDISK|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
| CA-MCGILL-CLUMEQ-T2|CA-EAST-T2|  false|CA-MCGILL-CLUMEQ-...|srm://storm02.clu...|    ACTIVE|DISABLED|       ATLASPRODDISK|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
| CA-MCGILL-CLUMEQ-T2|CA-EAST-T2|  false|CA-MCGILL-CLUMEQ-...|srm://storm02.clu...|    ACTIVE|  ACTIVE|    ATLASSCRATCHDISK|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|        CA-SCINET-T2|CA-EAST-T2|  false|CA-SCINET-T2_DATA...|srm://lcg-se1.sci...|    ACTIVE|  ACTIVE|       ATLASDATADISK|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|        CA-SCINET-T2|CA-EAST-T2|  false|CA-SCINET-T2_HOTDISK|srm://lcg-se1.sci...|    ACTIVE|DISABLED|        ATLASHOTDISK|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|        CA-SCINET-T2|CA-EAST-T2|  false|CA-SCINET-T2_LOCA...|srm://lcg-se1.sci...|    ACTIVE|  ACTIVE| ATLASLOCALGROUPDISK|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|        CA-SCINET-T2|CA-EAST-T2|  false|CA-SCINET-T2_PHYS...|srm://lcg-se1.sci...|    ACTIVE|  ACTIVE|       ATLASDATADISK|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|        CA-SCINET-T2|CA-EAST-T2|  false|CA-SCINET-T2_PROD...|srm://lcg-se1.sci...|    ACTIVE|DISABLED|       ATLASPRODDISK|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|        CA-SCINET-T2|CA-EAST-T2|  false|CA-SCINET-T2_SCRA...|srm://lcg-se1.sci...|    ACTIVE|  ACTIVE|    ATLASSCRATCHDISK|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|        CA-SCINET-T2|CA-EAST-T2|  false|CA-SCINET-T2_SOFT...|srm://lcg-se1.sci...|    ACTIVE|DISABLED|      ATLASGROUPDISK|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|CA-VICTORIA-WESTG...|CA-WEST-T2|  false|CA-VICTORIA-WESTG...|srm://charon01.we...|    ACTIVE|  ACTIVE|       ATLASDATADISK|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|CA-VICTORIA-WESTG...|CA-WEST-T2|  false|CA-VICTORIA-WESTG...|srm://charon01.we...|    ACTIVE|DISABLED|        ATLASHOTDISK|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
|CA-VICTORIA-WESTG...|CA-WEST-T2|  false|CA-VICTORIA-WESTG...|srm://charon01.we...|    ACTIVE|  ACTIVE| ATLASLOCALGROUPDISK|    agis|agis_raw_ddmendpoint|ddmendpoint|        raw|
+--------------------+----------+-------+--------------------+--------------------+----------+--------+--------------------+--------+--------------------+-----------+-----------+

```



## Snapshot for pandaqueue

```python

df.select("data.experiment_site","data.pattern","data.resource_type","metadata.event_timestamp","metadata.hostname","metadata.producer","metadata.timestamp","metadata.topic","metadata.type","metadata.type_prefix","metadata.version").show(100)

```

### Example output

```text

+---------------+--------------------+-------------+---------------+--------------------+--------+-------------+-------------------+----------+-----------+-------+
|experiment_site|             pattern|resource_type|event_timestamp|            hostname|producer|    timestamp|              topic|      type|type_prefix|version|
+---------------+--------------------+-------------+---------------+--------------------+--------+-------------+-------------------+----------+-----------+-------+
|          AGLT2|          AGLT2_LMEM|         GRID|  1472683019098|monit-httpsource-...|    agis|1472683019098|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|          AGLT2|AGLT2-CE-gate04.a...|         GRID|  1472683019100|monit-httpsource-...|    agis|1472683019100|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|          AGLT2|   AGLT2_LMEM-condor|         GRID|  1472683019100|monit-httpsource-...|    agis|1472683019100|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|          AGLT2|    gate04.aglt2.org|         GRID|  1472683019100|monit-httpsource-...|    agis|1472683019100|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|          AGLT2|         AGLT2_MCORE|         GRID|  1472683019100|monit-httpsource-...|    agis|1472683019100|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|          AGLT2|  AGLT2_MCORE-condor|         GRID|  1472683019100|monit-httpsource-...|    agis|1472683019100|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|          AGLT2|AGLT2-CE-gate04.a...|         GRID|  1472683019100|monit-httpsource-...|    agis|1472683019100|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|          AGLT2|    gate04.aglt2.org|         GRID|  1472683019100|monit-httpsource-...|    agis|1472683019100|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|          AGLT2|    AGLT2_SL6-condor|         GRID|  1472683019100|monit-httpsource-...|    agis|1472683019100|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|          AGLT2|AGLT2-CE-gate04.a...|         GRID|  1472683019100|monit-httpsource-...|    agis|1472683019100|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|          AGLT2|    gate04.aglt2.org|         GRID|  1472683019100|monit-httpsource-...|    agis|1472683019100|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|          AGLT2|           AGLT2_SL6|         GRID|  1472683019100|monit-httpsource-...|    agis|1472683019100|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|          AGLT2|   AGLT2_TEST-condor|         GRID|  1472683019100|monit-httpsource-...|    agis|1472683019100|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|          AGLT2|    gate03.aglt2.org|         GRID|  1472683019100|monit-httpsource-...|    agis|1472683019100|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|          AGLT2|AGLT2-CE-gate03.a...|         GRID|  1472683019100|monit-httpsource-...|    agis|1472683019100|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|          AGLT2|          AGLT2_TEST|         GRID|  1472683019100|monit-httpsource-...|    agis|1472683019100|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|          AGLT2|ANALY_AGLT2_SL6-c...|         GRID|  1472683019100|monit-httpsource-...|    agis|1472683019100|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|          AGLT2|     ANALY_AGLT2_SL6|         GRID|  1472683019100|monit-httpsource-...|    agis|1472683019100|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|          AGLT2|AGLT2-CE-gate04.a...|         GRID|  1472683019100|monit-httpsource-...|    agis|1472683019100|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|          AGLT2|    gate04.aglt2.org|         GRID|  1472683019100|monit-httpsource-...|    agis|1472683019100|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|          AGLT2|ANALY_AGLT2_TEST_...|         GRID|  1472683019100|monit-httpsource-...|    agis|1472683019100|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|          AGLT2|AGLT2-CE-gate04.a...|         GRID|  1472683019101|monit-httpsource-...|    agis|1472683019101|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|          AGLT2|    gate04.aglt2.org|         GRID|  1472683019101|monit-httpsource-...|    agis|1472683019101|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|          AGLT2|ANALY_AGLT2_TIER3...|         GRID|  1472683019101|monit-httpsource-...|    agis|1472683019101|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|          AGLT2|ANALY_AGLT2_TIER3...|         GRID|  1472683019101|monit-httpsource-...|    agis|1472683019101|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|          AGLT2|AGLT2-CE-gate04.a...|         GRID|  1472683019101|monit-httpsource-...|    agis|1472683019101|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|          AGLT2|    gate04.aglt2.org|         GRID|  1472683019101|monit-httpsource-...|    agis|1472683019101|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|   AM-04-YERPHI|AM-04-YERPHI-CE-c...|         GRID|  1472683019101|monit-httpsource-...|    agis|1472683019101|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|   AM-04-YERPHI|        AM-04-YERPHI|         GRID|  1472683019101|monit-httpsource-...|    agis|1472683019101|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|   AM-04-YERPHI|ce.yerphi-cluster...|         GRID|  1472683019101|monit-httpsource-...|    agis|1472683019101|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|   AM-04-YERPHI|AM-04-YERPHI-CE-c...|         GRID|  1472683019101|monit-httpsource-...|    agis|1472683019101|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|   AM-04-YERPHI|ce.yerphi-cluster...|         GRID|  1472683019101|monit-httpsource-...|    agis|1472683019101|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|   AM-04-YERPHI|  ANALY_AM-04-YERPHI|         GRID|  1472683019101|monit-httpsource-...|    agis|1472683019101|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|         ANLASC|        ANALY_ANLASC|        local|  1472683019101|monit-httpsource-...|    agis|1472683019101|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|         ANLASC|BNL-ATLAS-CE-grid...|        local|  1472683019101|monit-httpsource-...|    agis|1472683019101|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|         ANLASC|gridgk03.racf.bnl...|        local|  1472683019101|monit-httpsource-...|    agis|1472683019101|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|         ANLASC|   ANALY_ANLASC_Argo|          hpc|  1472683019101|monit-httpsource-...|    agis|1472683019101|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|         ANLASC|BNL-ATLAS-CE-grid...|         GRID|  1472683019101|monit-httpsource-...|    agis|1472683019101|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|         ANLASC|gridgk03.racf.bnl...|         GRID|  1472683019101|monit-httpsource-...|    agis|1472683019101|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|         ANLASC| ANALY_ANLASC_T3Test|         GRID|  1472683019101|monit-httpsource-...|    agis|1472683019101|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|         ANLASC|              ANLASC|         GRID|  1472683019101|monit-httpsource-...|    agis|1472683019101|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|           ARGO|         ARGO_ANLASC|        local|  1472683019101|monit-httpsource-...|    agis|1472683019101|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|           ARGO|           ARGO_Cori|          hpc|  1472683019101|monit-httpsource-...|    agis|1472683019101|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|           ARGO|         ARGO_Edison|          hpc|  1472683019101|monit-httpsource-...|    agis|1472683019101|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|           ARGO|           ARGO_Mira|          hpc|  1472683019101|monit-httpsource-...|    agis|1472683019101|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|          ARNES|         ANALY_ARNES|         GRID|  1472683019101|monit-httpsource-...|    agis|1472683019101|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|          ARNES|ARNES-CE-jost.arn...|         GRID|  1472683019101|monit-httpsource-...|    agis|1472683019101|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|          ARNES|              gsiftp|         GRID|  1472683019101|monit-httpsource-...|    agis|1472683019101|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|          ARNES|               ARNES|         GRID|  1472683019101|monit-httpsource-...|    agis|1472683019101|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|          ARNES|ARNES-CE-jost.arn...|         GRID|  1472683019101|monit-httpsource-...|    agis|1472683019101|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|          ARNES|              gsiftp|         GRID|  1472683019101|monit-httpsource-...|    agis|1472683019101|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|          ARNES|ARNES-CE-jost.arn...|         GRID|  1472683019101|monit-httpsource-...|    agis|1472683019101|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|          ARNES|              gsiftp|         GRID|  1472683019101|monit-httpsource-...|    agis|1472683019101|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|          ARNES|         ARNES_MCORE|         GRID|  1472683019101|monit-httpsource-...|    agis|1472683019101|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|        Arizona|       ANALY_Arizona|         GRID|  1472683019101|monit-httpsource-...|    agis|1472683019101|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|        Arizona|             Arizona|         GRID|  1472683019101|monit-httpsource-...|    agis|1472683019101|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|Australia-ATLAS|     ANALY_AUSTRALIA|         GRID|  1472683019101|monit-httpsource-...|    agis|1472683019101|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|Australia-ATLAS|Australia-ATLAS-C...|         GRID|  1472683019101|monit-httpsource-...|    agis|1472683019101|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|Australia-ATLAS|agcream1.atlas.un...|         GRID|  1472683019101|monit-httpsource-...|    agis|1472683019101|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|Australia-ATLAS|ANALY_AUSTRALIA_TEST|         GRID|  1472683019101|monit-httpsource-...|    agis|1472683019101|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|Australia-ATLAS|Australia-ATLAS-C...|         GRID|  1472683019101|monit-httpsource-...|    agis|1472683019101|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|Australia-ATLAS|agcream1.atlas.un...|         GRID|  1472683019102|monit-httpsource-...|    agis|1472683019102|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|Australia-ATLAS|     Australia-ATLAS|         GRID|  1472683019102|monit-httpsource-...|    agis|1472683019102|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|Australia-ATLAS|Australia-ATLAS-C...|         GRID|  1472683019102|monit-httpsource-...|    agis|1472683019102|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|Australia-ATLAS|agcream1.atlas.un...|         GRID|  1472683019102|monit-httpsource-...|    agis|1472683019102|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|Australia-ATLAS|Australia-ATLAS_M...|         GRID|  1472683019102|monit-httpsource-...|    agis|1472683019102|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|Australia-ATLAS|Australia-ATLAS-C...|         GRID|  1472683019102|monit-httpsource-...|    agis|1472683019102|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|Australia-ATLAS|agcream1.atlas.un...|         GRID|  1472683019102|monit-httpsource-...|    agis|1472683019102|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|Australia-ATLAS|Australia-ATLAS-C...|         GRID|  1472683019102|monit-httpsource-...|    agis|1472683019102|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|Australia-ATLAS|Australia-ATLAS_M...|         GRID|  1472683019102|monit-httpsource-...|    agis|1472683019102|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|Australia-ATLAS|agcream1.atlas.un...|         GRID|  1472683019102|monit-httpsource-...|    agis|1472683019102|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|Australia-ATLAS|Australia-ATLAS-C...|         GRID|  1472683019102|monit-httpsource-...|    agis|1472683019102|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|Australia-ATLAS|Australia-ATLAS_M...|         GRID|  1472683019102|monit-httpsource-...|    agis|1472683019102|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|Australia-ATLAS|agcream1.atlas.un...|         GRID|  1472683019102|monit-httpsource-...|    agis|1472683019102|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|Australia-ATLAS|Australia-ATLAS_TEST|         GRID|  1472683019102|monit-httpsource-...|    agis|1472683019102|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|Australia-ATLAS|Australia-ATLAS-C...|         GRID|  1472683019102|monit-httpsource-...|    agis|1472683019102|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|Australia-ATLAS|agcream1.atlas.un...|         GRID|  1472683019102|monit-httpsource-...|    agis|1472683019102|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|Australia-ATLAS|Australia-ATLAS_T...|         GRID|  1472683019102|monit-httpsource-...|    agis|1472683019102|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|Australia-ATLAS|Australia-ATLAS-C...|         GRID|  1472683019102|monit-httpsource-...|    agis|1472683019102|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|Australia-ATLAS|agcream1.atlas.un...|         GRID|  1472683019102|monit-httpsource-...|    agis|1472683019102|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|   BEIJING-LCG2|       ANALY_BEIJING|         GRID|  1472683019102|monit-httpsource-...|    agis|1472683019102|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|   BEIJING-LCG2|      cce.ihep.ac.cn|         GRID|  1472683019103|monit-httpsource-...|    agis|1472683019103|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|   BEIJING-LCG2|BEIJING-LCG2-CE-c...|         GRID|  1472683019103|monit-httpsource-...|    agis|1472683019103|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|   BEIJING-LCG2|BEIJING-LCG2-CE-a...|          hpc|  1472683019103|monit-httpsource-...|    agis|1472683019103|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|   BEIJING-LCG2|              gsiftp|          hpc|  1472683019103|monit-httpsource-...|    agis|1472683019103|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|   BEIJING-LCG2|BEIJING-CS-TH-1A_...|          hpc|  1472683019103|monit-httpsource-...|    agis|1472683019103|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|   BEIJING-LCG2|BEIJING-LCG2-CE-a...|          hpc|  1472683019103|monit-httpsource-...|    agis|1472683019103|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|   BEIJING-LCG2|BEIJING-LCG2-CE-a...|          hpc|  1472683019103|monit-httpsource-...|    agis|1472683019103|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|   BEIJING-LCG2|              gsiftp|          hpc|  1472683019103|monit-httpsource-...|    agis|1472683019103|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|   BEIJING-LCG2|BEIJING-LCG2-CE-a...|          hpc|  1472683019103|monit-httpsource-...|    agis|1472683019103|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|   BEIJING-LCG2| BEIJING-ERAII_MCORE|          hpc|  1472683019103|monit-httpsource-...|    agis|1472683019103|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|   BEIJING-LCG2|BEIJING-LCG2-CE-a...|          hpc|  1472683019103|monit-httpsource-...|    agis|1472683019103|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|   BEIJING-LCG2|              gsiftp|          hpc|  1472683019103|monit-httpsource-...|    agis|1472683019103|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|   BEIJING-LCG2|BEIJING-LCG2-CE-a...|          hpc|  1472683019103|monit-httpsource-...|    agis|1472683019103|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|   BEIJING-LCG2|   BEIJING-ERA_MCORE|          hpc|  1472683019103|monit-httpsource-...|    agis|1472683019103|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|   BEIJING-LCG2|             BEIJING|         GRID|  1472683019103|monit-httpsource-...|    agis|1472683019103|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|   BEIJING-LCG2|BEIJING-LCG2-crea...|         GRID|  1472683019103|monit-httpsource-...|    agis|1472683019103|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|   BEIJING-LCG2|      cce.ihep.ac.cn|         GRID|  1472683019103|monit-httpsource-...|    agis|1472683019103|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|   BEIJING-LCG2|BEIJING-LCG2-CE-c...|         GRID|  1472683019103|monit-httpsource-...|    agis|1472683019103|agis_raw_pandaqueue|pandaqueue|        raw|    004|
|   BEIJING-LCG2|BEIJING-LCG2-crea...|         GRID|  1472683019103|monit-httpsource-...|    agis|1472683019103|agis_raw_pandaqueue|pandaqueue|        raw|    004|
+---------------+--------------------+-------------+---------------+--------------------+--------+-------------+-------------------+----------+-----------+-------+

```

##  Snapshot for topology

```python

dfagistopology = sqlContext.read.json("/project/monitoring/archive/agis/raw/topology/2016/09/*")
dfagistopology.select("data.cloud","data.country","data.experiment_site","data.federation","data.official_site","data.tier","data.vo","metadata.hostname","metadata.event_timestamp","metadata.producer","metadata.topic","metadata.type","metadata.type_prefix").show(100)
```

### Example output

```text

+-----+--------------------+--------------------+--------------------+--------------------+----+-----+--------------------+---------------+--------+-----------------+--------+-----------+
|cloud|             country|     experiment_site|          federation|       official_site|tier|   vo|            hostname|event_timestamp|producer|            topic|    type|type_prefix|
+-----+--------------------+--------------------+--------------------+--------------------+----+-----+--------------------+---------------+--------+-----------------+--------+-----------+
|   US|                 USA|               AGLT2|            US-AGLT2|               AGLT2|   2|atlas|monit-httpsource-...|  1472684352614|    agis|agis_raw_topology|topology|        raw|
|   NL|             Armenia|        AM-04-YERPHI|           NL-Tier3s|        AM-04-YERPHI|   3|atlas|monit-httpsource-...|  1472684352614|    agis|agis_raw_topology|topology|        raw|
|   US|                 USA|              ANLASC|           US-Tier3s|              ANLASC|   3|atlas|monit-httpsource-...|  1472684352614|    agis|agis_raw_topology|topology|        raw|
|   US|                 USA|                ARGO|           US-Tier3s|              ANLASC|   3|atlas|monit-httpsource-...|  1472684352614|    agis|agis_raw_topology|topology|        raw|
|   ND|            Slovenia|               ARNES|                NULL|               ARNES|   3|atlas|monit-httpsource-...|  1472684352614|    agis|agis_raw_topology|topology|        raw|
|   US|                NULL|             Arizona|                NULL|             Arizona|   3|atlas|monit-httpsource-...|  1472684352614|    agis|agis_raw_topology|topology|        raw|
|   CA|           Australia|     Australia-ATLAS|            AU-ATLAS|     Australia-ATLAS|   2|atlas|monit-httpsource-...|  1472684352614|    agis|agis_raw_topology|topology|        raw|
|   CA|           Australia|    Australia-NECTAR|            AU-ATLAS|     Australia-ATLAS|   3|atlas|monit-httpsource-...|  1472684352614|    agis|agis_raw_topology|topology|        raw|
|   FR|               China|        BEIJING-LCG2|             CN-IHEP|        BEIJING-LCG2|   2|atlas|monit-httpsource-...|  1472684352614|    agis|agis_raw_topology|topology|        raw|
|   US|                 USA| BELLARMINE-ATLAS-T3|           US-Tier3s| BELLARMINE-ATLAS-T3|   3|atlas|monit-httpsource-...|  1472684352614|    agis|agis_raw_topology|topology|        raw|
|   US|                 USA|           BNL-ATLAS|           US-T1-BNL|           BNL-ATLAS|   1|atlas|monit-httpsource-...|  1472684352614|    agis|agis_raw_topology|topology|        raw|
|   US|                 USA|         BNL-AWSEAST|           US-T1-BNL|           BNL-ATLAS|   3|atlas|monit-httpsource-...|  1472684352614|    agis|agis_raw_topology|topology|        raw|
|   US|                 USA|         BNL-AWSWEST|           US-T1-BNL|           BNL-ATLAS|   3|atlas|monit-httpsource-...|  1472684352614|    agis|agis_raw_topology|topology|        raw|
|   US|                 USA|        BNL-AWSWEST2|           US-T1-BNL|           BNL-ATLAS|   3|atlas|monit-httpsource-...|  1472684352614|    agis|agis_raw_topology|topology|        raw|
| CERN|         Switzerland|               BOINC|           CH-Tier3s|               BOINC|   3|atlas|monit-httpsource-...|  1472684352614|    agis|agis_raw_topology|topology|        raw|
|   US|                 USA|      BU_ATLAS_Tier2|             US-NET2|      BU_ATLAS_Tier2|   2|atlas|monit-httpsource-...|  1472684352614|    agis|agis_raw_topology|topology|        raw|
|   US|                 USA|   Brandeis-Atlas-T3|           US-Tier3s|   Brandeis-Atlas-T3|   3|atlas|monit-httpsource-...|  1472684352614|    agis|agis_raw_topology|topology|        raw|
|   CA|              Canada|CA-ALBERTA-WESTGR...|          CA-WEST-T2|CA-ALBERTA-WESTGR...|   2|atlas|monit-httpsource-...|  1472684352614|    agis|agis_raw_topology|topology|        raw|
|   CA|              Canada|             CA-JADE|          CA-WEST-T2|            SFU-LCG2|   3|atlas|monit-httpsource-...|  1472684352614|    agis|agis_raw_topology|topology|        raw|
|   CA|              Canada| CA-MCGILL-CLUMEQ-T2|          CA-EAST-T2| CA-MCGILL-CLUMEQ-T2|   2|atlas|monit-httpsource-...|  1472684352614|    agis|agis_raw_topology|topology|        raw|
|   CA|              Canada|        CA-SCINET-T2|          CA-EAST-T2|        CA-SCINET-T2|   2|atlas|monit-httpsource-...|  1472684352680|    agis|agis_raw_topology|topology|        raw|
|   CA|              Canada|CA-VICTORIA-WESTG...|          CA-WEST-T2|CA-VICTORIA-WESTG...|   2|atlas|monit-httpsource-...|  1472684352680|    agis|agis_raw_topology|topology|        raw|
| CERN|         Switzerland|      CERN-EXTENSION|             CH-CERN|           CERN-PROD|   3|atlas|monit-httpsource-...|  1472684352680|    agis|agis_raw_topology|topology|        raw|
| CERN|         Switzerland|             CERN-P1|             CH-CERN|           CERN-PROD|   3|atlas|monit-httpsource-...|  1472684352680|    agis|agis_raw_topology|topology|        raw|
| CERN|         Switzerland|           CERN-PROD|             CH-CERN|           CERN-PROD|   0|atlas|monit-httpsource-...|  1472684352680|    agis|agis_raw_topology|topology|        raw|
|   DE|         Switzerland|           CSCS-LCG2|       CH-CHIPP-CSCS|           CSCS-LCG2|   2|atlas|monit-httpsource-...|  1472684352680|    agis|agis_raw_topology|topology|        raw|
|   DE|              Poland|       CYFRONET-LCG2|       PL-TIER2-WLCG|       CYFRONET-LCG2|   2|atlas|monit-httpsource-...|  1472684352680|    agis|agis_raw_topology|topology|        raw|
|   DE|             Germany|             DESY-HH|    DE-DESY-ATLAS-T2|             DESY-HH|   2|atlas|monit-httpsource-...|  1472684352680|    agis|agis_raw_topology|topology|        raw|
|   DE|             Germany|             DESY-ZN|    DE-DESY-ATLAS-T2|             DESY-ZN|   2|atlas|monit-httpsource-...|  1472684352680|    agis|agis_raw_topology|topology|        raw|
|   US|                 USA|              DukeT3|           US-Tier3s|              DukeT3|   3|atlas|monit-httpsource-...|  1472684352680|    agis|agis_raw_topology|topology|        raw|
|   ES|           Argentina|           EELA-UNLP|           ES-Tier3s|           EELA-UNLP|   3|atlas|monit-httpsource-...|  1472684352680|    agis|agis_raw_topology|topology|        raw|
|   ES|               Chile|          EELA-UTFSM|         ES-ATLAS-T2|          EELA-UTFSM|   2|atlas|monit-httpsource-...|  1472684352680|    agis|agis_raw_topology|topology|        raw|
|   DE|            Slovakia|          FMPhI-UNIB| SK-Tier2-Federation|         FMPhI-UNIBA|   3|atlas|monit-httpsource-...|  1472684352680|    agis|agis_raw_topology|topology|        raw|
|   DE|            Slovakia|         FMPhI-UNIBA| SK-Tier2-Federation|         FMPhI-UNIBA|   2|atlas|monit-httpsource-...|  1472684352680|    agis|agis_raw_topology|topology|        raw|
|   DE|             Germany|            FZK-LCG2|              DE-KIT|            FZK-LCG2|   1|atlas|monit-httpsource-...|  1472684352680|    agis|agis_raw_topology|topology|        raw|
|   DE|      Czech Republic|            FZU-IPV6|        CZ-Prague-T2|            FZU-IPV6|   3|atlas|monit-httpsource-...|  1472684352680|    agis|agis_raw_topology|topology|        raw|
|   US|                 USA|             Firefly|           US-Tier3s|             Firefly|   3|atlas|monit-httpsource-...|  1472684352680|    agis|agis_raw_topology|topology|        raw|
|   US|                 USA|GOOGLE_COMPUTE_EN...|           US-Tier3s|GOOGLE_COMPUTE_EN...|   3|atlas|monit-httpsource-...|  1472684352680|    agis|agis_raw_topology|topology|        raw|
|   IT|              Greece|          GR-01-AUTH|           IT-Tier3s|          GR-01-AUTH|   3|atlas|monit-httpsource-...|  1472684352680|    agis|agis_raw_topology|topology|        raw|
|   IT|              Greece|        GR-12-TEIKAV|           IT-Tier3s|        GR-12-TEIKAV|   3|atlas|monit-httpsource-...|  1472684352680|    agis|agis_raw_topology|topology|        raw|
|   FR|              France|           GRIF-IRFU|             FR-GRIF|                GRIF|   2|atlas|monit-httpsource-...|  1472684352726|    agis|agis_raw_topology|topology|        raw|
|   FR|              France|            GRIF-LAL|             FR-GRIF|                GRIF|   2|atlas|monit-httpsource-...|  1472684352726|    agis|agis_raw_topology|topology|        raw|
|   FR|              France|          GRIF-LPNHE|             FR-GRIF|                GRIF|   2|atlas|monit-httpsource-...|  1472684352726|    agis|agis_raw_topology|topology|        raw|
|   DE|             Germany|             GoeGrid|DE-DESY-GOE-ATLAS-T2|             GoeGrid|   2|atlas|monit-httpsource-...|  1472684352726|    agis|agis_raw_topology|topology|        raw|
| CERN|         Switzerland|        HELIX_NEBULA|             CH-CERN|        HELIX_NEBULA|   3|atlas|monit-httpsource-...|  1472684352726|    agis|agis_raw_topology|topology|        raw|
|   DE|             Austria|          HEPHY-UIBK|AT-HEPHY-VIENNA-UIBK|          HEPHY-UIBK|   2|atlas|monit-httpsource-...|  1472684352726|    agis|agis_raw_topology|topology|        raw|
|   US|                 USA|      HU_ATLAS_Tier2|             US-NET2|      HU_ATLAS_Tier2|   2|atlas|monit-httpsource-...|  1472684352726|    agis|agis_raw_topology|topology|        raw|
|   US|                 USA|          Hampton_T3|           US-Tier3s|  Hampton PPCF ATLAS|   3|atlas|monit-httpsource-...|  1472684352726|    agis|agis_raw_topology|topology|        raw|
|   CA|              Canada|                IAAS|          CA-WEST-T2|CA-VICTORIA-WESTG...|   3|atlas|monit-httpsource-...|  1472684352726|    agis|agis_raw_topology|topology|        raw|
|   DE|            Slovakia|       IEPSAS-Kosice| SK-Tier2-Federation|       IEPSAS-Kosice|   2|atlas|monit-httpsource-...|  1472684352726|    agis|agis_raw_topology|topology|        raw|
|   ES|               Spain|           IFIC-LCG2|         ES-ATLAS-T2|           IFIC-LCG2|   2|atlas|monit-httpsource-...|  1472684352726|    agis|agis_raw_topology|topology|        raw|
|   NL|              Israel|          IL-TAU-HEP|        IL-HEPTier-2|          IL-TAU-HEP|   2|atlas|monit-httpsource-...|  1472684352726|    agis|agis_raw_topology|topology|        raw|
|   FR|              France|            IN2P3-CC|          FR-CCIN2P3|            IN2P3-CC|   1|atlas|monit-httpsource-...|  1472684352726|    agis|agis_raw_topology|topology|        raw|
|   FR|              France|         IN2P3-CC-T2|      FR-IN2P3-CC-T2|         IN2P3-CC-T2|   2|atlas|monit-httpsource-...|  1472684352726|    agis|agis_raw_topology|topology|        raw|
|   FR|              France|         IN2P3-CC-T3|          FR-CCIN2P3|            IN2P3-CC|   3|atlas|monit-httpsource-...|  1472684352726|    agis|agis_raw_topology|topology|        raw|
|   FR|              France|          IN2P3-CPPM|       FR-IN2P3-CPPM|          IN2P3-CPPM|   2|atlas|monit-httpsource-...|  1472684352726|    agis|agis_raw_topology|topology|        raw|
|   FR|              France|          IN2P3-LAPP|       FR-IN2P3-LAPP|          IN2P3-LAPP|   2|atlas|monit-httpsource-...|  1472684352726|    agis|agis_raw_topology|topology|        raw|
|   FR|              France|           IN2P3-LPC|        FR-IN2P3-LPC|           IN2P3-LPC|   2|atlas|monit-httpsource-...|  1472684352726|    agis|agis_raw_topology|topology|        raw|
|   FR|              France|          IN2P3-LPSC|       FR-IN2P3-LPSC|          IN2P3-LPSC|   2|atlas|monit-httpsource-...|  1472684352726|    agis|agis_raw_topology|topology|        raw|
|   IT|               Italy|     INFN-BOLOGNA-T3|           IT-Tier3s|     INFN-BOLOGNA-T3|   3|atlas|monit-httpsource-...|  1472684352726|    agis|agis_raw_topology|topology|        raw|
|   IT|               Italy|        INFN-COSENZA|           IT-Tier3s|        INFN-COSENZA|   3|atlas|monit-httpsource-...|  1472684352737|    agis|agis_raw_topology|topology|        raw|
|   IT|               Italy|       INFN-FRASCATI|          IT-INFN-T2|       INFN-FRASCATI|   2|atlas|monit-httpsource-...|  1472684352737|    agis|agis_raw_topology|topology|        raw|
|   IT|               Italy|         INFN-GENOVA|           IT-Tier3s|         INFN-GENOVA|   3|atlas|monit-httpsource-...|  1472684352737|    agis|agis_raw_topology|topology|        raw|
|   IT|               Italy|          INFN-LECCE|           IT-Tier3s|          INFN-LECCE|   3|atlas|monit-httpsource-...|  1472684352737|    agis|agis_raw_topology|topology|        raw|
|   IT|               Italy|  INFN-MILANO-ATLASC|          IT-INFN-T2|  INFN-MILANO-ATLASC|   2|atlas|monit-httpsource-...|  1472684352737|    agis|agis_raw_topology|topology|        raw|
|   IT|               Italy|   INFN-NAPOLI-ATLAS|          IT-INFN-T2|   INFN-NAPOLI-ATLAS|   2|atlas|monit-httpsource-...|  1472684352737|    agis|agis_raw_topology|topology|        raw|
|   IT|               Italy|          INFN-PAVIA|           IT-Tier3s|          INFN-PAVIA|   3|atlas|monit-httpsource-...|  1472684352737|    agis|agis_raw_topology|topology|        raw|
|   IT|               Italy|          INFN-ROMA1|          IT-INFN-T2|          INFN-ROMA1|   2|atlas|monit-httpsource-...|  1472684352737|    agis|agis_raw_topology|topology|        raw|
|   IT|               Italy|          INFN-ROMA2|           IT-Tier3s|          INFN-ROMA2|   3|atlas|monit-httpsource-...|  1472684352737|    agis|agis_raw_topology|topology|        raw|
|   IT|               Italy|          INFN-ROMA3|           IT-Tier3s|          INFN-ROMA3|   3|atlas|monit-httpsource-...|  1472684352737|    agis|agis_raw_topology|topology|        raw|
|   IT|               Italy|             INFN-T1|        IT-INFN-CNAF|             INFN-T1|   1|atlas|monit-httpsource-...|  1472684352737|    agis|agis_raw_topology|topology|        raw|
|   IT|               Italy|        INFN-TRIESTE|           IT-Tier3s|        INFN-TRIESTE|   3|atlas|monit-httpsource-...|  1472684352737|    agis|agis_raw_topology|topology|        raw|
|   RU|  Russian Federation|                ITEP|             RU-RDIG|                ITEP|   2|atlas|monit-httpsource-...|  1472684352737|    agis|agis_raw_topology|topology|        raw|
|   US|                 USA|         IllinoisHEP|           US-Tier3s|         IllinoisHEP|   3|atlas|monit-httpsource-...|  1472684352737|    agis|agis_raw_topology|topology|        raw|
|   US|                 USA|             Indiana|                NULL| Indiana_ATLAS_Tier3|   3|atlas|monit-httpsource-...|  1472684352737|    agis|agis_raw_topology|topology|        raw|
|   RU|  Russian Federation|           JINR-LCG2|             RU-RDIG|           JINR-LCG2|   2|atlas|monit-httpsource-...|  1472684352737|    agis|agis_raw_topology|topology|        raw|
|   US|                 USA|        LBNL_DSD_ITB|           US-Tier3s|        LBNL_DSD_ITB|   3|atlas|monit-httpsource-...|  1472684352737|    agis|agis_raw_topology|topology|        raw|
|   ES|            Portugal|         LIP-Coimbra|    PT-LIP-LCG-Tier2|         LIP-Coimbra|   2|atlas|monit-httpsource-...|  1472684352737|    agis|agis_raw_topology|topology|        raw|
|   ES|            Portugal|          LIP-Lisbon|    PT-LIP-LCG-Tier2|          LIP-Lisbon|   3|atlas|monit-httpsource-...|  1472684352737|    agis|agis_raw_topology|topology|        raw|
|   DE|             Germany|             LRZ-LMU|             DE-MCAT|             LRZ-LMU|   2|atlas|monit-httpsource-...|  1472684352737|    agis|agis_raw_topology|topology|        raw|
|   US|                 USA|             LUCILLE|             US-SWT2|             LUCILLE|   3|atlas|monit-httpsource-...|  1472684352774|    agis|agis_raw_topology|topology|        raw|
|   DE|             Germany|               MPPMU|             DE-MCAT|               MPPMU|   2|atlas|monit-httpsource-...|  1472684352774|    agis|agis_raw_topology|topology|        raw|
|   US|                 USA|                MWT2|             US-MWT2|                MWT2|   2|atlas|monit-httpsource-...|  1472684352774|    agis|agis_raw_topology|topology|        raw|
|   DE|             Germany|             MaiGRID|           DE-Tier3s|             MaiGRID|   3|atlas|monit-httpsource-...|  1472684352774|    agis|agis_raw_topology|topology|        raw|
| CERN|         Switzerland|     Microsoft-Azure|             CH-CERN|           CERN-PROD|   3|atlas|monit-httpsource-...|  1472684352774|    agis|agis_raw_topology|topology|        raw|
|   ES|            Portugal|       NCG-INGRID-PT|    PT-LIP-LCG-Tier2|       NCG-INGRID-PT|   2|atlas|monit-httpsource-...|  1472684352774|    agis|agis_raw_topology|topology|        raw|
|   ND|              Nordic|             NDGF-T1|                NDGF|             NDGF-T1|   1|atlas|monit-httpsource-...|  1472684352774|    agis|agis_raw_topology|topology|        raw|
|   NL|         Netherlands|       NIKHEF-ELPROD|               NL-T1|       NIKHEF-ELPROD|   1|atlas|monit-httpsource-...|  1472684352774|    agis|agis_raw_topology|topology|        raw|
|   ND|              Norway|       NO-NORGRID-T2|       NO-NORGRID-T2|       NO-NORGRID-T2|   2|atlas|monit-httpsource-...|  1472684352774|    agis|agis_raw_topology|topology|        raw|
|   US|                 USA|           NYU-ATLAS|                NULL|           NYU-ATLAS|   3|atlas|monit-httpsource-...|  1472684352774|    agis|agis_raw_topology|topology|        raw|
|   US|                 USA|               Nevis|                NULL|               Nevis|   3|atlas|monit-httpsource-...|  1472684352774|    agis|agis_raw_topology|topology|        raw|
|   US|United States of ...|                OLCF|           US-Tier3s|                OLCF|   3|atlas|monit-httpsource-...|  1472684352774|    agis|agis_raw_topology|topology|        raw|
|   US|                 USA|           OUHEP_OSG|           US-Tier3s|           OUHEP_OSG|   3|atlas|monit-httpsource-...|  1472684352774|    agis|agis_raw_topology|topology|        raw|
|   US|                 USA|       OU_OCHEP_SWT2|             US-SWT2|       OU_OCHEP_SWT2|   2|atlas|monit-httpsource-...|  1472684352774|    agis|agis_raw_topology|topology|        raw|
|   US|                 USA|      OU_OSCER_ATLAS|             US-SWT2|      OU_OSCER_ATLAS|   2|atlas|monit-httpsource-...|  1472684352774|    agis|agis_raw_topology|topology|        raw|
|   DE|              Poland|                PSNC|       PL-TIER2-WLCG|                PSNC|   2|atlas|monit-httpsource-...|  1472684352774|    agis|agis_raw_topology|topology|        raw|
|   US|                 USA|              PennT3|           US-Tier3s|              PennT3|   3|atlas|monit-httpsource-...|  1472684352774|    agis|agis_raw_topology|topology|        raw|
|   UK|                  UK|            RAL-LCG2|           UK-T1-RAL|            RAL-LCG2|   1|atlas|monit-httpsource-...|  1472684352774|    agis|agis_raw_topology|topology|        raw|
|   FR|             Romania|         RO-02-NIPNE|              RO-LCG|         RO-02-NIPNE|   2|atlas|monit-httpsource-...|  1472684352774|    agis|agis_raw_topology|topology|        raw|
|   FR|             Romania|         RO-07-NIPNE|              RO-LCG|         RO-07-NIPNE|   2|atlas|monit-httpsource-...|  1472684352774|    agis|agis_raw_topology|topology|        raw|
+-----+--------------------+--------------------+--------------------+--------------------+----+-----+--------------------+---------------+--------+-----------------+--------+-----------+


```

## Snapshot for ``agis_metadata\raw\pandaqueue``

```python
df = sqlContext.read.json("/project/monitoring/archive/agis_metadata/raw/pandaqueue/2016/05/*")
df.select("message.ceinfo","message.hc_param","message.panda_queue_name","message.panda_resource","message.panda_site","message.pilot_manager","message.rc_site","message.site","message.vo_name").filter("upper(panda_queue_name) LIKE '%CERN%'").distinct().show()


```

### Example output


```text
+------+-------------+--------------------+--------------------+----------+-------------+---------+---------+-------+
|ceinfo|     hc_param|    panda_queue_name|      panda_resource|panda_site|pilot_manager|  rc_site|     site|vo_name|
+------+-------------+--------------------+--------------------+----------+-------------+---------+---------+-------+
|  null|     OnlyTest|CERN-P1_preprod_M...|CERN-P1_preprod_M...|   CERN-P1|          APF|CERN-PROD|  CERN-P1|  atlas|
|  null|AutoExclusion|    ANALY_CERN_SHORT|    ANALY_CERN_SHORT| CERN-PROD|          APF|CERN-PROD|CERN-PROD|  atlas|
|  null|AutoExclusion|CERN-P1_DYNAMIC_M...|CERN-P1_DYNAMIC_M...|   CERN-P1|          APF|CERN-PROD|  CERN-P1|  atlas|
|  null|AutoExclusion|CERN-PROD_CLOUD_M...|CERN-PROD_CLOUD_M...|CERN-CLOUD|          APF|CERN-PROD|CERN-PROD|  atlas|
|  null|     OnlyTest|    ANALY_CERN_CLOUD|    ANALY_CERN_CLOUD|CERN-CLOUD|          APF|CERN-PROD|CERN-PROD|  atlas|
|  null|     OnlyTest|     ANALY_CERN_TEST|     ANALY_CERN_TEST| CERN-PROD|        local|CERN-PROD|CERN-PROD|  atlas|
|  null|     OnlyTest|     CERN-P1_preprod|     CERN-P1_preprod|   CERN-P1|          APF|CERN-PROD|  CERN-P1|  atlas|
|  null|AutoExclusion|CERN-PROD-all-pro...|           CERN-PROD| CERN-PROD|          APF|CERN-PROD|CERN-PROD|  atlas|
|  null|        False|        ANALY_CERNVM|   ANALY_CERNVM_test|    CERNVM|          APF|CERN-PROD|CERN-PROD|  atlas|
|  null|     OnlyTest|   ANALY_CERN_XROOTD|   ANALY_CERN_XROOTD| CERN-PROD|          APF|CERN-PROD|CERN-PROD|  atlas|
|  null|     OnlyTest|ANALY_CERN_GLEXECDEV|ANALY_CERN_GLEXECDEV| CERN-PROD|          APF|CERN-PROD|CERN-PROD|  atlas|
|  null|AutoExclusion|     CERN-PROD_SHORT|     CERN-PROD_SHORT| CERN-PROD|          APF|CERN-PROD|CERN-PROD|  atlas|
|  null|AutoExclusion|CERN-PROD_PRESERV...|CERN-PROD_PRESERV...|CERN-CLOUD|          APF|CERN-PROD|CERN-PROD|  atlas|
|  null|AutoExclusion|    CERN-P1_MCORE_HI|    CERN-P1_MCORE_HI|   CERN-P1|          APF|CERN-PROD|  CERN-P1|  atlas|
|  null|AutoExclusion|   CERN-P1-OpenStack|             CERN-P1|   CERN-P1|          APF|CERN-PROD|  CERN-P1|  atlas|
|  null|AutoExclusion|CERN-P1_DYNAMIC_M...|CERN-P1_DYNAMIC_M...|   CERN-P1|          APF|CERN-PROD|  CERN-P1|  atlas|
|  null|AutoExclusion|   CERN-PROD-preprod|   CERN-PROD-preprod| CERN-PROD|          APF|CERN-PROD|CERN-PROD|  atlas|
|  null|AutoExclusion|     ANALY_CERN_SLC6|     ANALY_CERN_SLC6| CERN-PROD|          APF|CERN-PROD|CERN-PROD|  atlas|
|  null|AutoExclusion|     CERN-PROD_CLOUD|     CERN-PROD_CLOUD|CERN-CLOUD|          APF|CERN-PROD|CERN-PROD|  atlas|
|  null|AutoExclusion|       CERN-P1_MCORE|       CERN-P1_MCORE|   CERN-P1|          APF|CERN-PROD|  CERN-P1|  atlas|
+------+-------------+--------------------+--------------------+----------+-------------+---------+---------+-------+

```


## Snapshot for ``agis_metadata\raw\pandaqueue`` "objectstores:"

```python
df = sqlContext.read.json("/project/monitoring/archive/agis_metadata/raw/pandaqueue/2016/05/*")
df.select(explode("message.objectstores")).toDF("element").show(100,False)


```

### Example output


```text

+----------------------------------------------------------------------------------------------------------------------------------------------------------+
|element                                                                                                                                                   |
+----------------------------------------------------------------------------------------------------------------------------------------------------------+
|[,BNL_ObjectStoreKey.pub,/atlas_eventservice,2.0,eventservice,s3://cephgw.usatlas.bnl.gov:8443/,AWS-S3,17113.0,false,BNL_OS_0,BNL_ObjectStoreKey,ACTIVE]  |
|[BNL-ATLAS_LOGS_old,BNL_ObjectStoreKey.pub,/atlas_logs,3.0,logs,s3://cephgw.usatlas.bnl.gov:8443/,AWS-S3,17113.0,false,BNL_OS_0,BNL_ObjectStoreKey,ACTIVE]|
|[,BNL_ObjectStoreKey.pub,/atlas_eventservice,2.0,eventservice,s3://cephgw.usatlas.bnl.gov:8443/,AWS-S3,17113.0,false,BNL_OS_0,BNL_ObjectStoreKey,ACTIVE]  |
|[BNL-ATLAS_LOGS_old,BNL_ObjectStoreKey.pub,/atlas_logs,3.0,logs,s3://cephgw.usatlas.bnl.gov:8443/,AWS-S3,17113.0,false,BNL_OS_0,BNL_ObjectStoreKey,ACTIVE]|
|[,BNL_ObjectStoreKey.pub,/atlas_eventservice,2.0,eventservice,s3://cephgw.usatlas.bnl.gov:8443/,AWS-S3,17113.0,false,BNL_OS_0,BNL_ObjectStoreKey,ACTIVE]  |
|[BNL-ATLAS_LOGS_old,BNL_ObjectStoreKey.pub,/atlas_logs,3.0,logs,s3://cephgw.usatlas.bnl.gov:8443/,AWS-S3,17113.0,false,BNL_OS_0,BNL_ObjectStoreKey,ACTIVE]|
|[,BNL_ObjectStoreKey.pub,/atlas_eventservice,2.0,eventservice,s3://cephgw.usatlas.bnl.gov:8443/,AWS-S3,17113.0,false,BNL_OS_0,BNL_ObjectStoreKey,ACTIVE]  |
|[BNL-ATLAS_LOGS_old,BNL_ObjectStoreKey.pub,/atlas_logs,3.0,logs,s3://cephgw.usatlas.bnl.gov:8443/,AWS-S3,17113.0,false,BNL_OS_0,BNL_ObjectStoreKey,ACTIVE]|
|[,BNL_ObjectStoreKey.pub,/atlas_eventservice,2.0,eventservice,s3://cephgw.usatlas.bnl.gov:8443/,AWS-S3,17113.0,false,BNL_OS_0,BNL_ObjectStoreKey,ACTIVE]  |
|[BNL-ATLAS_LOGS_old,BNL_ObjectStoreKey.pub,/atlas_logs,3.0,logs,s3://cephgw.usatlas.bnl.gov:8443/,AWS-S3,17113.0,false,BNL_OS_0,BNL_ObjectStoreKey,ACTIVE]|
|[,BNL_ObjectStoreKey.pub,/atlas_eventservice,2.0,eventservice,s3://cephgw.usatlas.bnl.gov:8443/,AWS-S3,17113.0,false,BNL_OS_0,BNL_ObjectStoreKey,ACTIVE]  |
|[BNL-ATLAS_LOGS_old,BNL_ObjectStoreKey.pub,/atlas_logs,3.0,logs,s3://cephgw.usatlas.bnl.gov:8443/,AWS-S3,17113.0,false,BNL_OS_0,BNL_ObjectStoreKey,ACTIVE]|
|[,BNL_ObjectStoreKey.pub,/atlas_eventservice,2.0,eventservice,s3://cephgw.usatlas.bnl.gov:8443/,AWS-S3,17113.0,false,BNL_OS_0,BNL_ObjectStoreKey,ACTIVE]  |
|[BNL-ATLAS_LOGS_old,BNL_ObjectStoreKey.pub,/atlas_logs,3.0,logs,s3://cephgw.usatlas.bnl.gov:8443/,AWS-S3,17113.0,false,BNL_OS_0,BNL_ObjectStoreKey,ACTIVE]|
|[,BNL_ObjectStoreKey.pub,/atlas_eventservice,2.0,eventservice,s3://cephgw.usatlas.bnl.gov:8443/,AWS-S3,17113.0,false,BNL_OS_0,BNL_ObjectStoreKey,ACTIVE]  |
|[BNL-ATLAS_LOGS_old,BNL_ObjectStoreKey.pub,/atlas_logs,3.0,logs,s3://cephgw.usatlas.bnl.gov:8443/,AWS-S3,17113.0,false,BNL_OS_0,BNL_ObjectStoreKey,ACTIVE]|
|[,BNL_ObjectStoreKey.pub,/atlas_eventservice,2.0,eventservice,s3://cephgw.usatlas.bnl.gov:8443/,AWS-S3,17113.0,false,BNL_OS_0,BNL_ObjectStoreKey,ACTIVE]  |
|[BNL-ATLAS_LOGS_old,BNL_ObjectStoreKey.pub,/atlas_logs,3.0,logs,s3://cephgw.usatlas.bnl.gov:8443/,AWS-S3,17113.0,false,BNL_OS_0,BNL_ObjectStoreKey,ACTIVE]|
|[,BNL_ObjectStoreKey.pub,/atlas_eventservice,2.0,eventservice,s3://cephgw.usatlas.bnl.gov:8443/,AWS-S3,17113.0,false,BNL_OS_0,BNL_ObjectStoreKey,ACTIVE]  |
|[BNL-ATLAS_LOGS_old,BNL_ObjectStoreKey.pub,/atlas_logs,3.0,logs,s3://cephgw.usatlas.bnl.gov:8443/,AWS-S3,17113.0,false,BNL_OS_0,BNL_ObjectStoreKey,ACTIVE]|
|[,BNL_ObjectStoreKey.pub,/atlas_eventservice,2.0,eventservice,s3://cephgw.usatlas.bnl.gov:8443/,AWS-S3,17113.0,false,BNL_OS_0,BNL_ObjectStoreKey,ACTIVE]  |
|[BNL-ATLAS_LOGS_old,BNL_ObjectStoreKey.pub,/atlas_logs,3.0,logs,s3://cephgw.usatlas.bnl.gov:8443/,AWS-S3,17113.0,false,BNL_OS_0,BNL_ObjectStoreKey,ACTIVE]|
|[,BNL_ObjectStoreKey.pub,/atlas_eventservice,2.0,eventservice,s3://cephgw.usatlas.bnl.gov:8443/,AWS-S3,17113.0,false,BNL_OS_0,BNL_ObjectStoreKey,ACTIVE]  |
|[BNL-ATLAS_LOGS_old,BNL_ObjectStoreKey.pub,/atlas_logs,3.0,logs,s3://cephgw.usatlas.bnl.gov:8443/,AWS-S3,17113.0,false,BNL_OS_0,BNL_ObjectStoreKey,ACTIVE]|
|[,BNL_ObjectStoreKey.pub,/atlas_eventservice,2.0,eventservice,s3://cephgw.usatlas.bnl.gov:8443/,AWS-S3,17113.0,false,BNL_OS_0,BNL_ObjectStoreKey,ACTIVE]  |
|[BNL-ATLAS_LOGS_old,BNL_ObjectStoreKey.pub,/atlas_logs,3.0,logs,s3://cephgw.usatlas.bnl.gov:8443/,AWS-S3,17113.0,false,BNL_OS_0,BNL_ObjectStoreKey,ACTIVE]|
|[,BNL_ObjectStoreKey.pub,/atlas_eventservice,2.0,eventservice,s3://cephgw.usatlas.bnl.gov:8443/,AWS-S3,17113.0,false,BNL_OS_0,BNL_ObjectStoreKey,ACTIVE]  |
|[BNL-ATLAS_LOGS_old,BNL_ObjectStoreKey.pub,/atlas_logs,3.0,logs,s3://cephgw.usatlas.bnl.gov:8443/,AWS-S3,17113.0,false,BNL_OS_0,BNL_ObjectStoreKey,ACTIVE]|
|[,BNL_ObjectStoreKey.pub,/atlas_eventservice,2.0,eventservice,s3://cephgw.usatlas.bnl.gov:8443/,AWS-S3,17113.0,false,BNL_OS_0,BNL_ObjectStoreKey,ACTIVE]  |
|[BNL-ATLAS_LOGS_old,BNL_ObjectStoreKey.pub,/atlas_logs,3.0,logs,s3://cephgw.usatlas.bnl.gov:8443/,AWS-S3,17113.0,false,BNL_OS_0,BNL_ObjectStoreKey,ACTIVE]|
|[,BNL_ObjectStoreKey.pub,/atlas_eventservice,2.0,eventservice,s3://cephgw.usatlas.bnl.gov:8443/,AWS-S3,17113.0,false,BNL_OS_0,BNL_ObjectStoreKey,ACTIVE]  |
|[BNL-ATLAS_LOGS_old,BNL_ObjectStoreKey.pub,/atlas_logs,3.0,logs,s3://cephgw.usatlas.bnl.gov:8443/,AWS-S3,17113.0,false,BNL_OS_0,BNL_ObjectStoreKey,ACTIVE]|
|[,BNL_ObjectStoreKey.pub,/atlas_eventservice,2.0,eventservice,s3://cephgw.usatlas.bnl.gov:8443/,AWS-S3,17113.0,false,BNL_OS_0,BNL_ObjectStoreKey,ACTIVE]  |
|[BNL-ATLAS_LOGS_old,BNL_ObjectStoreKey.pub,/atlas_logs,3.0,logs,s3://cephgw.usatlas.bnl.gov:8443/,AWS-S3,17113.0,false,BNL_OS_0,BNL_ObjectStoreKey,ACTIVE]|
|[,BNL_ObjectStoreKey.pub,/atlas_eventservice,2.0,eventservice,s3://cephgw.usatlas.bnl.gov:8443/,AWS-S3,17113.0,false,BNL_OS_0,BNL_ObjectStoreKey,ACTIVE]  |
|[BNL-ATLAS_LOGS_old,BNL_ObjectStoreKey.pub,/atlas_logs,3.0,logs,s3://cephgw.usatlas.bnl.gov:8443/,AWS-S3,17113.0,false,BNL_OS_0,BNL_ObjectStoreKey,ACTIVE]|
|[,BNL_ObjectStoreKey.pub,/atlas_eventservice,2.0,eventservice,s3://cephgw.usatlas.bnl.gov:8443/,AWS-S3,17113.0,false,BNL_OS_0,BNL_ObjectStoreKey,ACTIVE]  |
|[BNL-ATLAS_LOGS_old,BNL_ObjectStoreKey.pub,/atlas_logs,3.0,logs,s3://cephgw.usatlas.bnl.gov:8443/,AWS-S3,17113.0,false,BNL_OS_0,BNL_ObjectStoreKey,ACTIVE]|
|[,BNL_ObjectStoreKey.pub,/atlas_eventservice,2.0,eventservice,s3://cephgw.usatlas.bnl.gov:8443/,AWS-S3,17113.0,false,BNL_OS_0,BNL_ObjectStoreKey,ACTIVE]  |
|[BNL-ATLAS_LOGS_old,BNL_ObjectStoreKey.pub,/atlas_logs,3.0,logs,s3://cephgw.usatlas.bnl.gov:8443/,AWS-S3,17113.0,false,BNL_OS_0,BNL_ObjectStoreKey,ACTIVE]|
|[,BNL_ObjectStoreKey.pub,/atlas_eventservice,2.0,eventservice,s3://cephgw.usatlas.bnl.gov:8443/,AWS-S3,17113.0,false,BNL_OS_0,BNL_ObjectStoreKey,ACTIVE]  |
|[BNL-ATLAS_LOGS_old,BNL_ObjectStoreKey.pub,/atlas_logs,3.0,logs,s3://cephgw.usatlas.bnl.gov:8443/,AWS-S3,17113.0,false,BNL_OS_0,BNL_ObjectStoreKey,ACTIVE]|
|[,BNL_ObjectStoreKey.pub,/atlas_eventservice,2.0,eventservice,s3://cephgw.usatlas.bnl.gov:8443/,AWS-S3,17113.0,false,BNL_OS_0,BNL_ObjectStoreKey,ACTIVE]  |
|[BNL-ATLAS_LOGS_old,BNL_ObjectStoreKey.pub,/atlas_logs,3.0,logs,s3://cephgw.usatlas.bnl.gov:8443/,AWS-S3,17113.0,false,BNL_OS_0,BNL_ObjectStoreKey,ACTIVE]|
|[,BNL_ObjectStoreKey.pub,/atlas_eventservice,2.0,eventservice,s3://cephgw.usatlas.bnl.gov:8443/,AWS-S3,17113.0,false,BNL_OS_0,BNL_ObjectStoreKey,ACTIVE]  |
|[BNL-ATLAS_LOGS_old,BNL_ObjectStoreKey.pub,/atlas_logs,3.0,logs,s3://cephgw.usatlas.bnl.gov:8443/,AWS-S3,17113.0,false,BNL_OS_0,BNL_ObjectStoreKey,ACTIVE]|
|[,BNL_ObjectStoreKey.pub,/atlas_eventservice,2.0,eventservice,s3://cephgw.usatlas.bnl.gov:8443/,AWS-S3,17113.0,false,BNL_OS_0,BNL_ObjectStoreKey,ACTIVE]  |
|[BNL-ATLAS_LOGS_old,BNL_ObjectStoreKey.pub,/atlas_logs,3.0,logs,s3://cephgw.usatlas.bnl.gov:8443/,AWS-S3,17113.0,false,BNL_OS_0,BNL_ObjectStoreKey,ACTIVE]|
|[,CERN_ObjectStoreKey.pub,/atlas_eventservice,41.0,eventservice,s3://cs3.cern.ch:443/,AWS-S3-SSL,17172.0,true,CERN_OS_1,CERN_ObjectStoreKey,ACTIVE]       |
|[CERN-PROD_LOGS_old,CERN_ObjectStoreKey.pub,/atlas_logs,42.0,logs,s3://cs3.cern.ch:443/,AWS-S3-SSL,17172.0,true,CERN_OS_1,CERN_ObjectStoreKey,ACTIVE]     |
|[,CERN_ObjectStoreKey.pub,/atlas_eventservice,41.0,eventservice,s3://cs3.cern.ch:443/,AWS-S3-SSL,17172.0,true,CERN_OS_1,CERN_ObjectStoreKey,ACTIVE]       |
|[CERN-PROD_LOGS_old,CERN_ObjectStoreKey.pub,/atlas_logs,42.0,logs,s3://cs3.cern.ch:443/,AWS-S3-SSL,17172.0,true,CERN_OS_1,CERN_ObjectStoreKey,ACTIVE]     |
|[,CERN_ObjectStoreKey.pub,/atlas_eventservice,41.0,eventservice,s3://cs3.cern.ch:443/,AWS-S3-SSL,17172.0,true,CERN_OS_1,CERN_ObjectStoreKey,ACTIVE]       |
|[CERN-PROD_LOGS_old,CERN_ObjectStoreKey.pub,/atlas_logs,42.0,logs,s3://cs3.cern.ch:443/,AWS-S3-SSL,17172.0,true,CERN_OS_1,CERN_ObjectStoreKey,ACTIVE]     |
|[,CERN_ObjectStoreKey.pub,/atlas_eventservice,41.0,eventservice,s3://cs3.cern.ch:443/,AWS-S3-SSL,17172.0,true,CERN_OS_1,CERN_ObjectStoreKey,ACTIVE]       |
|[CERN-PROD_LOGS_old,CERN_ObjectStoreKey.pub,/atlas_logs,42.0,logs,s3://cs3.cern.ch:443/,AWS-S3-SSL,17172.0,true,CERN_OS_1,CERN_ObjectStoreKey,ACTIVE]     |
|[,CERN_ObjectStoreKey.pub,/atlas_eventservice,41.0,eventservice,s3://cs3.cern.ch:443/,AWS-S3-SSL,17172.0,true,CERN_OS_1,CERN_ObjectStoreKey,ACTIVE]       |
|[CERN-PROD_LOGS_old,CERN_ObjectStoreKey.pub,/atlas_logs,42.0,logs,s3://cs3.cern.ch:443/,AWS-S3-SSL,17172.0,true,CERN_OS_1,CERN_ObjectStoreKey,ACTIVE]     |
|[,CERN_ObjectStoreKey.pub,/atlas_eventservice,41.0,eventservice,s3://cs3.cern.ch:443/,AWS-S3-SSL,17172.0,true,CERN_OS_1,CERN_ObjectStoreKey,ACTIVE]       |
|[CERN-PROD_LOGS_old,CERN_ObjectStoreKey.pub,/atlas_logs,42.0,logs,s3://cs3.cern.ch:443/,AWS-S3-SSL,17172.0,true,CERN_OS_1,CERN_ObjectStoreKey,ACTIVE]     |
|[,BNL_ObjectStoreKey.pub,/atlas_eventservice,2.0,eventservice,s3://cephgw.usatlas.bnl.gov:8443/,AWS-S3,17113.0,false,BNL_OS_0,BNL_ObjectStoreKey,ACTIVE]  |
|[BNL-ATLAS_LOGS_old,BNL_ObjectStoreKey.pub,/atlas_logs,3.0,logs,s3://cephgw.usatlas.bnl.gov:8443/,AWS-S3,17113.0,false,BNL_OS_0,BNL_ObjectStoreKey,ACTIVE]|
|[,BNL_ObjectStoreKey.pub,/atlas_eventservice,2.0,eventservice,s3://cephgw.usatlas.bnl.gov:8443/,AWS-S3,17113.0,false,BNL_OS_0,BNL_ObjectStoreKey,ACTIVE]  |
|[BNL-ATLAS_LOGS_old,BNL_ObjectStoreKey.pub,/atlas_logs,3.0,logs,s3://cephgw.usatlas.bnl.gov:8443/,AWS-S3,17113.0,false,BNL_OS_0,BNL_ObjectStoreKey,ACTIVE]|
|[,BNL_ObjectStoreKey.pub,/atlas_eventservice,2.0,eventservice,s3://cephgw.usatlas.bnl.gov:8443/,AWS-S3,17113.0,false,BNL_OS_0,BNL_ObjectStoreKey,ACTIVE]  |
|[BNL-ATLAS_LOGS_old,BNL_ObjectStoreKey.pub,/atlas_logs,3.0,logs,s3://cephgw.usatlas.bnl.gov:8443/,AWS-S3,17113.0,false,BNL_OS_0,BNL_ObjectStoreKey,ACTIVE]|
|[,BNL_ObjectStoreKey.pub,/atlas_eventservice,2.0,eventservice,s3://cephgw.usatlas.bnl.gov:8443/,AWS-S3,17113.0,false,BNL_OS_0,BNL_ObjectStoreKey,ACTIVE]  |
|[BNL-ATLAS_LOGS_old,BNL_ObjectStoreKey.pub,/atlas_logs,3.0,logs,s3://cephgw.usatlas.bnl.gov:8443/,AWS-S3,17113.0,false,BNL_OS_0,BNL_ObjectStoreKey,ACTIVE]|
|[,BNL_ObjectStoreKey.pub,/atlas_eventservice,2.0,eventservice,s3://cephgw.usatlas.bnl.gov:8443/,AWS-S3,17113.0,false,BNL_OS_0,BNL_ObjectStoreKey,ACTIVE]  |
|[BNL-ATLAS_LOGS_old,BNL_ObjectStoreKey.pub,/atlas_logs,3.0,logs,s3://cephgw.usatlas.bnl.gov:8443/,AWS-S3,17113.0,false,BNL_OS_0,BNL_ObjectStoreKey,ACTIVE]|
|[,BNL_ObjectStoreKey.pub,/atlas_eventservice,2.0,eventservice,s3://cephgw.usatlas.bnl.gov:8443/,AWS-S3,17113.0,false,BNL_OS_0,BNL_ObjectStoreKey,ACTIVE]  |
|[BNL-ATLAS_LOGS_old,BNL_ObjectStoreKey.pub,/atlas_logs,3.0,logs,s3://cephgw.usatlas.bnl.gov:8443/,AWS-S3,17113.0,false,BNL_OS_0,BNL_ObjectStoreKey,ACTIVE]|
|[,BNL_ObjectStoreKey.pub,/atlas_eventservice,2.0,eventservice,s3://cephgw.usatlas.bnl.gov:8443/,AWS-S3,17113.0,false,BNL_OS_0,BNL_ObjectStoreKey,ACTIVE]  |
|[BNL-ATLAS_LOGS_old,BNL_ObjectStoreKey.pub,/atlas_logs,3.0,logs,s3://cephgw.usatlas.bnl.gov:8443/,AWS-S3,17113.0,false,BNL_OS_0,BNL_ObjectStoreKey,ACTIVE]|
|[,BNL_ObjectStoreKey.pub,/atlas_eventservice,2.0,eventservice,s3://cephgw.usatlas.bnl.gov:8443/,AWS-S3,17113.0,false,BNL_OS_0,BNL_ObjectStoreKey,ACTIVE]  |
|[BNL-ATLAS_LOGS_old,BNL_ObjectStoreKey.pub,/atlas_logs,3.0,logs,s3://cephgw.usatlas.bnl.gov:8443/,AWS-S3,17113.0,false,BNL_OS_0,BNL_ObjectStoreKey,ACTIVE]|
|[,CERN_ObjectStoreKey.pub,/atlas_eventservice,41.0,eventservice,s3://cs3.cern.ch:443/,AWS-S3-SSL,17172.0,true,CERN_OS_1,CERN_ObjectStoreKey,ACTIVE]       |
|[CERN-PROD_LOGS_old,CERN_ObjectStoreKey.pub,/atlas_logs,42.0,logs,s3://cs3.cern.ch:443/,AWS-S3-SSL,17172.0,true,CERN_OS_1,CERN_ObjectStoreKey,ACTIVE]     |
|[,CERN_ObjectStoreKey.pub,/atlas_eventservice,41.0,eventservice,s3://cs3.cern.ch:443/,AWS-S3-SSL,17172.0,true,CERN_OS_1,CERN_ObjectStoreKey,ACTIVE]       |
|[CERN-PROD_LOGS_old,CERN_ObjectStoreKey.pub,/atlas_logs,42.0,logs,s3://cs3.cern.ch:443/,AWS-S3-SSL,17172.0,true,CERN_OS_1,CERN_ObjectStoreKey,ACTIVE]     |
|[,CERN_ObjectStoreKey.pub,/atlas_eventservice,41.0,eventservice,s3://cs3.cern.ch:443/,AWS-S3-SSL,17172.0,true,CERN_OS_1,CERN_ObjectStoreKey,ACTIVE]       |
|[CERN-PROD_LOGS_old,CERN_ObjectStoreKey.pub,/atlas_logs,42.0,logs,s3://cs3.cern.ch:443/,AWS-S3-SSL,17172.0,true,CERN_OS_1,CERN_ObjectStoreKey,ACTIVE]     |
|[,CERN_ObjectStoreKey.pub,/atlas_eventservice,41.0,eventservice,s3://cs3.cern.ch:443/,AWS-S3-SSL,17172.0,true,CERN_OS_1,CERN_ObjectStoreKey,ACTIVE]       |
|[CERN-PROD_LOGS_old,CERN_ObjectStoreKey.pub,/atlas_logs,42.0,logs,s3://cs3.cern.ch:443/,AWS-S3-SSL,17172.0,true,CERN_OS_1,CERN_ObjectStoreKey,ACTIVE]     |
|[,CERN_ObjectStoreKey.pub,/atlas_eventservice,41.0,eventservice,s3://cs3.cern.ch:443/,AWS-S3-SSL,17172.0,true,CERN_OS_1,CERN_ObjectStoreKey,ACTIVE]       |
|[CERN-PROD_LOGS_old,CERN_ObjectStoreKey.pub,/atlas_logs,42.0,logs,s3://cs3.cern.ch:443/,AWS-S3-SSL,17172.0,true,CERN_OS_1,CERN_ObjectStoreKey,ACTIVE]     |
|[,CERN_ObjectStoreKey.pub,/atlas_eventservice,41.0,eventservice,s3://cs3.cern.ch:443/,AWS-S3-SSL,17172.0,true,CERN_OS_1,CERN_ObjectStoreKey,ACTIVE]       |
|[CERN-PROD_LOGS_old,CERN_ObjectStoreKey.pub,/atlas_logs,42.0,logs,s3://cs3.cern.ch:443/,AWS-S3-SSL,17172.0,true,CERN_OS_1,CERN_ObjectStoreKey,ACTIVE]     |
|[,CERN_ObjectStoreKey.pub,/atlas_eventservice,41.0,eventservice,s3://cs3.cern.ch:443/,AWS-S3-SSL,17172.0,true,CERN_OS_1,CERN_ObjectStoreKey,ACTIVE]       |
|[CERN-PROD_LOGS_old,CERN_ObjectStoreKey.pub,/atlas_logs,42.0,logs,s3://cs3.cern.ch:443/,AWS-S3-SSL,17172.0,true,CERN_OS_1,CERN_ObjectStoreKey,ACTIVE]     |
|[,CERN_ObjectStoreKey.pub,/atlas_eventservice,41.0,eventservice,s3://cs3.cern.ch:443/,AWS-S3-SSL,17172.0,true,CERN_OS_1,CERN_ObjectStoreKey,ACTIVE]       |
|[CERN-PROD_LOGS_old,CERN_ObjectStoreKey.pub,/atlas_logs,42.0,logs,s3://cs3.cern.ch:443/,AWS-S3-SSL,17172.0,true,CERN_OS_1,CERN_ObjectStoreKey,ACTIVE]     |
|[,CERN_ObjectStoreKey.pub,/atlas_eventservice,41.0,eventservice,s3://cs3.cern.ch:443/,AWS-S3-SSL,17172.0,true,CERN_OS_1,CERN_ObjectStoreKey,ACTIVE]       |
|[CERN-PROD_LOGS_old,CERN_ObjectStoreKey.pub,/atlas_logs,42.0,logs,s3://cs3.cern.ch:443/,AWS-S3-SSL,17172.0,true,CERN_OS_1,CERN_ObjectStoreKey,ACTIVE]     |
|[,BNL_ObjectStoreKey.pub,/atlas_eventservice,2.0,eventservice,s3://cephgw.usatlas.bnl.gov:8443/,AWS-S3,17113.0,false,BNL_OS_0,BNL_ObjectStoreKey,ACTIVE]  |
|[BNL-ATLAS_LOGS_old,BNL_ObjectStoreKey.pub,/atlas_logs,3.0,logs,s3://cephgw.usatlas.bnl.gov:8443/,AWS-S3,17113.0,false,BNL_OS_0,BNL_ObjectStoreKey,ACTIVE]|
|[,,/atlas_logs,1.0,http,http://cephgw.usatlas.bnl.gov:8443/,AWS-HTTP,17114.0,false,BNL_OS_0,,ACTIVE]                                                      |
|[,BNL_ObjectStoreKey.pub,/atlas_eventservice,2.0,eventservice,s3://cephgw.usatlas.bnl.gov:8443/,AWS-S3,17113.0,false,BNL_OS_0,BNL_ObjectStoreKey,ACTIVE]  |
|[BNL-ATLAS_LOGS_old,BNL_ObjectStoreKey.pub,/atlas_logs,3.0,logs,s3://cephgw.usatlas.bnl.gov:8443/,AWS-S3,17113.0,false,BNL_OS_0,BNL_ObjectStoreKey,ACTIVE]|
|[,BNL_ObjectStoreKey.pub,/atlas_eventservice,2.0,eventservice,s3://cephgw.usatlas.bnl.gov:8443/,AWS-S3,17113.0,false,BNL_OS_0,BNL_ObjectStoreKey,ACTIVE]  |
+----------------------------------------------------------------------------------------------------------------------------------------------------------+
```


## Snapshot for ``agis_metadata\raw\pandaqueue`` "queues:"

```python
df = sqlContext.read.json("/project/monitoring/archive/agis_metadata/raw/pandaqueue/2016/05/*")
df.select(explode("message.queues")).toDF("element").select("element.ce_endpoint","element.ce_flavour","element.ce_jobmanager","element.ce_name","element.ce_queue_maxcputime","element.ce_queue_maxwctime","element.ce_queue_name").distinct().show(100,False)


```

### Example output


```text

+---------------------------------------+-----------+-------------+--------------------------------------------------------+-------------------+------------------+-----------------+
|ce_endpoint                            |ce_flavour |ce_jobmanager|ce_name                                                 |ce_queue_maxcputime|ce_queue_maxwctime|ce_queue_name    |
+---------------------------------------+-----------+-------------+--------------------------------------------------------+-------------------+------------------+-----------------+
|gsiftp://jost.arnes.si:2811            |ARC-CE     |arc          |ARNES-CE-jost.arnes.si                                  |20160.0            |20160.0           |all              |
|clrccece01.in2p3.fr:8443               |CREAM-CE   |pbs          |IN2P3-LPC-CE-clrccece01.in2p3.fr                        |2880.0             |5760.0            |atlasana         |
|atlasce1.lnf.infn.it:8443              |CREAM-CE   |pbs          |INFN-FRASCATI-CE-atlasce1.lnf.infn.it                   |120.0              |240.0             |atlas_short      |
|gridgk05.racf.bnl.gov:2119             |GLOBUS     |condor       |BNL-ATLAS-CE-gridgk05.racf.bnl.gov                      |1440.0             |1440.0            |default          |
|gridgk01.racf.bnl.gov:2119             |GLOBUS     |condor       |BNL-ATLAS-CE-gridgk01.racf.bnl.gov                      |0.0                |0.0               |distr_analysis   |
|vm3.tier2.hep.manchester.ac.uk:8443    |CREAM-CE   |pbs          |UKI-NORTHGRID-MAN-HEP-CE-vm3.tier2.hep.manchester.ac.uk |2880.0             |4320.0            |long             |
|stremsel.nikhef.nl:8443                |CREAM-CE   |pbs          |NIKHEF-ELPROD-CE-stremsel.nikhef.nl                     |9.99999999E8       |9.99999999E8      |short            |
|cccreamceli05.in2p3.fr:8443            |CREAM-CE   |sge          |IN2P3-CC-CE-cccreamceli05.in2p3.fr                      |2880.0             |4320.0            |verylong         |
|cccreamceli01.in2p3.fr:8443            |CREAM-CE   |sge          |IN2P3-CC-CE-TEST-cccreamceli01.in2p3.fr                 |0.0                |0.0               |mc_long          |
|cream-ge-3-kit.gridka.de:8443          |CREAM-CE   |sge          |FZK-LCG2-CE-cream-ge-3-kit.gridka.de                    |7200.0             |9.99999999E8      |sl6              |
|atlas-cream02.na.infn.it:8443          |CREAM-CE   |pbs          |INFN-NAPOLI-ATLAS-CE-atlas-cream02.na.infn.it           |2880.0             |4320.0            |atlas            |
|ce1.triumf.ca:8443                     |CREAM-CE   |pbs          |TRIUMF-LCG2-CE-ce1.triumf.ca                            |5040.0             |5760.0            |himem            |
|atlas-cream01.na.infn.it:8443          |CREAM-CE   |pbs          |INFN-NAPOLI-ATLAS-CE-atlas-cream01.na.infn.it           |2880.0             |4320.0            |atlas_short      |
|creamce2.gina.sara.nl:8443             |CREAM-CE   |pbs          |SARA-MATRIX-CE-creamce2.gina.sara.nl                    |5760.0             |5760.0            |long             |
|creamce2.goegrid.gwdg.de:8443          |CREAM-CE   |pbs          |GoeGrid-CE-creamce2.goegrid.gwdg.de                     |5760.0             |2880.0            |atlasXL          |
|ce01.tier2.hep.manchester.ac.uk:8443   |CREAM-CE   |pbs          |UKI-NORTHGRID-MAN-HEP-CE-ce01.tier2.hep.manchester.ac.uk|4320.0             |5040.0            |long             |
|ce6.glite.ecdf.ed.ac.uk:8443           |CREAM-CE   |sge          |UKI-SCOTGRID-ECDF-CE-ce6.glite.ecdf.ed.ac.uk            |2592.0             |2880.0            |ecdf             |
|klomp.nikhef.nl:8443                   |CREAM-CE   |pbs          |NIKHEF-ELPROD-CE-klomp.nikhef.nl                        |9.99999999E8       |9.99999999E8      |himem            |
|grid.uibk.ac.at:8443                   |CREAM-CE   |pbs          |HEPHY-UIBK-CE-grid.uibk.ac.at                           |2880.0             |4320.0            |atlas            |
|net2.rc.fas.harvard.edu:2119           |GLOBUS     |lsf          |HU_ATLAS_Tier2-CE-net2.rc.fas.harvard.edu               |1440.0             |1440.0            |ATLAS_MCORE      |
|creamce01.ge.infn.it:8443              |CREAM-CE   |lsf          |INFN-GENOVA-CE-creamce01.ge.infn.it                     |9.99999999E8       |2880.0            |atlas            |
|ce08-lcg.cr.cnaf.infn.it:8443          |CREAM-CE   |lsf          |INFN-T1-CE-ce08-lcg.cr.cnaf.infn.it                     |46080.0            |5880.0            |atlas_bari       |
|heplnv147.pp.rl.ac.uk:2811             |ARC-CE     |arc          |UKI-SOUTHGRID-RALPP-CE-heplnv147.pp.rl.ac.uk            |259200.0           |259200.0          |grid             |
|ce09.pic.es:8443                       |CREAM-CE   |pbs          |pic-CE-ce09.pic.es                                      |9.99999999E8       |9.99999999E8      |rglong_sl6       |
|lcgceatlas.dnp.fmph.uniba.sk:8443      |CREAM-CE   |pbs          |FMPhI-UNIBA-CE-lcgceatlas.dnp.fmph.uniba.sk             |2880.0             |4320.0            |mcore            |
|ce-cream-iep-grid.saske.sk:8443        |CREAM-CE   |pbs          |IEPSAS-Kosice-CE-ce-cream-iep-grid.saske.sk             |21600.0            |43200.0           |mcore            |
|ceprod07.grid.hep.ph.ic.ac.uk:8443     |CREAM-CE   |sge          |UKI-LT2-IC-HEP-CE-ceprod07.grid.hep.ph.ic.ac.uk         |2940.0             |2940.0            |grid.q           |
|ce04.ncg.ingrid.pt:8443                |CREAM-CE   |sge          |NCG-INGRID-PT-CE-ce04.ncg.ingrid.pt                     |9.99999999E8       |9.99999999E8      |atlasgrid        |
|grid30.lal.in2p3.fr:8443               |CREAM-CE   |condor       |GRIF-CE-grid30.lal.in2p3.fr                             |0.0                |0.0               |multicore        |
|agcream1.atlas.unimelb.edu.au:8443     |CREAM-CE   |pbs          |Australia-ATLAS-CE-agcream1.atlas.unimelb.edu.au        |3120.0             |5760.0            |atlas            |
|atlasce3.lnf.infn.it:8443              |CREAM-CE   |pbs          |INFN-FRASCATI-CE-atlasce3.lnf.infn.it                   |120.0              |240.0             |atlas_short      |
|grid-emicream2.rzg.mpg.de:8443         |CREAM-CE   |sge          |MPPMU-CE-grid-emicream2.rzg.mpg.de                      |0.0                |0.0               |cream2-mc.q      |
|testwulf.hpcc.ttu.edu                  |GLOBUS     |sge          |TTU-TESTWULF-CE-testwulf.hpcc.ttu.edu                   |0.0                |0.0               |default          |
|cream2.farm.particle.cz:8443           |CREAM-CE   |pbs          |praguelcg2-CE-cream2.farm.particle.cz                   |9.99999999E8       |9.99999999E8      |gridatlas        |
|grid-cr1.desy.de:8443                  |CREAM-CE   |pbs          |DESY-HH-CE-grid-cr1.desy.de                             |3600.0             |5400.0            |mcore            |
|atlas-creamce-02.roma1.infn.it:8443    |CREAM-CE   |lsf          |INFN-ROMA1-CE-atlas-creamce-02.roma1.infn.it            |9.99999999E8       |9.99999999E8      |atlasglong       |
|juk.nikhef.nl:8443                     |CREAM-CE   |pbs          |NIKHEF-ELPROD-CE-juk.nikhef.nl                          |9.99999999E8       |9.99999999E8      |himem            |
|ce.le.infn.it:8443                     |CREAM-CE   |lsf          |INFN-LECCE-CE-ce.le.infn.it                             |9.99999999E8       |9.99999999E8      |atlas            |
|ce09.pic.es:8443                       |CREAM-CE   |pbs          |pic-CE-ce09.pic.es                                      |9.99999999E8       |9.99999999E8      |hmem_sl6         |
|lcg-ce01.icepp.jp:8443                 |CREAM-CE   |pbs          |TOKYO-LCG2-CE-lcg-ce01.icepp.jp                         |10080.0            |10080.0           |atlas            |
|kalkan1.ulakbim.gov.tr:8443            |CREAM-CE   |pbs          |TR-10-ULAKBIM-CE-kalkan1.ulakbim.gov.tr                 |1440.0             |1440.0            |atlas_mqueue     |
|ce301.cern.ch:8443                     |CREAM-CE   |lsf          |CERN-PROD-CE-ce301.cern.ch                              |17280.0            |17280.0           |grid_atlas       |
|lcgce12.jinr.ru:8443                   |CREAM-CE   |pbs          |JINR-LCG2-CE-lcgce12.jinr.ru                            |2880.0             |3060.0            |mc8atl           |
|gsiftp://atlas.triolith.nsc.liu.se:2811|ARC-CE     |arc          |NDGF-T1-CE-atlas.triolith.nsc.liu.se                    |0.0                |0.0               |wlcg             |
|svr009.gla.scotgrid.ac.uk:2811         |ARC-CE     |arc          |UKI-SCOTGRID-GLASGOW-CE-svr009.gla.scotgrid.ac.uk       |2880.0             |2880.0            |condor_q2d       |
|ifaece03.pic.es:8443                   |CREAM-CE   |pbs          |ifae-CE-ifaece03.pic.es                                 |4800.0             |5220.0            |at2ifae          |
|grid-ce.chpc.ac.za:8443                |CREAM-CE   |pbs          |ZA-CHPC-CE-grid-ce.chpc.ac.za                           |9.99999999E8       |9.99999999E8      |atlas            |
|grid-cr4.desy.de:8443                  |CREAM-CE   |pbs          |DESY-HH-CE-grid-cr4.desy.de                             |3600.0             |5400.0            |mcore            |
|arc-ce03.gridpp.rl.ac.uk:2811          |ARC-CE     |arc          |RAL-LCG2-CE-arc-ce03.gridpp.rl.ac.uk                    |6480.0             |7776.0            |grid3000M        |
|ce06.esc.qmul.ac.uk:8443               |CREAM-CE   |sge          |UKI-LT2-QMUL-CE-ce06.esc.qmul.ac.uk                     |5760.0             |5760.0            |sl6_lcg_1G_long  |
|ce-cream-iep-grid.saske.sk:8443        |CREAM-CE   |pbs          |IEPSAS-Kosice-CE-ce-cream-iep-grid.saske.sk             |2880.0             |4320.0            |atlas            |
|creamce1.itep.ru:8443                  |CREAM-CE   |pbs          |ITEP-CE-creamce1.itep.ru                                |7200.0             |7200.0            |atlas            |
|osgserv02.slac.stanford.edu:2119       |GLOBUS     |lsf          |WT2-CE-osgserv02.slac.stanford.edu                      |0.0                |0.0               |default          |
|ce203.cern.ch:8443                     |CREAM-CE   |lsf          |CERN-PROD-CE-ce203.cern.ch                              |17280.0            |17280.0           |grid_atlas       |
|dc2-grid-28.brunel.ac.uk:2811          |ARC-CE     |arc          |UKI-LT2-Brunel-CE-dc2-grid-28.brunel.ac.uk              |0.0                |0.0               |default          |
|ce01.hpc.utfsm.cl:8443                 |CREAM-CE   |pbs          |EELA-UTFSM-CE-ce01.hpc.utfsm.cl                         |4320.0             |4800.0            |atlmc1           |
|cream1.farm.particle.cz:8443           |CREAM-CE   |pbs          |praguelcg2-CE-cream1.farm.particle.cz                   |9.99999999E8       |9.99999999E8      |atlasmc          |
|creamce3.goegrid.gwdg.de:8443          |CREAM-CE   |pbs          |GoeGrid-CE-creamce3.goegrid.gwdg.de                     |5760.0             |2880.0            |atlasXL          |
|lpsc-ce2.in2p3.fr:8443                 |CREAM-CE   |pbs          |IN2P3-LPSC-CE-lpsc-ce2.in2p3.fr                         |34560.0            |5760.0            |atlasMC8         |
|ce02.tier2.hep.manchester.ac.uk:8443   |CREAM-CE   |pbs          |UKI-NORTHGRID-MAN-HEP-CE-ce02.tier2.hep.manchester.ac.uk|4320.0             |5040.0            |long             |
|gridgk02.racf.bnl.gov:2119             |GLOBUS     |condor       |BNL-ATLAS-CE-gridgk02.racf.bnl.gov                      |0.0                |0.0               |bnl-local        |
|cream2.bfg.uni-freiburg.de:8443        |CREAM-CE   |slurm        |UNI-FREIBURG-CE-cream2.bfg.uni-freiburg.de              |2.147483647E9      |5760.0            |grid_medium_mcore|
|cream2.ppgrid1.rhul.ac.uk:8443         |CREAM-CE   |pbs          |UKI-LT2-RHUL-CE-cream2.ppgrid1.rhul.ac.uk               |9.99999999E8       |9.99999999E8      |atlas            |
|grid-cr0.desy.de:8443                  |CREAM-CE   |pbs          |DESY-HH-CE-grid-cr0.desy.de                             |3120.0             |5760.0            |atlas            |
|t2-recas-ce01.na.infn.it:8443          |CREAM-CE   |pbs          |INFN-NAPOLI-ATLAS-CE-t2-recas-ce01.na.infn.it           |2880.0             |4320.0            |atlas_short      |
|gsiftp://arc01.hpc.ku.dk:2811          |ARC-CE     |arc          |NDGF-T1-CE-arc01.hpc.ku.dk                              |5760.0             |5760.0            |tier1            |
|ce07.esc.qmul.ac.uk:8443               |CREAM-CE   |sge          |UKI-LT2-QMUL-CE-ce07.esc.qmul.ac.uk                     |5760.0             |5760.0            |sl6_lcg_4G_long  |
|gsiftp://pikolit.ijs.si:2811           |ARC-CE     |arc          |SiGNET-CE-pikolit.ijs.si                                |0.0                |0.0               |batch            |
|creamce.gina.sara.nl:8443              |CREAM-CE   |pbs          |SARA-MATRIX-CE-creamce.gina.sara.nl                     |0.0                |0.0               |mediummc         |
|ce.yerphi-cluster.grid.am:8443         |CREAM-CE   |pbs          |AM-04-YERPHI-CE-ce.yerphi-cluster.grid.am               |9.99999999E8       |9.99999999E8      |atlas            |
|ce05.ific.uv.es:8443                   |CREAM-CE   |pbs          |IFIC-LCG2-CE-ce05.ific.uv.es                            |9.99999999E8       |9.99999999E8      |atlasMP          |
|ce06-lcg.cr.cnaf.infn.it:8443          |CREAM-CE   |lsf          |INFN-T1-CE-ce06-lcg.cr.cnaf.infn.it                     |46080.0            |5880.0            |mcore            |
|lcgce21.jinr.ru:8443                   |CREAM-CE   |pbs          |JINR-LCG2-CE-lcgce21.jinr.ru                            |2880.0             |3120.0            |atlas            |
|ce11.pic.es:8443                       |CREAM-CE   |pbs          |pic-CE-ce11.pic.es                                      |2.147483647E9      |6420.0            |mcore_sl6_atlas  |
|ce11.pic.es:8443                       |CREAM-CE   |pbs          |pic-CE-ce11.pic.es                                      |9.99999999E8       |9.99999999E8      |hmem_sl6         |
|gsiftp://arc04.ihep.ac.cn:2811         |ARC-CE     |ARC          |BEIJING-LCG2-CE-arc04.ihep.ac.cn                        |370.0              |370.0             |cpuII            |
|gorgon03.westgrid.ca:8443              |CREAM-CE   |pbs          |CA-VICTORIA-WESTGRID-T2-CE-gorgon03.westgrid.ca         |2.147483647E9      |1440.0            |atlas-mcore      |
|gk01.atlas-swt2.org:2119               |GLOBUS     |pbs          |SWT2_CPB-CE-gk01.atlas-swt2.org                         |0.0                |0.0               |default          |
|ceprod05.grid.hep.ph.ic.ac.uk:8443     |CREAM-CE   |sge          |UKI-LT2-IC-HEP-CE-ceprod05.grid.hep.ph.ic.ac.uk         |9.99999999E8       |9.99999999E8      |grid.q           |
|grid-cr3.desy.de:8443                  |CREAM-CE   |pbs          |DESY-HH-CE-grid-cr3.desy.de                             |3120.0             |5760.0            |atlas            |
|uct2-gk.mwt2.org:9619                  |HTCONDOR-CE|condor       |MWT2-CE-HTCONDOR-CE-uct2-gk.mwt2.org                    |1440.0             |1440.0            |default          |
|f-arc01.grid.sinica.edu.tw:2811        |ARC-CE     |arc          |TW-FTT-CE-f-arc01.grid.sinica.edu.tw                    |0.0                |0.0               |atlas            |
|svr011.gla.scotgrid.ac.uk:2811         |ARC-CE     |arc          |UKI-SCOTGRID-GLASGOW-CE-svr011.gla.scotgrid.ac.uk       |2880.0             |2880.0            |condor_q2d       |
|cream02.grid.cyf-kr.edu.pl:8443        |CREAM-CE   |pbs          |CYFRONET-LCG2-CE-cream02.grid.cyf-kr.edu.pl             |3120.0             |5760.0            |atlas            |
|ce07-lcg.cr.cnaf.infn.it:8443          |CREAM-CE   |lsf          |INFN-T1-CE-ce07-lcg.cr.cnaf.infn.it                     |5760.0             |5880.0            |atlas            |
|juk.nikhef.nl:8443                     |CREAM-CE   |pbs          |NIKHEF-ELPROD-CE-juk.nikhef.nl                          |237.0              |240.0             |short            |
|arc-ce04.gridpp.rl.ac.uk:2811          |ARC-CE     |arc          |RAL-LCG2-CE-arc-ce04.gridpp.rl.ac.uk                    |6480.0             |7776.0            |grid3000M        |
|f-arc01.grid.sinica.edu.tw:2811        |ARC-CE     |arc          |TW-FTT-CE-f-arc01.grid.sinica.edu.tw                    |0.0                |0.0               |mcore            |
|cream2.ppgrid1.rhul.ac.uk:8443         |CREAM-CE   |pbs          |UKI-LT2-RHUL-CE-cream2.ppgrid1.rhul.ac.uk               |2880.0             |3600.0            |atlaspil         |
|hpc-arc.ecdf.ed.ac.uk:2811             |ARC-CE     |arc          |UKI-SCOTGRID-ECDF-CE-ARC-CE-hpc-arc.ecdf.ed.ac.uk       |0.0                |0.0               |short            |
|ifaece02.pic.es:8443                   |CREAM-CE   |pbs          |ifae-CE-ifaece02.pic.es                                 |4800.0             |5220.0            |at2ifae_analy    |
|ce1.grid.lebedev.ru:8443               |CREAM-CE   |pbs          |ru-Moscow-FIAN-LCG2-CE-ce1.grid.lebedev.ru              |2880.0             |4320.0            |atlasmc          |
|recas-ce-02.cs.infn.it:8443            |CREAM-CE   |pbs          |INFN-COSENZA-CE-recas-ce-02.cs.infn.it                  |2880.0             |4320.0            |atlas            |
|ce6.glite.ecdf.ed.ac.uk:8443           |CREAM-CE   |sge          |UKI-SCOTGRID-ECDF-CE-ce6.glite.ecdf.ed.ac.uk            |9.99999999E8       |9.99999999E8      |multicore        |
|gorgon02.westgrid.ca:8443              |CREAM-CE   |pbs          |CA-VICTORIA-WESTGRID-T2-CE-gorgon02.westgrid.ca         |2.147483647E9      |2880.0            |atlas            |
|c2papdata2.lrz.de:8443                 |ARC-CE     |arc          |LRZ-LMU-CE-c2papdata2.lrz.de                            |2880.0             |2880.0            |atlas            |
|tbit07.nipne.ro:8443                   |CREAM-CE   |pbs          |RO-07-NIPNE-CE-tbit07.nipne.ro                          |2880.0             |4320.0            |atlas            |
|gridgk04.racf.bnl.gov:2119             |GLOBUS     |condor       |BNL-ATLAS-CE-gridgk04.racf.bnl.gov                      |1440.0             |1440.0            |default          |
|grid-arcce0.desy.de:2811               |ARC-CE     |arc          |DESY-HH-CE-grid-arcce0.desy.de                          |3600.0             |5760.0            |grid             |
|ce03.tier2.hep.manchester.ac.uk:8443   |CREAM-CE   |pbs          |UKI-NORTHGRID-MAN-HEP-CE-ce03.tier2.hep.manchester.ac.uk|4320.0             |5040.0            |long             |
+---------------------------------------+-----------+-------------+--------------------------------------------------------+-------------------+------------------+-----------------+


```