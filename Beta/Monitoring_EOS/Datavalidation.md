## Disk status analysis for EOS

```
Purpose it to find status of each disk so that we can make conclusions about disk failure and etc.
It has been seen two sensors which provide details about disks and their status. These are-

    1. eosdisk sensor
    2. smart sensor

These are explained in details as below-
```

### eosdisk sensor-

```text
It has several metrics which provide details for each disk in the EOS instance. As we are concerned about the disk status, 
metric diskstate (MetricId-10522) reports the configured and actual state for each disk in the EOS instance.
```
[EOS sensor](https://metricmgr.cern.ch/sensor/31/)

[EOS Disk state (metricId 10522)](https://metricmgr.cern.ch/metricclass/211/)

``Location of eos data`` -  /project/itmon/archive/lemon/eos 

``note``- I looked for the data of metric 10522 at /project/itmon/archive/lemon/eos  (see for aug, sept and oct data) but no record found.


### smart sensor-

```text
It contains two metrics (metric ID 6130 and 6132) which provide details for smart counter status of each disk instance.
```

[smart sensor](https://metricmgr.cern.ch/sensor/18/)

[ChkSmartFailing (metric Id-6130)](https://metricmgr.cern.ch/metricclass/166/)

[ChkSmartSelftest (metric Id-6132)](https://metricmgr.cern.ch/metricclass/103/)



Metric 6130 and 6132 are found in the following hostgroup-

[click here](https://gitlab.cern.ch/awg/lemon-scripts/blob/master/Beta/Monitoring_EOS/hostgroup_location_6130_6132.md)



```We need to consider only eos hostgroup. So following is the snapshot of data for metric 6130 and 6132 -```

### ```Snap shot of data```
```text
+---------------+----------+----------------------------------------------------------------------------+---------------+---------+----------------+--------+---------------------+-----------------------+--------------------+-------------+------------------+------+-------+
|_corrupt_record|aggregated|body                                                                        |entity         |metric_id|metric_name     |producer|submitter_environment|submitter_host         |submitter_hostgroup |timestamp    |toplevel_hostgroup|type  |version|
+---------------+----------+----------------------------------------------------------------------------+---------------+---------+----------------+--------+---------------------+-----------------------+--------------------+-------------+------------------+------+-------+
|null           |null      |{"errcode":0.0,"errtext":"OK"}                                              |lxfssi45a04    |6132     |ChkSmartSelftest|lemon   |qa                   |lxfssi45a04.cern.ch    |eos/spare           |1475745158000|eos               |metric|3.0    |
|null           |null      |{"errcode":0.0,"errtext":"OK"}                                              |lxfsrd48c01    |6132     |ChkSmartSelftest|lemon   |production           |lxfsrd48c01.cern.ch    |eos/cms/storage     |1475661701000|eos               |metric|3.0    |
|null           |hourly    |{"errcode":0.0,"errtext":"OK"}                                              |lxfsre09a05    |6132     |ChkSmartSelftest|lemon   |production           |lxfsre09a05.cern.ch    |eos/cms/storage     |1475663400000|eos               |metric|3.0    |
|null           |null      |{"errcode":0.0,"errtext":"OK"}                                              |lxfsrk46c02    |6130     |ChkSmartFailing |lemon   |production           |lxfsrk46c02.cern.ch    |eos/atlas/storage   |1476120072000|eos               |metric|3.0    |
|null           |daily     |{"errcode":0.0,"errtext":"OK"}                                              |lxfsrd58c03    |6130     |ChkSmartFailing |lemon   |production           |lxfsrd58c03.cern.ch    |eos/alice/storage   |1475668800000|eos               |metric|3.0    |
|null           |null      |{"errcode":0.0,"errtext":"OK"}                                              |lxfsre15a09    |6132     |ChkSmartSelftest|lemon   |production           |lxfsre15a09.cern.ch    |eos/cms/storage     |1475663800000|eos               |metric|3.0    |
|null           |null      |{"errcode":0.0,"errtext":"OK"}                                              |lxfsre38c02    |6130     |ChkSmartFailing |lemon   |production           |lxfsre38c02.cern.ch    |eos/cms/storage     |1475660775000|eos               |metric|3.0    |
|null           |null      |{"errcode":0.0,"errtext":"OK"}                                              |lxfsrd14c03    |6132     |ChkSmartSelftest|lemon   |qa                   |lxfsrd14c03.cern.ch    |eos/alice/storage   |1475919822000|eos               |metric|3.0    |
|null           |null      |{"errcode":0.0,"errtext":"OK"}                                              |lxfsre13a05    |6132     |ChkSmartSelftest|lemon   |production           |lxfsre13a05.cern.ch    |eos/cms/storage     |1476182649000|eos               |metric|3.0    |
|null           |daily     |{"errcode":0.0,"errtext":"OK"}                                              |p06253977b86515|6132     |ChkSmartSelftest|lemon   |production           |p06253977b86515.cern.ch|eos/spare           |1475755200000|eos               |metric|3.0    |
|null           |null      |{"errcode":0.0,"errtext":"OK"}                                              |p05798818w87152|6132     |ChkSmartSelftest|lemon   |production           |p05798818w87152.cern.ch|eos/public/storage  |1475662896000|eos               |metric|3.0    |
|null           |hourly    |{"errcode":20.0,"errtext":"SMART_overall_health_status_failed_on_/dev/sg25"}|p05151113691315|6130     |ChkSmartFailing |lemon   |qa                   |p05151113691315.cern.ch|eos/user/storage    |1476001800000|eos               |metric|3.0    |
|null           |hourly    |{"errcode":0.0,"errtext":"OK"}                                              |lxfsre16c05    |6130     |ChkSmartFailing |lemon   |production           |lxfsre16c05.cern.ch    |eos/cms/storage     |1475663400000|eos               |metric|3.0    |
|null           |null      |{"errcode":0.0,"errtext":"OK"}                                              |lxfsrk43a04    |6130     |ChkSmartFailing |lemon   |production           |lxfsrk43a04.cern.ch    |eos/atlas/storage   |1475688049000|eos               |metric|3.0    |
|null           |hourly    |{"errcode":0.0,"errtext":"OK"}                                              |p05151207953124|6132     |ChkSmartSelftest|lemon   |production           |p05151207953124.cern.ch|eos/atlas/storage   |1475577000000|eos               |metric|3.0    |
|null           |hourly    |{"errcode":0.0,"errtext":"OK"}                                              |lxfsrf14b05    |6130     |ChkSmartFailing |lemon   |production           |lxfsrf14b05.cern.ch    |eos/lhcb/storage    |1475663400000|eos               |metric|3.0    |
|null           |null      |{"errcode":0.0,"errtext":"OK"}                                              |lxfsrf10b07    |6130     |ChkSmartFailing |lemon   |production           |lxfsrf10b07.cern.ch    |eos/atlas/storage   |1476206367000|eos               |metric|3.0    |
|null           |hourly    |{"errcode":0.0,"errtext":"OK"}                                              |eosaliceftp02  |6132     |ChkSmartSelftest|lemon   |production           |eosaliceftp02.cern.ch  |eos/alice/datamovers|1475685000000|eos               |metric|3.0    |
|null           |hourly    |{"errcode":0.0,"errtext":"OK"}                                              |p05614923d30886|6130     |ChkSmartFailing |lemon   |production           |p05614923d30886.cern.ch|eos/cms/storage     |1475663400000|eos               |metric|3.0    |
|null           |daily     |{"errcode":0.0,"errtext":"OK"}                                              |p05153074006960|6130     |ChkSmartFailing |lemon   |production           |p05153074006960.cern.ch|eos/alice/storage   |1476100800000|eos               |metric|3.0    |
|null           |daily     |{"errcode":0.0,"errtext":"OK"}                                              |p05614923v23967|6132     |ChkSmartSelftest|lemon   |production           |p05614923v23967.cern.ch|eos/user/storage    |1475496000000|eos               |metric|3.0    |
|null           |null      |{"errcode":0.0,"errtext":"OK"}                                              |lxfsrf15c06    |6130     |ChkSmartFailing |lemon   |production           |lxfsrf15c06.cern.ch    |eos/atlas/storage   |1475601611000|eos               |metric|3.0    |
|null           |null      |{"errcode":0.0,"errtext":"OK"}                                              |lxfsrd10c03    |6130     |ChkSmartFailing |lemon   |production           |lxfsrd10c03.cern.ch    |eos/alice/storage   |1476102189000|eos               |metric|3.0    |
|null           |null      |{"errcode":0.0,"errtext":"OK"}                                              |p05151113960384|6132     |ChkSmartSelftest|lemon   |production           |p05151113960384.cern.ch|eos/public/storage  |1476180812000|eos               |metric|3.0    |
|null           |hourly    |{"errcode":0.0,"errtext":"OK"}                                              |p05153074145270|6132     |ChkSmartSelftest|lemon   |production           |p05153074145270.cern.ch|eos/public/storage  |1475663400000|eos               |metric|3.0    |
|null           |null      |{"errcode":0.0,"errtext":"OK"}                                              |lxfsrd12c01    |6132     |ChkSmartSelftest|lemon   |production           |lxfsrd12c01.cern.ch    |eos/lhcb/storage    |1475663862000|eos               |metric|3.0    |
|null           |null      |{"errcode":0.0,"errtext":"OK"}                                              |lxfsrd04c01    |6130     |ChkSmartFailing |lemon   |production           |lxfsrd04c01.cern.ch    |eos/alice/storage   |1476102165000|eos               |metric|3.0    |
|null           |null      |{"errcode":0.0,"errtext":"OK"}                                              |p05153074266370|6130     |ChkSmartFailing |lemon   |production           |p05153074266370.cern.ch|eos/alice/storage   |1476188622000|eos               |metric|3.0    |
|null           |null      |{"errcode":0.0,"errtext":"OK"}                                              |p05798818d83796|6130     |ChkSmartFailing |lemon   |qa                   |p05798818d83796.cern.ch|eos/pps/storage     |1475662350000|eos               |metric|3.0    |
|null           |daily     |{"errcode":0.0,"errtext":"OK"}                                              |lxfsrf03c01    |6130     |ChkSmartFailing |lemon   |production           |lxfsrf03c01.cern.ch    |eos/alice/storage   |1475668800000|eos               |metric|3.0    |
|null           |hourly    |{"errcode":0.0,"errtext":"OK"}                                              |lxfsrk45a07    |6132     |ChkSmartSelftest|lemon   |production           |lxfsrk45a07.cern.ch    |eos/cms/storage     |1475663400000|eos               |metric|3.0    |
|null           |hourly    |{"errcode":0.0,"errtext":"OK"}                                              |p05799459p40139|6132     |ChkSmartSelftest|lemon   |production           |p05799459p40139.cern.ch|eos/cms/storage     |1475663400000|eos               |metric|3.0    |

+---------------+----------+----------------------------------------------------------------------------+---------------+---------+----------------+--------+---------------------+-----------------------+--------------------+-------------+------------------+------+-------+
```


``` possible values of errcode```

```text
+-------+
|errcode|
+-------+
|    6.0|
|   10.0|
|    0.0|
|   20.0|
+-------+
```



``` possible values of errtext```

```text
errtext has either OK or error message like-

|SMART_overall_health_status_failed_on_/dev/sg20            |
|SMART_overall_health_status_failed_on_/dev/sg22            |
|SMART_overall_health_status_failed_on_/dev/sg1_and_/dev/sg7|
|OK                                                         |
|SMART_overall_health_status_failed_on_/dev/sg34            |
|No_SMART_directives_available_for_storage
```


``` snap shot of errcode and errtext```

```text
+-------+-----------------------------------------------------------+
|errcode|errtext                                                    |
+-------+-----------------------------------------------------------+
|20.0   |SMART_overall_health_status_failed_on_/dev/sg41            |
|20.0   |SMART_overall_health_status_failed_on_/dev/sg47            |
|20.0   |SMART_overall_health_status_failed_on_/dev/sg48            |
|20.0   |SMART_overall_health_status_failed_on_/dev/sg49            |
|20.0   |No_SMART_directives_available_for_storage                  |
|20.0   |SMART_overall_health_status_failed_on_/dev/sg52            |
|20.0   |SMART_overall_health_status_failed_on_/dev/sg1_and_/dev/sg7|
|6.0    |OK                                                         |
|20.0   |SMART_overall_health_status_failed_on_/dev/sg10            |
|20.0   |SMART_overall_health_status_failed_on_/dev/sg11            |
|20.0   |SMART_overall_health_status_failed_on_/dev/sg12            |
|20.0   |SMART_overall_health_status_failed_on_/dev/sg13            |
|20.0   |SMART_overall_health_status_failed_on_/dev/sg16            |
|20.0   |SMART_overall_health_status_failed_on_/dev/sg17            |
|20.0   |SMART_overall_health_status_failed_on_/dev/sg19            |
|0.0    |OK                                                         |
|20.0   |SMART_overall_health_status_failed_on_/dev/sg20            |
|20.0   |SMART_overall_health_status_failed_on_/dev/sg22            |
|20.0   |SMART_overall_health_status_failed_on_/dev/sg24            |
|20.0   |SMART_overall_health_status_failed_on_/dev/sg25            |
|20.0   |SMART_overall_health_status_failed_on_/dev/sg27            |
|20.0   |SMART_overall_health_status_failed_on_/dev/sg0             |
|20.0   |SMART_overall_health_status_failed_on_/dev/sg1             |
|20.0   |SMART_overall_health_status_failed_on_/dev/sg3             |
|20.0   |SMART_overall_health_status_failed_on_/dev/sg5             |
|20.0   |SMART_overall_health_status_failed_on_/dev/sg31            |
|20.0   |SMART_overall_health_status_failed_on_/dev/sg9             |
|20.0   |SMART_overall_health_status_failed_on_/dev/sg34            |
|10.0   |OK                                                         |
|20.0   |SMART_overall_health_status_failed_on_/dev/sg38            |
+-------+-----------------------------------------------------------+
````



```Note```- From the data it seems that error code 20 means some error is there is the disk. Error code 0,10 and 60 means disk status is OK.

