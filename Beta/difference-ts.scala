import scala.util.parsing.json._
import org.apache.spark.sql.Row
import scala.math._
import org.apache.spark.sql.types._
import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf

val APP_NAME = "Sample Lemon Application"
val CPU_METRIC_ID = 9011
val DATA_FILE = "/project/itmon/archive/lemon/hadoop/2016-02/"
val RESULT_PATH = "percIdleResult201602"

def lemon-scala(){
    def main(args: String){
        val sqlCtxt = new org.apache.spark.sql.hive.HiveContext(sc)

        val schema = StructType(Array(
                    StructField("aggregated",StringType,true),
                    StructField("body",StringType,true),
                    StructField("entity",StringType,true),
                    StructField("metric_id",StringType,true),
                    StructField("metric_name",StringType,true),
                    StructField("producer",StringType,true),
                    StructField("submitter_environment",StringType,true),
                    StructField("submitter_host",StringType,true),
                    StructField("submitter_hostgroup",StringType,true),
                    StructField("timestamp",StringType,true),
                    StructField("toplevel_hostgroup",StringType,true),
                    StructField("type",StringType,true),
                    StructField("version",StringType,true))
        )
        val fulldata = sqlCtxt.jsonFile(DATA_FILE, schema)
        fulldata.registerTempTable("fulldata")

        val newcpulists = sqlCtxt.sql("""SELECT `timestamp`/1000 as ts FROM fulldata where metric_id="9011" and entity="p01001532965510" order by ts""")
        val rddone = newcpulists.rdd.zipWithIndex().map({case (k, v) => (v, k)})

        def mapper(x:Tuple2[Long, Row]): Tuple2[Long, Row] = {
            return Tuple2(x._1 + 1, x._2)
        }

        val rddtwo = rddone.map(mapper)

        val rdd = rddone.join(rddtwo)

        def mapdiff(x: Tuple2[Long, (Row, Row)]): Double = {
            return x._2._1.getDouble(0) - x._2._2.getDouble(0)
        }

        val finalrdd = rdd.map(mapdiff)
        finalrdd.coalesce(1, true).saveAsTextFile("firstdiff.csv")
    }
}
