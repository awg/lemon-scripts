Description
============

For each `entity` which are all `Metric_IDs` present for a given hostgroup e.g. 'bi', for a given time interval i.e TSstart < timestamp < TSend 
and for unaggregated lemon records.


How to execute script
=====================


```bash
spark-submit <script_name> file_name TSstart TSend
```


where

script_name is [entity_metric_count.py](entity_metric_count.py)

file_name   is input file from HDFS

TSstart     srating time stamp in ms

TSend       end time stamp in ms


#### `Example`:

```bash
spark-submit entity_metric_count.py /project/itmon/archive/lemon/bi/2016-07/part-r-00001 1467374400000 1468584000000
```

Input/Output
===========

### Input

The name of the HDFS folders to process. The HDFS folder should have LeMon records in standard [LeMon JSON format](../doc/lemon_json.md)

### Output 

A text file of format:

```
(entity , totalcount, [(metric_id1,count1),(metric_id2,count2),...])
```

output sample is as follows:

```
(u'b69210cc1e', 149, [(u'30081', 1), (u'9208', 4), (u'10001', 1), (u'4022', 3), (u'4005', 1), (u'10006', 1), (u'13342', 3), 
(u'901', 5), (u'5032', 2), (u'10005', 4), (u'13062', 1), (u'9201', 4), (u'6807', 1), (u'10007', 1), (u'13184', 1), (u'9200', 1), 
(u'33446', 1), (u'20002', 9), (u'9104', 9), (u'9025', 7), (u'4105', 1), (u'9103', 7), (u'9012', 4), (u'46', 1), (u'9023', 5), 
(u'902', 3), (u'9022', 3), (u'10004', 2), (u'30001', 1), (u'9031', 3), (u'4009', 1), (u'13001', 1), (u'13104', 8), (u'20047', 1), 
(u'9011', 6), (u'9013', 3), (u'4012', 3), (u'13183', 6), (u'9032', 5), (u'13173', 15), (u'4008', 5), (u'13056', 5)])

(u'b646c66bbb', 131, [(u'10007', 2), (u'9102', 5), (u'9012', 3), (u'10004', 1), (u'9104', 12), (u'9022', 1), (u'30081', 2), 
(u'901', 6), (u'13056', 3), (u'9103', 3), (u'9201', 4), (u'9025', 4), (u'6807', 1), (u'13342', 3), (u'9011', 4), (u'4105', 2),
(u'30003', 1), (u'5032', 2), (u'9208', 4), (u'13062', 2), (u'13173', 18), (u'4022', 2), (u'20003', 1), (u'4008', 2), (u'10005', 6),
(u'9013', 4), (u'13184', 1), (u'902', 3), (u'13104', 5), (u'9031', 4), (u'13183', 6), (u'9023', 5), (u'20002', 7), (u'4012', 1), (u'4009', 1)])
```









