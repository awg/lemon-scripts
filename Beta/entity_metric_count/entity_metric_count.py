# script find out count of all the different metric_ids present for each entity for a given hostgroup e.g.bi
# for inaggregated lemon record i.e.  aggregate = NUll 
# for a given time interval 
# In this exercise we need to group metric_id and their count like 
# entity1,total1, (metric_id1,count1), (metric_id2,count2)...... (total1 = SUM(count1+count2+...))
# entity2,total2, (metric_id1,count1), (metric_id2,count2)...... (total2 = SUM(count1+count2+...))
# .... for all entity

from pyspark import SparkContext, SparkConf, SQLContext, HiveContext
from pyspark.sql.types import *
import json
import time
import sys
import uuid
from pyspark.sql.functions import udf


def sum(x):
	total = 0
	for item in x[1]:
		total = total + item[1]
	return (x[0],total,x[1])



def main():
	try:
		fname = sys.argv[1]
		startTS = sys.argv[2]
		endTS = sys.argv[3]
	except IndexError:
		print "Usage: {0} FILE STARTTIMESTAMP(ms) ENDTIMESTAMP(ms)".format(sys.argv[0])
		sys.exit(1)
	conf = SparkConf()
	APP_NAME = ("entity metric count for a given timeinterval in file {0}").format(fname)
	conf = conf.setAppName(APP_NAME)
	sc = SparkContext(conf=conf)
	sc.setLogLevel("FATAL")
	hiveContext = HiveContext(sc)

	fileschema = StructType([
		StructField("aggregated", StringType()),
	        StructField("body", StringType()),
	        StructField("entity", StringType()),
	        StructField("metric_id", StringType()),
        	StructField("metric_name", StringType()),
	        StructField("producer", StringType()),
	        StructField("submitter_environment", StringType()),
	        StructField("submitter_host", StringType()),
	        StructField("submitter_hostgroup", StringType()),
	        StructField("timestamp", StringType()),
	        StructField("toplevel_hostgroup", StringType()),
	        StructField("type", StringType()),
	        StructField("version", StringType())
        ])

	df = hiveContext.read.json(fname,fileschema)
	#filter for unaggregated records and for hostgroup 'bi'
	df = df.filter("aggregated is not null").filter("toplevel_hostgroup = '%s'" % 'bi')
	#filter for a given time interval
	df1 = df.select(df.entity,df.metric_id,df.timestamp.cast("long"))
	df2 = df1.filter((startTS < df1.timestamp) & (df1.timestamp < endTS))
	#df3 = df2.select("entity","metric_id").map(lambda row:(row.entity,row.metric_id)).groupByKey().mapValues(list)
	df3 = df2.select("entity","metric_id").map(lambda row:((row.entity,row.metric_id),1)).reduceByKey(lambda x,y:x+y).map(lambda reduced:(reduced[0][0],(reduced[0][1],reduced[1]))).groupByKey().mapValues(list).map(sum)
	RESULT_PATH = "resultEntityMetricCount"
	df3.saveAsTextFile(RESULT_PATH)
	print "Result File: {0} in current directory".format(RESULT_PATH)


if __name__ == "__main__":
	main()
