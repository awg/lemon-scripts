from pyspark import SparkContext, SparkConf, SQLContext, HiveContext
from pyspark.sql.types import *
import json
import time

"""App Name: Any string"""
APP_NAME = "Sample Lemon Application"

"""CPU_METRIC_ID to be picked up along with the details from body to be aggregated"""
CPU_METRIC_ID = {"13184":["PercUser", "PercNice", "PercSystem", "PercIdle", "PercIOWait", "PercSoftIRQ", "PercIRQ", "PercSteal", "PercGuest", "PercGuestNice"]}

"""DATAFILE: Input file from hdfs"""
DATA_FILE = "/project/itmon/archive/lemon/bi/2016-03"

"""INTERVAL: Aggregation Interval (String) from implemented values"""
INTERVAL = "hourly" ## possible values are weekly, daily, hourly

"""RESULT_PATH: Output filename"""
RESULT_PATH = "result201603"

def main(sc):
    sqlCtxt = HiveContext(sc)
    
    fileschema = StructType([
        StructField("aggregated", StringType()),
        StructField("body", StringType()),
        StructField("entity", StringType()),
        StructField("metric_id", StringType()),
        StructField("metric_name", StringType()),
        StructField("producer", StringType()),
        StructField("submitter_environment", StringType()),
        StructField("submitter_host", StringType()),
        StructField("submitter_hostgroup", StringType()),
        StructField("timestamp", StringType()),
        StructField("toplevel_hostgroup", StringType()),
        StructField("type", StringType()),
        StructField("version", StringType())
    ])
    
    fulldata = sqlCtxt.jsonFile(DATA_FILE, fileschema)
    fulldata.registerTempTable("fulldata")

    def convertts(x):
        if INTERVAL == "weekly":
            #### Timestamp is getting truncated in the SQL to date. Other useful functions are, weekofyear(), 
            tsformat = "%Y-%m %U"
        elif INTERVAL == "hourly":
            ##########  date string + ',' + hour
            tsformat = "%Y-%m-%d %H"
        elif INTERVAL == "daily":
            ##########  date string
            tsformat = "%Y-%m-%d"
        else:
            tsformat = "%Y-%m-%d"
        tsformat += " +0000"
        return {'ts':time.strftime(tsformat, time.gmtime(x.ts)), 'body':x.body, 'entity':x.entity, 'metric_id':x.metric_id}
       
    def reduceSumPerc(x,y):
        """
            RETURN VALUES
            index 0: PercIdle min_percidle
            index 1: PercIdle max_percidle
            index 2: PercIdle average
            index 3: PercIdle Varience total
            index 4: Total number of samples
            index 5: PercIdle min_perciowait
            index 6: PercIdle max_perciowait
            index 7: PercIOWait average
            index 8: PercIOWait Varience Total
        """
        valuelist = []
        values = CPU_METRIC_ID["13184"]
        i = 0
        for value in values:
            num = x[i][4] + y[i][4]
            avg = x[i][2] + (y[i][2] - x[i][2])/num
            sd = x[i][3] + (y[i][2] - x[i][2]) * (y[i][2] - avg)
            valuelist.append([min(x[i][0], y[i][0]), max(x[i][1], y[i][1]), avg, sd, num])
            i += 1
        return valuelist
    
    def mapfunc(x):
        bodyjson = json.loads(x['body'])
        if x['metric_id'] == "13184":
            interfacename = ""
            keylist = [x['entity'], x['ts'], x['metric_id'], interfacename]
            valuelist = []
            values = CPU_METRIC_ID["13184"]
            for value in values:
                valuelist.append([bodyjson[value], bodyjson[value], bodyjson[value], 0, 1])
            return (tuple(keylist), tuple(valuelist))


    def map2func(x):
        return ((x[0][0], x[0][1]),((x[0][2:] + tuple(x[1]))))

    def reduce2func(x, y):
        if x[0] != CPU_METRIC_ID.keys()[0]:
            (x, y) = (y, x)
        return x + y


    def flatten(x):
        result = []
        for i in x:
            for y in i:
                if type(y) is list:
                    result.append(",".join(str(s) for s in y))
                else:
                    result.append(str(y))
        return result

   
    newcpulists = sqlCtxt.sql("""SELECT `timestamp`/1000 as ts, body, entity, metric_id 
            FROM fulldata 
            WHERE metric_id="""+ CPU_METRIC_ID.keys()[0])

    reducedresult = (newcpulists
                        .map(convertts)
                        .map(mapfunc)
                        .reduceByKey(reduceSumPerc)
                        .map(map2func)
                        .reduceByKey(reduce2func)
                        .map(flatten)
                        .map(lambda x: ",".join(x))
                    )
    
    reducedresult.coalesce(10, False).saveAsTextFile(RESULT_PATH)
    
    #print reducedresult.take(10)
    
if __name__ == "__main__":
    conf = (SparkConf().setAppName(APP_NAME). 
        setMaster("yarn-client")
        )
    sc = SparkContext()
    main(sc)
