A collection of scripts and ideas

Files details
---------------
* [lemon-agg-13184.py](lemon-agg-13184.py): pyspark script for extracting information of `metric_id` 13184 with parameterized argument and saving the output in csv format.[details](doc/lemon-agg-13184.py.md)
* [lemon-mult-format.py](lemon-mult-format.py): pyspark script for extracting multiple `metric_id`s and saving in a file in csv format.[details](doc/lemon-mult-format.py.md)
* [difference-ts.scala](difference-ts.scala): scala script to find out first difference in timestamps for a `metric_id` per entity. [details](doc/difference-ts.scala.md)
* [agg_13184.py](agg_13184.py): An alternate script to compute aggregates for 13184 (CPU) plugin based on user specified parameters (like time frame etc.)
* lemonplots/: matplotlib script in python
    * [timevscountperentity.py](lemonplots/timevscountperentity.py): script to generate plot for a series of entities, the count for each time-interval for a particular `metric_id` as generated in the csv file by spark.[details](doc/lemonplots/timevscountperentity.py.md)
    * [histocount.py](lemonplots/histocount.py): script to generate frequency distribution for the data in input file. [details](doc/lemonplots/histocount.py.md)
    * [matplotlibrc.rc](lemonplots/matplotlibrc.rc): empty matplotlib init conf file.
