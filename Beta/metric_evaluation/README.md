# Introduction

LeMon metrics need to be evaluated to understand the degre of coverage offered by them. This directory has a lemon script which can evaluate individual metrics and report various indices such as:
* Fraction of entities covered (day wise and total)
* Percentage of raw, hourly and daily aggregates for all entities and entity coverage for each category

### ``evaluate_metric.py``
The Pyspark script which does the work.

#### Usage
To get help, run

```bash
$ spark-submit evaluate_metric.py --help
```

#### Output
Text to stdout. Sample outputs (for June 2016) are in ``outputs/`` sub-directory
