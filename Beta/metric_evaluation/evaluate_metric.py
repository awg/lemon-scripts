# Python script which is used to evaluate the metric for the coverage of
# entities
# Works on raw HDFS lemon files (bi/2016-06)
# WARNING: Possibly exorbitant execution times (even in quick mode)
import calendar
from collections import defaultdict
import datetime
import optparse
import sys
import time

from pyspark import (HiveContext,
                     SparkConf,
                     SparkContext)
from pyspark.sql.functions import (countDistinct,
                                   sum as psum,
                                   udf)
from pyspark.sql.types import (IntegerType,
                               StringType,
                               StructField,
                               StructType)


def convert_timestamp_to_date(ts):
    return datetime.datetime(*time.gmtime(int(ts) / 1000)[:6]).strftime(
        "%d-%m-%Y")


def _get_ts(dt):
    d = datetime.datetime.strptime(dt, '%d-%m-%Y')
    return calendar.timegm(d.timetuple())


def raw_entry(agg):
    if agg == "raw":
        return 1
    return 0


def hour_entry(agg):
    if agg == "hourly":
        return 1
    return 0


def day_entry(agg):
    if agg == "daily":
        return 1
    return 0


def main(filename, metric_id, quick=False):
    APP_NAME = "Metric-Eval: {0}".format(metric_id)
    # Initialize an empty conf so that spark-submit options can fill it
    conf = SparkConf()
    # But we set master explicilty
    conf = conf.setMaster("yarn-client").setAppName(APP_NAME)
    # Build the spark context
    sc = SparkContext(conf=conf)
    sc.setLogLevel("FATAL")
    hiveContext = HiveContext(sc)

    # Read the file from HDFS
    fileschema = StructType([
        StructField("aggregated", StringType()),
        StructField("body", StringType()),
        StructField("entity", StringType()),
        StructField("metric_id", StringType()),
        StructField("metric_name", StringType()),
        StructField("producer", StringType()),
        StructField("submitter_environment", StringType()),
        StructField("submitter_host", StringType()),
        StructField("submitter_hostgroup", StringType()),
        StructField("timestamp", StringType()),
        StructField("toplevel_hostgroup", StringType()),
        StructField("type", StringType()),
        StructField("version", StringType())
    ])
    df = hiveContext.read.json(filename, schema=fileschema)
    df = df.filter("timestamp is not null")
    # Now process it
    # 1. Count the total number of entities and the number of entities covered
    ent_count = df.select(
        countDistinct("entity").alias("num_entities")).first().num_entities
    metric_ent_count = df.filter("metric_id='{0}'".format(metric_id)).select(
        countDistinct("entity").alias("num_entities")).first().num_entities

    # 2. Count the number of entries for metric and the breakdown by type
    df2 = df.fillna({"aggregated": "raw"})
    df3 = (df2.withColumn("raw", udf(raw_entry, IntegerType())("aggregated"))
           .withColumn("hour", udf(hour_entry, IntegerType())("aggregated"))
           .withColumn("day", udf(day_entry, IntegerType())("aggregated")))
    ent_type = (df3.filter("metric_id='{0}'".format(metric_id))
                .select(psum("raw").alias("num_raw"),
                        psum("hour").alias("num_hour"),
                        psum("day").alias("num_day")).first())
    tot = ent_type.num_raw + ent_type.num_hour + ent_type.num_day
    metric_breakdown = {"raw": ent_type.num_raw,
                        "hour": ent_type.num_hour,
                        "day": ent_type.num_day,
                        "total": tot}

    # Print Values
    print "Results for Metric: {0}".format(metric_id)
    print "Entity Coverage Index: {0:.4f} ({1}/{2})".format(
        float(metric_ent_count)/ent_count, metric_ent_count, ent_count)
    print "Raw entry fraction: {0:.4f} ({1}/{2})".format(
        float(metric_breakdown["raw"])/metric_breakdown["total"],
        metric_breakdown["raw"], metric_breakdown["total"])
    print "Hour entry fraction: {0:.4f} ({1}/{2})".format(
        float(metric_breakdown["hour"])/metric_breakdown["total"],
        metric_breakdown["hour"], metric_breakdown["total"])
    print "Day entry fraction: {0:.4f} ({1}/{2})".format(
        float(metric_breakdown["day"])/metric_breakdown["total"],
        metric_breakdown["day"], metric_breakdown["total"])
    if not quick:
        # More detailed stuff
        # 3. Count the number of entities covered per day and per type
        df4 = df.withColumn(
            "date",
            udf(convert_timestamp_to_date, StringType())("timestamp"))
        dt_ent = df4.groupBy("date").agg(
            countDistinct("entity").alias("num_ent")).collect()
        df5 = df4.filter("metric_id='{0}'".format(metric_id))
        dt_ent_met = df5.groupBy("date").agg(
            countDistinct("entity").alias("num_ent")).collect()
        # group by date and aggregated
        dt_ent_met_type = df5.groupBy("date", "aggregated").agg(
            countDistinct("entity").alias("num_ent")).collect()
        date_dict = defaultdict(dict)
        for x in dt_ent:
            date_dict[x.date]["entities"] = int(x.num_ent)
        for x in dt_ent_met:
            date_dict[x.date]["metric_entities"] = int(x.num_ent)
        for x in dt_ent_met_type:
            if x.aggregated is None:
                type_ = "raw"
            elif x.aggregated == "daily":
                type_ = "day"
            elif x.aggregated == "hourly":
                type_ = "hour"
            date_dict[x.date][type_] = int(x.num_ent)
        # Now sort the dict (after changing it to list of tuples by date
        dtitems = sorted(date_dict.items(), key=lambda x: _get_ts(x[0]))
        print "Daily Breakup:"
        for d in dtitems:
            print "Date: {0}: Entity Coverage: {1:.4f} ({2}/{3})".format(
                d[0], float(d[1].get(
                    "metric_entities", 0))/d[1].get("entities", 0),
                d[1].get("metric_entities", 0), d[1]["entities"])
            print "     Entity Coverage for raw: {0:.4f} ({1}/{2})".format(
                float(d[1].get("raw", 0))/d[1].get("entities", -1),
                d[1].get("raw", 0), d[1].get("entities", 0))
            print "     Entity Coverage for hour: {0:.4f} ({1}/{2})".format(
                float(d[1].get("hour", 0))/d[1].get("entities", -1),
                d[1].get("hour", 0), d[1].get("entities", 0))
            print "     Entity Coverage for day: {0:.4f} ({1}/{2})".format(
                float(d[1].get("day", 0))/d[1].get("entities", -1),
                d[1].get("day", 0), d[1].get("entities", 0))


if __name__ == '__main__':
    parser = optparse.OptionParser(
        usage="%prog [-options] FILE")
    parser.add_option("-m", "--metric-id", type="int", dest="metric_id",
                      help="Specify the metric ID to be processed. REQUIRED")
    parser.add_option(
        "-q", "--quick", dest="quick", action="store_true",
        help="Perform a quick evaluation (no daily breakup)")
    opts, args = parser.parse_args()
    # Sanity checking of options and arguments
    if not opts.metric_id:
        print "Please specify a metric ID"
        sys.exit(1)
    try:
        FILE_NAME = args[0]
    except IndexError:
        print("Please specify a file name")
        parser.print_help()
        sys.exit(1)
    if len(args) > 1:
        print("Please specify a single file name")
        sys.exit(1)
    main(FILE_NAME, opts.metric_id, quick=opts.quick)
