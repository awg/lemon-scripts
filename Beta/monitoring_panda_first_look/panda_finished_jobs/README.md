### Description

This will take input a ranges of dates and return all completed (with state finished only) jobs divided in daily folders

### How to execute script

```bash
spark-submit <script_name>  FILENAME RESULTFILE  STARTDATE ENDDATE
```

where

script_name is [daily_finished_jobs_dump.py](daily_finished_jobs_dump.py)

FILENAME   is input file from HDFS

RESULTFILE location where result is stored

STARTDATE     start date

ENDDATE       end date 


#### `Example`:

```bash
spark-submit daily_finished_jobs_dump.py /project/monitoring/archive/panda/enr/panda_job/2016/08/*  resultFinishedJobs_August_2016 3 5
```

### Input/Output

#### Input

The name of the HDFS folders to process. The HDFS folder should have PANDA records in standard [PANDA JSON format](../schema.md)

### Output 

A text file of format:

```

```