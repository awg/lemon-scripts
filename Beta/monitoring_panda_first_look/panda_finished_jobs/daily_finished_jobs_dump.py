# this script take as input a ranges of dates and return all completed (with state finished only) jobs divided in daily folders


from pyspark import SparkContext, SparkConf, SQLContext, HiveContext
from pyspark.sql.types import *
import json
import time
import sys
import uuid
from pyspark.sql.functions import udf,col
import datetime
from datetime import datetime
from pyspark.sql.types import StringType,IntegerType

def convert(ts):
        ts = ts/1000
        dt = datetime.utcfromtimestamp(ts)
        date = dt.strftime("%d")
        return int(date)


def main():
	try:
		fname = sys.argv[1]
		result_path = sys.argv[2]
		start_date = int(sys.argv[3])
		end_date = int(sys.argv[4])
	except IndexError:
		print "Usage: {0} FILENAME RESULTPATH STARTDATE ENDDATE".format(sys.argv[0])
		sys.exit(1)
	conf = SparkConf()
	APP_NAME = ("finished jobs' dump for a given range of date in file {0}").format(fname)
	conf = conf.setMaster("yarn-client").setAppName(APP_NAME)
	sc = SparkContext(conf=conf)
	sc.setLogLevel("FATAL")
	hiveContext = HiveContext(sc)
	df = hiveContext.read.json(fname)
	hourudf=udf(convert, IntegerType())
	dffinished = df.filter("metadata.timestamp is not null and data.status_name is not null and upper(data.status_name) LIKE '%FINISHED%'").withColumn("timestamp_date",hourudf("metadata.timestamp"))
	for date in range(start_date,end_date+1):
		dfresult = dffinished.filter(col("timestamp_date") == date)
		dffinal = dfresult.drop("timestamp_date")
		RESULT_PATH = result_path + "/" + str(date) + "/"
		dffinal.write.json(RESULT_PATH)

	print "Result File: {0} in directory".format(result_path)


if __name__ == "__main__":
	main()


