## Job -state definition in Panda

 There are 12 values in Panda describing different possible states of jobs and corresponding to jobStatus column in PandaDB (jobsDefined4, jobsWaiting4, jobsActive4 and jobsArchived4 tables):



``pending :`` job-record is injected to PandaDB by JEDI

``defined :`` kicked to start by bamboo or JEDI

``assigned :`` dispatchDBlock is subscribed to site to transfer input files to T2 or to prestage input files from T1 TAPE

``waiting :`` input files or software are not ready

``activated :`` waiting for pilot requests

``sent :`` sent to a pilot

``starting :`` the pilot is starting the job on a worker node

``running :`` running on a worker node

``holding :`` adding output files to datasets. Or waiting for job recovery

``transferring :`` output files are moving to the final destination

``finished :`` completed successfully

``failed :`` failed due to errors

``cancelled :`` manually killed

``merging :`` output files are being merged by merge jobs

``throttled :`` throttled to regulate WAN data access

``closed :`` terminated by the system before completing the allocated workload. E.g., killed to be reassigned to another site 



###  Normal sequence of job-states:

``pending -> defined -> assigned -> activated -> sent -> running -> holding -> transferring -> finished/failed``

## If input files are not available

``defined -> waiting``

## then, when files are ready

``-> assigned -> activated``

## state transitions


``pending -> defined :`` triggered by JEDI

``defined -> assigned/waiting :`` automatic

``assigned -> activated :`` received a callback for the dispatchDBlock. If jobs don't have input files or all input files are already available, the jobs get activated without a callback.

``activated -> sent :`` sent the job to a pilot

``sent -> running :`` the pilot received the job

``waiting -> assigned :`` received a callback for the destinationDBlock of upstream jobs

``running -> holding :`` received the final status report from the pilot

``holding -> transferring :`` added the output files to destinationDBlocks

``transferring -> finished/failed :`` received callbacks for the destinationDBlocks











