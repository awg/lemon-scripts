## 7.A) Count how many applications (_id) are there for each CERN queue_name


```python

df = sqlContext.read.json("/project/monitoring/archive/panda/enr/panda_job/2016/07/11.tmp")
df.filter("data.atlas_queue_name is not null and metadata._id is not null AND data.atlas_queue_name LIKE '%CERN%'").select("data.atlas_queue_name","metadata._id").groupBy("atlas_queue_name").agg(count("*").alias("count_id")).orderBy(col("count_id").desc()).show()
```

### Example output

```text

+--------------------+--------+
|    atlas_queue_name|count_id|
+--------------------+--------+
|     ANALY_CERN_SLC6|   71967|
|        CERN-PROD_HI|   21964|
|   CERN-PROD-preprod|   20408|
|       CERN-P1_MCORE|   13895|
|    ANALY_CERN_SHORT|   11204|
|           CERN-PROD|    9538|
|     CERN-PROD_CLOUD|    9439|
|     ANALY_CERN_TEST|     951|
|     CERN-PROD_SHORT|     906|
|CERN-P1_DYNAMIC_M...|     806|
|    ANALY_CERN_CLOUD|     339|
|ANALY_CERN_GLEXECDEV|     183|
|     CERN-PROD_MCORE|      42|
|   ANALY_CERN_XROOTD|       8|
|CERN-PROD-preprod...|       4|
|             CERN-P1|       4|
|CERN-PROD_PRESERV...|       2|
|CERN-PROD_CLOUD_M...|       2|
|CERN-P1_DYNAMIC_M...|       1|
+--------------------+--------+


```

## 7.B) How many applications are there for each CERN queue and batch_id. List out application ids along with total count. Result is ordered by count in descending order.

``` python

df = sqlContext.read.json("/project/monitoring/archive/panda/enr/panda_job/2016/07/11.tmp")
rddCount = df.filter("data.atlas_queue_name is not null AND data.batch_id is not null and metadata._id is not null AND data.atlas_queue_name LIKE '%CERN%'").select("data.atlas_queue_name","data.batch_id","metadata._id").map(lambda x:((x[0],x[1]),x[2])).groupByKey().mapValues(len)
rddList = df.filter("data.atlas_queue_name is not null AND data.batch_id is not null and metadata._id is not null AND data.atlas_queue_name LIKE '%CERN%'").select("data.atlas_queue_name","data.batch_id","metadata._id").map(lambda x:((x[0],x[1]),x[2])).groupByKey().mapValues(list)
rddFinal = rddCount.join(rddList)
rddFinal.sortBy(lambda x: x[1][0],False).take(5)

```

### Example output

```text
((u'ANALY_CERN_SHORT', u'31544421'), (47, [u'2919780701', u'2919780684', u'2919780712', u'2919780719', u'2919780724', u'2919780719', u'2919780724', u'2919780726', u'2919780724', u'2919773259', u'2919773272', u'2919773312', u'2919773325', u'2919773312', u'2919773393', u'2919780625', u'2919780643', u'2919780665', u'2919780625', u'2919780684', u'2919773325', u'2919773406', u'2919780590', u'2919773393', u'2919780612', u'2919780612', u'2919780625', u'2919780643', u'2919780665', u'2919780643', u'2919780684', u'2919780701', u'2919780712', u'2919780719', u'2919780701', u'2919780712', u'2919780726', u'2919780726', u'2919773312', u'2919773272', u'2919773393', u'2919773406', u'2919780590', u'2919773406', u'2919780590', u'2919780612', u'2919780665'])),
((u'ANALY_CERN_SHORT', u'31544420'), (47, [u'2919773245', u'2919773257', u'2919773270', u'2919773270', u'2919773310', u'2919773257', u'2919773323', u'2919780588', u'2919780624', u'2919780607', u'2919780624', u'2919780723', u'2919780664', u'2919780698', u'2919780716', u'2919780716', u'2919780698', u'2919780723', u'2919773391', u'2919773310', u'2919773404', u'2919773391', u'2919780588', u'2919780588', u'2919773404', u'2919780664', u'2919780664', u'2919780641', u'2919780710', u'2919780683', u'2919780710', u'2919780716', u'2919780710', u'2919773310', u'2919773270', u'2919773323', u'2919773391', u'2919773404', u'2919780607', u'2919780607', u'2919780624', u'2919780641', u'2919780641', u'2919780683', u'2919780683', u'2919780698', u'2919780723'])),
((u'ANALY_CERN_SHORT', u'31544422'), (45, [u'2919773313', u'2919773394', u'2919780592', u'2919780617', u'2919780649', u'2919780634', u'2919780673', u'2919780649', u'2919780693', u'2919780707', u'2919780714', u'2919780693', u'2919780722', u'2919780722', u'2919780725', u'2919780714', u'2919780725', u'2919780673', u'2919780707', u'2919780736', u'2919780736', u'2919780722', u'2919780736', u'2919773301', u'2919773326', u'2919773394', u'2919773326', u'2919773407', u'2919780617', u'2919780634', u'2919780592', u'2919773313', u'2919773326', u'2919773407', u'2919773394', u'2919780592', u'2919773407', u'2919780634', u'2919780649', u'2919780617', u'2919780673', u'2919780693', u'2919780707', u'2919780714', u'2919780725']))
((u'ANALY_CERN_SHORT', u'31544417'), (43, [u'2919773309', u'2919773322', u'2919773390', u'2919773322', u'2919773403', u'2919780587', u'2919780587', u'2919780622', u'2919780587', u'2919780652', u'2919780677', u'2919780715', u'2919780708', u'2919780715', u'2919780652', u'2919780708', u'2919780696', u'2919773230', u'2919773230', u'2919773244', u'2919773269', u'2919773309', u'2919773256', u'2919773269', u'2919780608', u'2919773403', u'2919780608', u'2919780652', u'2919780638', u'2919780696', u'2919773403', u'2919773309', u'2919773390', u'2919780608', u'2919780622', u'2919780638', u'2919780638', u'2919780622', u'2919780677', u'2919780677', u'2919780696', u'2919780708', u'2919780715']))
((u'ANALY_CERN_SHORT', u'31544410'), (43, [u'2919780697', u'2919780697', u'2919780697', u'2919773212', u'2919773218', u'2919773226', u'2919773218', u'2919773238', u'2919773238', u'2919773246', u'2919773226', u'2919773258', u'2919773258', u'2919773246', u'2919773311', u'2919773324', u'2919773311', u'2919773392', u'2919773405', u'2919773324', u'2919773405', u'2919780610', u'2919780589', u'2919780623', u'2919780610', u'2919780639', u'2919780681', u'2919780639', u'2919780681', u'2919773324', u'2919780589', u'2919773405', u'2919780610', u'2919780663', u'2919780663', u'2919780681', u'2919773392', u'2919780589', u'2919773392', u'2919780623', u'2919780639', u'2919780663', u'2919780623']))


```


## 7.C) Find all possible job status for a given applicationID

```python
df = sqlContext.read.json("/project/monitoring/archive/panda/enr/panda_job/2016/07/11.tmp")
app_id = '2919689701'
df.filter("metadata._id = '%s'" % str(app_id)).select("data.status_name").show()

```


### Example output

```text

+------------+
| status_name|
+------------+
|     RUNNING|
|     RUNNING|
|     HOLDING|
|    FINISHED|
|TRANSFERRING|
+------------+


```


## 7.D) List out how many applications are there for each CERN queue and batch_id. List out application ids along with total count, created_timestamp and status. Result is ordered by created_timestamp in descending order.

```python

df = sqlContext.read.json("/project/monitoring/archive/panda/enr/panda_job/2016/07/11.tmp")
rddCount = df.filter("data.atlas_queue_name is not null AND data.batch_id is not null and metadata._id is not null AND data.atlas_queue_name LIKE '%CERN%'").select("data.atlas_queue_name","data.batch_id","metadata._id").map(lambda x:((x[0],x[1]),x[2])).groupByKey().mapValues(len)
rddList = df.filter("data.atlas_queue_name is not null AND data.batch_id is not null and metadata._id is not null AND data.atlas_queue_name LIKE '%CERN%'").select("data.atlas_queue_name","data.batch_id","metadata._id","data.created_timestamp","data.status_name").map(lambda x:((x[0],x[1]),(x[2],x[3],x[4]))).groupByKey().mapValues(list)
rddFinal = rddCount.join(rddList)
rddFinal.sortBy(lambda x: x[1][1][0][1],False).take(10)


```


### Example output

```text

[((u'CERN-PROD-preprod', u'ce503.cern.ch#8408862.0#1468273968'), (1, [(u'2920134129', u'2016-07-11T21:54:51Z', u'RUNNING')])), 

((u'ANALY_CERN_TEST', u'ce503.cern.ch#8408895.0#1468274086'), (1, [(u'2920134124', u'2016-07-11T21:54:50Z', u'RUNNING')])),

((u'CERN-PROD-preprod', u'ce503.cern.ch#8408806.0#1468273839'), (1, [(u'2920133932', u'2016-07-11T21:54:35Z', u'RUNNING')])),

((u'CERN-PROD-preprod', u'ce503.cern.ch#8408644.0#1468273533'), (1, [(u'2920133796', u'2016-07-11T21:54:19Z', u'RUNNING')])), 

((u'CERN-PROD-preprod', u'ce503.cern.ch#8408767.0#1468273779'), (1, [(u'2920133710', u'2016-07-11T21:54:14Z', u'RUNNING')])), 

((u'CERN-PROD-preprod', u'ce503.cern.ch#8408693.0#1468273668'), (1, [(u'2920133520', u'2016-07-11T21:53:58Z', u'RUNNING')])), 

((u'CERN-PROD-preprod', u'ce503.cern.ch#8408813.0#1468273851'), (1, [(u'2920133419', u'2016-07-11T21:53:36Z', u'RUNNING')])), 

((u'ANALY_CERN_SHORT', u'31595833'), (1, [(u'2920133177', u'2016-07-11T21:53:11Z', u'RUNNING')])), 

((u'ANALY_CERN_TEST', u'ce503.cern.ch#8408809.0#1468273850'), (2, [(u'2920132262', u'2016-07-11T21:52:20Z', u'RUNNING'), (u'2920132262', u'2016-07-11T21:52:20Z', u'HOLDING')])), 

((u'CERN-PROD_CLOUD', u'aiatlas009.cern.ch#1579672.2#1468273922'), (1, [(u'2920132123', u'2016-07-11T21:52:06Z', u'RUNNING')]))]


```

## 7.E) List out how many applications are there for each CERN queue and batch_id. List out application ids along with total count, created_timestamp and status. Result is ordered by created_timestamp in descending order.

```python

df = sqlContext.read.json("/project/monitoring/archive/panda/enr/panda_job/2016/07/11.tmp")
rddCount = df.filter("data.atlas_queue_name is not null AND data.batch_id is not null and metadata._id is not null AND data.atlas_queue_name LIKE '%CERN%'").select("data.atlas_queue_name","data.batch_id","metadata._id","data.created_timestamp","data.status_name").map(lambda x: (x[2],(x[0],x[1],x[3],x[4]))).groupByKey().mapValues(len)
rddList = df.filter("data.atlas_queue_name is not null AND data.batch_id is not null and metadata._id is not null AND data.atlas_queue_name LIKE '%CERN%'").select("data.atlas_queue_name","data.batch_id","metadata._id","data.created_timestamp","data.status_name").map(lambda x: (x[2],(x[0],x[1],x[3],x[4]))).groupByKey().mapValues(list)
rddFinal = rddCount.join(rddList)
rddFinal.take(10)


```


### Example output

```text

[(u'2918619363', (1, [(u'CERN-PROD-preprod', u'ce503.cern.ch#8339077.0#1468127292', u'2016-07-09T21:41:44Z', u'FINISHED')])), 

(u'2919832253', (4, [(u'ANALY_CERN_SLC6', u'826582022', u'2016-07-11T13:54:29Z', u'HOLDING'), (u'ANALY_CERN_SLC6', u'826582022', u'2016-07-11T13:54:29Z', u'FINISHED'), (u'ANALY_CERN_SLC6', u'826582022', u'2016-07-11T13:54:29Z', u'RUNNING'), (u'ANALY_CERN_SLC6', u'826582022', u'2016-07-11T13:54:29Z', u'MERGING')])), 

(u'2919752302', (15, [(u'CERN-P1_MCORE', u'aipanda009.cern.ch#1757179.9#1468234652', u'2016-07-11T10:19:00Z', u'RUNNING'), (u'CERN-P1_MCORE', u'aipanda009.cern.ch#1757179.9#1468234652', u'2016-07-11T10:19:00Z', u'FINISHED'), (u'CERN-P1_MCORE', u'aipanda009.cern.ch#1757179.9#1468234652', u'2016-07-11T10:19:00Z', u'TRANSFERRING'), (u'CERN-P1_MCORE', u'aipanda009.cern.ch#1757179.9#1468234652', u'2016-07-11T10:19:00Z', u'TRANSFERRING'), (u'CERN-P1_MCORE', u'aipanda009.cern.ch#1757179.9#1468234652', u'2016-07-11T10:19:00Z', u'RUNNING'), (u'CERN-P1_MCORE', u'aipanda009.cern.ch#1757179.9#1468234652', u'2016-07-11T10:19:00Z', u'RUNNING'), (u'CERN-P1_MCORE', u'aipanda009.cern.ch#1757179.9#1468234652', u'2016-07-11T10:19:00Z', u'RUNNING'), (u'CERN-P1_MCORE', u'aipanda009.cern.ch#1757179.9#1468234652', u'2016-07-11T10:19:00Z', u'RUNNING'), (u'CERN-P1_MCORE', u'aipanda009.cern.ch#1757179.9#1468234652', u'2016-07-11T10:19:00Z', u'RUNNING'), (u'CERN-P1_MCORE', u'aipanda009.cern.ch#1757179.9#1468234652', u'2016-07-11T10:19:00Z', u'RUNNING'), (u'CERN-P1_MCORE', u'aipanda009.cern.ch#1757179.9#1468234652', u'2016-07-11T10:19:00Z', u'RUNNING'), (u'CERN-P1_MCORE', u'aipanda009.cern.ch#1757179.9#1468234652', u'2016-07-11T10:19:00Z', u'RUNNING'), (u'CERN-P1_MCORE', u'aipanda009.cern.ch#1757179.9#1468234652', u'2016-07-11T10:19:00Z', u'HOLDING'), (u'CERN-P1_MCORE', u'aipanda009.cern.ch#1757179.9#1468234652', u'2016-07-11T10:19:00Z', u'TRANSFERRING'), (u'CERN-P1_MCORE', u'aipanda009.cern.ch#1757179.9#1468234652', u'2016-07-11T10:19:00Z', u'RUNNING')])), 

(u'2919803118', (14, [(u'CERN-PROD', u'826521722', u'2016-07-11T12:53:16Z', u'TRANSFERRING'), (u'CERN-PROD', u'826521722', u'2016-07-11T12:53:16Z', u'RUNNING'), (u'CERN-PROD', u'826521722', u'2016-07-11T12:53:16Z', u'RUNNING'), (u'CERN-PROD', u'826521722', u'2016-07-11T12:53:16Z', u'TRANSFERRING'), (u'CERN-PROD', u'826521722', u'2016-07-11T12:53:16Z', u'RUNNING'), (u'CERN-PROD', u'826521722', u'2016-07-11T12:53:16Z', u'HOLDING'), (u'CERN-PROD', u'826521722', u'2016-07-11T12:53:16Z', u'TRANSFERRING'), (u'CERN-PROD', u'826521722', u'2016-07-11T12:53:16Z', u'TRANSFERRING'), (u'CERN-PROD', u'826521722', u'2016-07-11T12:53:16Z', u'RUNNING'), (u'CERN-PROD', u'826521722', u'2016-07-11T12:53:16Z', u'RUNNING'), (u'CERN-PROD', u'826521722', u'2016-07-11T12:53:16Z', u'TRANSFERRING'), (u'CERN-PROD', u'826521722', u'2016-07-11T12:53:16Z', u'TRANSFERRING'), (u'CERN-PROD', u'826521722', u'2016-07-11T12:53:16Z', u'TRANSFERRING'), (u'CERN-PROD', u'826521722', u'2016-07-11T12:53:16Z', u'RUNNING')])), 

(u'2917963274', (1, [(u'CERN-PROD', u'824940629', u'2016-07-09T05:44:26Z', u'FINISHED')])),

 (u'2919634794', (6, [(u'CERN-PROD', u'826414698', u'2016-07-11T05:06:26Z', u'RUNNING'), (u'CERN-PROD', u'826414698', u'2016-07-11T05:06:26Z', u'RUNNING'), (u'CERN-PROD', u'826414698', u'2016-07-11T05:06:26Z', u'TRANSFERRING'), (u'CERN-PROD', u'826414698', u'2016-07-11T05:06:26Z', u'RUNNING'), (u'CERN-PROD', u'826414698', u'2016-07-11T05:06:26Z', u'HOLDING'), (u'CERN-PROD', u'826414698', u'2016-07-11T05:06:26Z', u'CANCELLED')])), 

(u'2919997233', (3, [(u'ANALY_CERN_SLC6', u'826822412', u'2016-07-11T18:08:02Z', u'FAILED'), (u'ANALY_CERN_SLC6', u'826822412', u'2016-07-11T18:08:02Z', u'HOLDING'), (u'ANALY_CERN_SLC6', u'826822412', u'2016-07-11T18:08:02Z', u'RUNNING')])), 

(u'2919764013', (3, [(u'ANALY_CERN_SHORT', u'31545193', u'2016-07-11T10:52:28Z', u'HOLDING'), (u'ANALY_CERN_SHORT', u'31545193', u'2016-07-11T10:52:28Z', u'RUNNING'), (u'ANALY_CERN_SHORT', u'31545193', u'2016-07-11T10:52:28Z', u'FINISHED')])), 

(u'2918093318', (1, [(u'CERN-PROD-preprod', u'ce503.cern.ch#8290175.0#1468057906', u'2016-07-09T09:51:34Z', u'FINISHED')])), 

(u'2918036520', (7, [(u'CERN-PROD_HI', u'ce503.cern.ch#8286141.0#1468054017', u'2016-07-09T08:21:40Z', u'TRANSFERRING'), (u'CERN-PROD_HI', u'ce503.cern.ch#8286141.0#1468054017', u'2016-07-09T08:21:40Z', u'TRANSFERRING'), (u'CERN-PROD_HI', u'ce503.cern.ch#8286141.0#1468054017', u'2016-07-09T08:21:40Z', u'TRANSFERRING'), (u'CERN-PROD_HI', u'ce503.cern.ch#8286141.0#1468054017', u'2016-07-09T08:21:40Z', u'TRANSFERRING'), (u'CERN-PROD_HI', u'ce503.cern.ch#8286141.0#1468054017', u'2016-07-09T08:21:40Z', u'TRANSFERRING'), (u'CERN-PROD_HI', u'ce503.cern.ch#8286141.0#1468054017', u'2016-07-09T08:21:40Z', u'FAILED'), (u'CERN-PROD_HI', u'ce503.cern.ch#8286141.0#1468054017', u'2016-07-09T08:21:40Z', u'TRANSFERRING')]))]

```

