###  [distinct_vo_host_name](https://gitlab.cern.ch/awg/lemon-scripts/blob/master/Beta/monitoring_panda_first_look/distinct_vo_host_name.md)
- vo_name has only two values one is null and another is atlas.
- vo_name is null whenever site_name is null and atlas otherwise.It might be most likely this value is null when the job is not yet started.
