## 6.A Find all possible job status for a given applicationID

```python
df = sqlContext.read.json("/project/monitoring/archive/panda/enr/panda_job/2016/07/11.tmp")
app_id = '2919689701'
df.filter("metadata._id = '%s'" % str(app_id)).select("data.status_name").show()

```


### Example output

```text

+------------+
| status_name|
+------------+
|     RUNNING|
|     RUNNING|
|     HOLDING|
|    FINISHED|
|TRANSFERRING|
+------------+


```

## 6.B Find all possible application-id, job statuses for a given batch ID:

```python
df = sqlContext.read.json("/project/monitoring/archive/panda/enr/panda_job/2016/07/11.tmp")
batch_Id = '31544421'
df.filter("data.batch_id = '%s'" % str(batch_Id)).filter("metadata._id is not null and data.status_name is not null").select("metadata._id","data.status_name").orderBy("_id").show(100)

```


### Example output (printed them all)

```text

+----------+-----------+
|       _id|status_name|
+----------+-----------+
|2919773259|    HOLDING|
|2919773272|    RUNNING|
|2919773272|   FINISHED|
|2919773312|    HOLDING|
|2919773312|    RUNNING|
|2919773312|   FINISHED|
|2919773325|    HOLDING|
|2919773325|   FINISHED|
|2919773393|    RUNNING|
|2919773393|   FINISHED|
|2919773393|    HOLDING|
|2919773406|    RUNNING|
|2919773406|   FINISHED|
|2919773406|    HOLDING|
|2919780590|   FINISHED|
|2919780590|    HOLDING|
|2919780590|    RUNNING|
|2919780612|    RUNNING|
|2919780612|    HOLDING|
|2919780612|   FINISHED|
|2919780625|    RUNNING|
|2919780625|    HOLDING|
|2919780625|   FINISHED|
|2919780643|   FINISHED|
|2919780643|    RUNNING|
|2919780643|    HOLDING|
|2919780665|    RUNNING|
|2919780665|    HOLDING|
|2919780665|   FINISHED|
|2919780684|   FINISHED|
|2919780684|    HOLDING|
|2919780684|    RUNNING|
|2919780701|    HOLDING|
|2919780701|   FINISHED|
|2919780701|    RUNNING|
|2919780712|   FINISHED|
|2919780712|    HOLDING|
|2919780712|    RUNNING|
|2919780719|    HOLDING|
|2919780719|    RUNNING|
|2919780719|   FINISHED|
|2919780724|    HOLDING|
|2919780724|   FINISHED|
|2919780724|    RUNNING|
|2919780726|    HOLDING|
|2919780726|    RUNNING|
|2919780726|   FINISHED|
+----------+-----------+


```

`` observation:`` mainly a pattern is followed-

``  holding - running - finished ``
