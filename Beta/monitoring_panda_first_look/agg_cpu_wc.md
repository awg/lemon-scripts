## 1.A Find sum of wrap_cpu and wrap_wc for a atlas queue name

```python
from pyspark.sql.functions import sum
from pyspark.sql.functions import count
df = sqlContext.read.json("/project/monitoring/archive/panda/enr/panda_job/2016/07/11.tmp")
dfnew = df.groupBy("data.atlas_queue_name").agg(count("*").alias("count"),sum("data.wrap_cpu").alias("sum_cpu"), sum("data.wrap_wc").alias("sum_wc"))
dfnew.orderBy(dfnew["count"].desc()).show()
```

### Example output

```text

+--------------------+------+----------+----------+
|    atlas_queue_name| count|   sum_cpu|    sum_wc|
+--------------------+------+----------+----------+
|               BOINC|261281| 170508985| 252769530|
|     ANALY_BNL_SHORT| 96313|  87817734| 106204292|
|            BNL_PROD| 82277| 206280545| 263611116|
|      ANALY_MWT2_SL6| 79760|  87014340| 115007131|
|       ANALY_RAL_SL6| 73548|  64143514|  80032382|
|     ANALY_CERN_SLC6| 71967|  77673134|  95279985|
|           ANALY_FZK| 69834|  40137647|  45777242|
|      ANALY_BNL_LONG| 62430|  59122419|  73160265|
|             SLACXRD| 44493| 114109219| 172146278|
|    ORNL_Titan_MCORE| 38985|1123002880|8324577824|
|      ANALY_IN2P3-CC| 37267|  43108038|  58694154|
|     ANALY_AGLT2_SL6| 34162|  40682643|  58206700|
|         ANALY_TOKYO| 32706|  10832773|  14364535|
|   ANALY_GLASGOW_SL6| 31126|  42579032|  48631762|
|           ANALY_FZU| 28715|   7915124|  33891927|
|     ANALY_LANCS_SL6| 28586|  35719671|  38746650|
|      ANALY_SWT2_CPB| 28342|  23368869|  33474733|
|          ANALY_IFAE| 27643|  67634762|  71799245|
|ANALY_BU_ATLAS_Ti...| 27066|  16208265|  27487610|
|       ANALY_PIC_SL6| 26730|  20028750|  24297655|
+--------------------+------+----------+----------+


```

## 1.B Filter queue name by %CERN%

```python
dfnew = df.filter("upper(data.atlas_queue_name) LIKE '%CERN%'").groupBy("data.atlas_queue_name").agg(count("*").alias("count"),sum("data.wrap_cpu").alias("sum_cpu"), sum("data.wrap_wc").alias("sum_wc"))
dfnew.orderBy(dfnew["count"].desc()).show()


```

### Example output

```text
+--------------------+-----+---------+----------+
|    atlas_queue_name|count|  sum_cpu|    sum_wc|
+--------------------+-----+---------+----------+
|     ANALY_CERN_SLC6|71967| 77673134|  95279985|
|        CERN-PROD_HI|21964|667614177|1059088020|
|   CERN-PROD-preprod|20408|587256528| 712357160|
|       CERN-P1_MCORE|13895|711468150| 761906336|
|    ANALY_CERN_SHORT|11204|  3419547|   4909645|
|           CERN-PROD| 9538| 93108302| 130747171|
|     CERN-PROD_CLOUD| 9439|125599929| 172933321|
|     ANALY_CERN_TEST|  951|   120094|    288134|
|     CERN-PROD_SHORT|  906|  5317645|   6160887|
|CERN-P1_DYNAMIC_M...|  806| 34765562|  38212456|
|    ANALY_CERN_CLOUD|  339|    68482|    189963|
|ANALY_CERN_GLEXECDEV|  183|    49893|     77027|
|     CERN-PROD_MCORE|   42|   383188|    401792|
|   ANALY_CERN_XROOTD|    8|        0|         0|
|CERN-PROD-preprod...|    4|     5404|     15824|
|             CERN-P1|    4|        0|         0|
|CERN-PROD_PRESERV...|    2|        0|         0|
|CERN-PROD_CLOUD_M...|    2|     3945|     14872|
|CERN-P1_DYNAMIC_M...|    1|        0|         0|
+--------------------+-----+---------+----------+



```


## 1.C CPU utilization per CERN atlas queue

```python

dfnew = df.filter("upper(data.atlas_queue_name) LIKE '%CERN%'").groupBy("data.atlas_queue_name").agg(count("*").alias("count"),sum("data.wrap_cpu").alias("sum_cpu"), sum("data.wrap_wc").alias("sum_wc"))
dfut = dfnew.filter("sum_wc  != 0").withColumn("cpu_utilization(%)",dfnew.sum_cpu*100/dfnew.sum_wc)
dfut.orderBy(dfut["count"].desc()).show()
```

### Example 
```text
+--------------------+-----+---------+----------+------------------+
|    atlas_queue_name|count|  sum_cpu|    sum_wc|cpu_utilization(%)|
+--------------------+-----+---------+----------+------------------+
|     ANALY_CERN_SLC6|71967| 77673134|  95279985| 81.52093432844264|
|        CERN-PROD_HI|21964|667614177|1059088020|  63.0367036915402|
|   CERN-PROD-preprod|20408|587256528| 712357160| 82.43849588035305|
|       CERN-P1_MCORE|13895|711468150| 761906336| 93.38000176441635|
|    ANALY_CERN_SHORT|11204|  3419547|   4909645|  69.6495775152786|
|           CERN-PROD| 9538| 93108302| 130747171| 71.21247923597521|
|     CERN-PROD_CLOUD| 9439|125599929| 172933321| 72.62910830238437|
|     ANALY_CERN_TEST|  951|   120094|    288134|41.679912818341464|
|     CERN-PROD_SHORT|  906|  5317645|   6160887| 86.31297733589335|
|CERN-P1_DYNAMIC_M...|  806| 34765562|  38212456| 90.97965856996996|
|    ANALY_CERN_CLOUD|  339|    68482|    189963|  36.0501781925954|
|ANALY_CERN_GLEXECDEV|  183|    49893|     77027|  64.7733911485583|
|     CERN-PROD_MCORE|   42|   383188|    401792| 95.36974354890093|
|CERN-PROD-preprod...|    4|     5404|     15824|34.150657229524775|
|CERN-PROD_CLOUD_M...|    2|     3945|     14872|26.526358257127487|
+--------------------+-----+---------+----------+------------------+

```


## 1.D Atlas CERN queue ordered by cpu_utilization

```python

dfnew = df.filter("upper(data.atlas_queue_name) LIKE '%CERN%'").groupBy("data.atlas_queue_name").agg(count("*").alias("count"),sum("data.wrap_cpu").alias("sum_cpu"), sum("data.wrap_wc").alias("sum_wc"))
dfut = dfnew.filter("sum_wc  != 0").withColumn("cpu_utilization(%)",dfnew.sum_cpu*100/dfnew.sum_wc)
dfut.orderBy(dfut["cpu_utilization(%)"].desc()).show()
```

### Example Output
```text
+--------------------+-----+---------+----------+------------------+
|    atlas_queue_name|count|  sum_cpu|    sum_wc|cpu_utilization(%)|
+--------------------+-----+---------+----------+------------------+
|     CERN-PROD_MCORE|   42|   383188|    401792| 95.36974354890093|
|       CERN-P1_MCORE|13895|711468150| 761906336| 93.38000176441635|
|CERN-P1_DYNAMIC_M...|  806| 34765562|  38212456| 90.97965856996996|
|     CERN-PROD_SHORT|  906|  5317645|   6160887| 86.31297733589335|
|   CERN-PROD-preprod|20408|587256528| 712357160| 82.43849588035305|
|     ANALY_CERN_SLC6|71967| 77673134|  95279985| 81.52093432844264|
|     CERN-PROD_CLOUD| 9439|125599929| 172933321| 72.62910830238437|
|           CERN-PROD| 9538| 93108302| 130747171| 71.21247923597521|
|    ANALY_CERN_SHORT|11204|  3419547|   4909645|  69.6495775152786|
|ANALY_CERN_GLEXECDEV|  183|    49893|     77027|  64.7733911485583|
|        CERN-PROD_HI|21964|667614177|1059088020|  63.0367036915402|
|     ANALY_CERN_TEST|  951|   120094|    288134|41.679912818341464|
|    ANALY_CERN_CLOUD|  339|    68482|    189963|  36.0501781925954|
|CERN-PROD-preprod...|    4|     5404|     15824|34.150657229524775|
|CERN-PROD_CLOUD_M...|    2|     3945|     14872|26.526358257127487|
+--------------------+-----+---------+----------+------------------+

```