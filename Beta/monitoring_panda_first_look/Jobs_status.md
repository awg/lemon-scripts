## 2.A) job's status for each day

```python
import datetime
df = sqlContext.read.json("/project/monitoring/archive/panda/enr/panda_job/2016/*")
df.filter("metadata.timestamp is not null").select("metadata.timestamp","data.status_name").map(mapFunction1).map(lambda x : ((x[0],x[1]),1)).reduceByKey(lambda x,y:x+y).map(lambda reduced:(reduced[0][0],(reduced[0][1],reduced[1]))).groupByKey().mapValues(list).take(10)

def mapFunction1(x):
        ts = x.timestamp
        ts = ts/1000
        dt = datetime.utcfromtimestamp(ts)
        dayMon = dt.strftime("%d-%m")
        return (dayMon,x.status_name)		
```

### example output:

```
[('11-07', [(u'SENT', 13822), (u'ASSIGNED', 51602), (u'CANCELLED', 8462), (u'STARTING', 163820), (u'CLOSED', 11134), (u'PENDING', 9505), (u'RUNNING', 1596632), (u'DEFINED', 199499), (u'ACTIVATED', 303475), (u'MERGING', 108871), (u'FINISHED', 286956), (u'HOLDING', 341467), (u'FAILED', 98327), (u'TRANSFERRING', 124309)]), 
('12-07', [(u'CLOSED', 22152), (u'TRANSFERRING', 137481), (u'HOLDING', 575024), (u'FINISHED', 451728), (u'RUNNING', 3038598), (u'CANCELLED', 8231), (u'STARTING', 309079), (u'ACTIVATED', 544613), (u'MERGING', 189687), (u'SENT', 25020), (u'FAILED', 122128), (u'DEFINED', 299873), (u'ASSIGNED', 105707), (u'PENDING', 38552)]), 
('13-07', [(u'PENDING', 62010), (u'CANCELLED', 19508), (u'FAILED', 153267), (u'ACTIVATED', 761079), (u'CLOSED', 56257), (u'STARTING', 421758), (u'HOLDING', 757891), (u'TRANSFERRING', 170474), (u'ASSIGNED', 137635), (u'RUNNING', 4218090), (u'SENT', 33535), (u'MERGING', 236170), (u'FINISHED', 657096), (u'DEFINED', 378372)]), 
```

## 2.B) Job status for each site

```python
dfTest2.select("data.site_name","data.status_name").map(lambda x : ((x[0],x[1]),1)).reduceByKey(lambda x,y:x+y).map(lambda reduced:(reduced[0][0],(reduced[0][1],reduced[1]))).groupByKey().mapValues(list).take(10)
```


### Example output:

```
[(None, [(u'STARTING', 10910198), (u'FAILED', 4517014), (u'HOLDING', 19093301), (u'WAITING', 49), (u'FINISHED', 16021581), (u'MERGING', 4853615), (u'ASSIGNED', 3882498), (u'THROTTLED', 33), (u'CANCELLED', 594886), (u'RUNNING', 99694336), (u'CLOSED', 2572176), (u'TRANSFERRING', 6989226), (u'ACTIVATED', 18480716), (u'SENT', 827165), (u'DEFINED', 10893947), (u'PENDING', 1101988)]),

(u'AGLT2', [(u'HOLDING', 474), (u'FINISHED', 224), (u'CANCELLED', 25), (u'MERGING', 228), (u'STARTING', 5), (u'PENDING', 125), (u'SENT', 3), (u'FAILED', 122), (u'RUNNING', 2741), (u'ACTIVATED', 386), (u'TRANSFERRING', 160), (u'DEFINED', 595), (u'ASSIGNED', 108)]),

(u'ARNES', [(u'SENT', 8), (u'ACTIVATED', 21), (u'TRANSFERRING', 126), (u'CANCELLED', 2), (u'RUNNING', 2122), (u'STARTING', 683), (u'FINISHED', 50), (u'DEFINED', 60), (u'HOLDING', 103), (u'FAILED', 18), (u'MERGING', 24)]),

(u'Australia-ATLAS', [(u'FINISHED', 74), (u'ACTIVATED', 44), (u'FAILED', 58), (u'STARTING', 4), (u'SENT', 5), (u'HOLDING', 101), (u'RUNNING', 425), (u'MERGING', 21), (u'DEFINED', 80)]),

(u'BEIJING-LCG2', [(u'DEFINED', 3), (u'ACTIVATED', 1), (u'CANCELLED', 1), (u'FINISHED', 8), (u'TRANSFERRING', 20), (u'RUNNING', 251), (u'HOLDING', 9)]),

(u'BNL-ATLAS', [(u'TRANSFERRING', 579), (u'RUNNING', 17426), (u'DEFINED', 819), (u'MERGING', 1129), (u'ASSIGNED', 3365), (u'STARTING', 221), (u'FINISHED', 1027), (u'CLOSED', 16), (u'FAILED', 706), (u'HOLDING', 2728), (u'PENDING', 13), (u'CANCELLED', 83), (u'SENT', 229), (u'ACTIVATED', 1014)]),

(u'BNL-AWSEAST', [(u'CANCELLED', 1)]),

(u'BNL-AWSWEST', [(u'CANCELLED', 1)]), 

```


## 2.C) Job status for CERN atlas queue and host name

```python
df = sqlContext.read.json("/project/monitoring/archive/panda/enr/panda_job/2016/07/11.tmp")
df.filter("upper(data.atlas_queue_name) LIKE '%CERN%'").select("data.atlas_queue_name","data.wn_host_name","data.status_name").map(lambda x : ((x[0],x[1],x[2]),1)).reduceByKey(lambda x,y:x+y).map(lambda reduced:((reduced[0][0],reduced[0][1]),(reduced[0][2],reduced[1]))).groupByKey().mapValues(list).take(5)

```


### Example output

```text

((u'CERN-PROD_HI', u'slot1_7@b6b109b4d3ad.cern.ch'), [(u'FAILED', 1), (u'TRANSFERRING', 6)])
((u'CERN-PROD_HI', u'slot1_4@b60551b4ac.cern.ch'), [(u'TRANSFERRING', 7), (u'FAILED', 1)])
((u'CERN-PROD', u'b6413ec011.cern.ch'), [(u'FINISHED', 2), (u'TRANSFERRING', 10)])
((u'ANALY_CERN_SLC6', u'b649e0419a.cern.ch'), [(u'FINISHED', 1), (u'RUNNING', 15), (u'MERGING', 1), (u'HOLDING', 2)])
((u'CERN-PROD_HI', u'slot1_4@b6e617d8b6.cern.ch'), [(u'FINISHED', 2)])
((u'CERN-PROD-preprod', u'slot1_1@b60dc5dce0.cern.ch'), [(u'TRANSFERRING', 10), (u'FINISHED', 1)])
((u'CERN-PROD_HI', u'slot1_6@b6ccbff8b9.cern.ch'), [(u'FINISHED', 1)])
((u'CERN-PROD-preprod', u'slot1_3@b6af8a4b84.cern.ch'), [(u'TRANSFERRING', 10)])
((u'CERN-PROD-preprod', u'slot1_8@b6b4acfd94.cern.ch'), [(u'TRANSFERRING', 4), (u'FINISHED', 1)])
((u'CERN-PROD_SHORT', u'b672debb10.cern.ch'), [(u'FINISHED', 1)])
((u'CERN-PROD-preprod', u'slot1_6@b62b3b60e9.cern.ch'), [(u'FINISHED', 1), (u'TRANSFERRING', 14)])
((u'CERN-PROD_HI', u'slot1_5@b64f2c33e6.cern.ch'), [(u'TRANSFERRING', 9)])
((u'ANALY_CERN_TEST', u'slot1_4@b61eb81316a3.cern.ch'), [(u'RUNNING', 1), (u'FINISHED', 1), (u'HOLDING', 1)])
((u'CERN-PROD', u'b695fe5ad3.cern.ch'), [(u'RUNNING', 5), (u'CANCELLED', 2), (u'HOLDING', 2), (u'TRANSFERRING', 2)])
((u'ANALY_CERN_CLOUD', u'slot1_4@cern-atlas-c7ad0c0f-4f4f-488f-858e-754bde592d57.cern.ch'), [(u'RUNNING', 1), (u'HOLDING', 1), (u'FINISHED', 1)])
((u'CERN-PROD', u'b6a58e587a.cern.ch'), [(u'FINISHED', 1)])
((u'CERN-PROD_HI', u'slot1_4@b6685b862d.cern.ch'), [(u'FINISHED', 1)])
((u'CERN-PROD_HI', u'slot1_6@b671071eae.cern.ch'), [(u'FINISHED', 1)])
((u'CERN-PROD', u'b618964912.cern.ch'), [(u'FINISHED', 5), (u'TRANSFERRING', 8)])


```


## 2.D) Job status grouped by CERN atlas queue and Host name where job status are not Running,Finished and Closed

```python
df.filter("upper(data.atlas_queue_name) LIKE '%CERN%' AND data.status_name NOT IN ('FINISHED','RUNNING','CLOSED')").select("data.atlas_queue_name","data.wn_host_name","data.status_name").map(lambda x : ((x[0],x[1],x[2]),1)).reduceByKey(lambda x,y:x+y).map(lambda reduced:((reduced[0][0],reduced[0][1]),(reduced[0][2],reduced[1]))).groupByKey().mapValues(list).take(5)
```

### Example output

```text
((u'CERN-PROD', u'b6413ec011.cern.ch'), [(u'TRANSFERRING', 10)]), 
((u'ANALY_CERN_SLC6', u'b649e0419a.cern.ch'), [(u'HOLDING', 2), (u'MERGING', 1)]), 
((u'CERN-PROD-preprod', u'slot1_1@b60dc5dce0.cern.ch'), [(u'TRANSFERRING', 10)]), 
((u'CERN-PROD-preprod', u'slot1_8@b6b4acfd94.cern.ch'), [(u'TRANSFERRING', 4)]), 
((u'CERN-PROD-preprod', u'slot1_8@b651649637.cern.ch'), [(u'TRANSFERRING', 4)])
```


## 2.E) List atlas_queue for each site

```python

df = sqlContext.read.json("/project/monitoring/archive/panda/enr/panda_job/2016/07/11.tmp")
df.filter("data.site_name is not null and data.atlas_queue_name is not null").select("data.site_name","data.atlas_queue_name").map(lambda x : ((x[0],x[1]),1)).reduceByKey(lambda x,y:x+y).map(lambda reduced:(reduced[0][0],(reduced[0][1],reduced[1]))).groupByKey().mapValues(list).take(10)

```
### Example output

```text

[(u'UAM-LCG2', [(u'UAM-LCG2', 48), (u'UAM-LCG2_MCORE', 327), (u'ANALY_UAM', 180)]),
 
(u'INFN-FRASCATI', [(u'ANALY_INFN-FRASCATI', 456), (u'INFN-FRASCATI', 42), (u'INFN-FRASCATI_MCORE', 266), (u'ANALY_INFN-FRASCATI_PODtest', 1)]),

(u'GRIF-IRFU', [(u'GRIF-IRFU', 55), (u'ANALY_GRIF-IRFU', 6261), (u'GRIF-IRFU_MCORE', 241)]), 

(u'ITEP', [(u'ITEP_PROD', 42)]), 

(u'RRC-KI-T1', [(u'ANALY_RRC-KI-T1', 345), (u'RRC-KI-T1_MCORE', 993), (u'ANALY_RRC-KI-HPC', 116), (u'RRC-KI-T1', 2491), (u'RRC-KI-HPC2', 50)]),

(u'INFN-COSENZA', [(u'INFN-COSENZA-RECAS', 3)]), 

(u'INFN-LECCE', [(u'ANALY_INFN-LECCE', 88), (u'INFN-LECCE', 1103)]),

 (u'UKI-LT2-QMUL', [(u'ANALY_QMUL_HIMEM_SL6', 495), (u'ANALY_QMUL_SL6', 199), (u'UKI-LT2-QMUL_MCORE', 1212), (u'UKI-LT2-QMUL_HIMEM_SL6', 307), (u'UKI-LT2-QMUL_SL6', 98)]), 

(u'IN2P3-LPSC', [(u'IN2P3-LPSC', 46), (u'ANALY_LPSC', 298)]), 

(u'CERN-PROD', [(u'ANALY_CERN_SHORT', 1726), (u'ANALY_CERN_TEST', 131), (u'CERN-PROD_CLOUD', 2234), (u'CERN-PROD-preprod', 5165), (u'CERN-PROD_PRESERVATION', 1), (u'CERN-PROD_HI', 5221), (u'ANALY_CERN_GLEXECDEV', 34), (u'ANALY_CERN_SLC6', 6247), (u'CERN-PROD', 2477), (u'ANALY_CERN_CLOUD', 37), (u'CERN-PROD_MCORE', 2), (u'CERN-PROD_SHORT', 162)])]

```


## 2.F) List atlas_queue  (CERN) for each site

```python

df = sqlContext.read.json("/project/monitoring/archive/panda/enr/panda_job/2016/07/11.tmp")
df.filter("data.site_name is not null and data.atlas_queue_name is not null and upper(data.atlas_queue_name) LIKE '%CERN%'").select("data.site_name","data.atlas_queue_name").groupBy("site_name","atlas_queue_name").count().orderBy([col("site_name"),col("count")],ascending=[0, 0]).show()


```
### Example output

```text

+---------+--------------------+-----+
|site_name|    atlas_queue_name|count|
+---------+--------------------+-----+
|CERN-PROD|     ANALY_CERN_SLC6| 6247|
|CERN-PROD|        CERN-PROD_HI| 5221|
|CERN-PROD|   CERN-PROD-preprod| 5165|
|CERN-PROD|           CERN-PROD| 2477|
|CERN-PROD|     CERN-PROD_CLOUD| 2234|
|CERN-PROD|    ANALY_CERN_SHORT| 1726|
|CERN-PROD|     CERN-PROD_SHORT|  162|
|CERN-PROD|     ANALY_CERN_TEST|  131|
|CERN-PROD|    ANALY_CERN_CLOUD|   37|
|CERN-PROD|ANALY_CERN_GLEXECDEV|   34|
|CERN-PROD|     CERN-PROD_MCORE|    2|
|CERN-PROD|CERN-PROD_PRESERV...|    1|
|  CERN-P1|       CERN-P1_MCORE| 1766|
|  CERN-P1|CERN-P1_DYNAMIC_M...|  182|
+---------+--------------------+-----+


```


