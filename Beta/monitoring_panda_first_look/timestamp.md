# Timestamp

## 3.A) Difference between various timestamp

```python

df = sqlContext.read.json("/project/monitoring/archive/panda/enr/panda_job/2016/07/11.tmp")
df.select(df.data.created_timestamp,df.data.submitted_timestamp,df.data.task_created_timestamp).show()
```

### Example output

```text

--------------------+--------------------+----------------------+
|   created_timestamp| submitted_timestamp|task_created_timestamp|
+--------------------+--------------------+----------------------+
|2016-07-11T07:39:26Z|2016-07-11T07:39:26Z|  2016-07-11T07:39:26Z|
|2016-07-11T10:20:55Z|2016-07-11T10:20:55Z|  2016-07-11T10:20:55Z|
|2016-07-10T18:20:29Z|2016-07-10T18:20:29Z|  2016-07-10T18:20:29Z|
|2016-07-10T13:21:56Z|2016-07-10T13:21:56Z|  2016-07-10T13:21:56Z|
|2016-07-10T13:21:57Z|2016-07-10T13:21:57Z|  2016-07-10T13:21:57Z|
|2016-07-11T04:32:25Z|2016-07-11T04:32:25Z|  2016-07-11T04:32:25Z|
|2016-07-08T23:25:16Z|2016-07-08T23:25:16Z|  2016-07-08T23:25:16Z|
|2016-07-11T06:02:06Z|2016-07-11T06:02:06Z|  2016-07-11T06:02:06Z|
|2016-07-11T08:26:04Z|2016-07-11T08:26:04Z|  2016-07-11T08:26:04Z|
|2016-07-11T11:17:12Z|2016-07-11T11:17:12Z|  2016-07-11T11:17:12Z|


```

`Observation:`
created_timestamp, submitted_timestamp and task_created_timestamp are same


## 3.B) comparision with metadata.timestamp 

```python
from pyspark.sql.types import StringType
import datetime
from datetime import datetime
def convert(ts):
        ts = ts/1000
        dt = datetime.utcfromtimestamp(ts)
        dayMon = dt.strftime("%y-%m-%dT%H:%M:%S")
        return dayMon	

hourudf=udf(convert, StringType())
df2=df.withColumn("timestamp_datehour", hourudf("metadata.timestamp"))
df2.select(df2.data.created_timestamp,df2.data.submitted_timestamp,df2.data.task_created_timestamp,df2.timestamp_datehour).show()

```

### Example output

```text
|data[created_timestamp]|data[submitted_timestamp]|data[task_created_timestamp]|timestamp_datehour|
+-----------------------+-------------------------+----------------------------+------------------+
|   2016-07-11T07:39:26Z|     2016-07-11T07:39:26Z|        2016-07-11T07:39:26Z| 16-07-11T11:42:01|
|   2016-07-11T10:20:55Z|     2016-07-11T10:20:55Z|        2016-07-11T10:20:55Z| 16-07-11T11:42:01|
|   2016-07-10T18:20:29Z|     2016-07-10T18:20:29Z|        2016-07-10T18:20:29Z| 16-07-11T11:42:01|
|   2016-07-10T13:21:56Z|     2016-07-10T13:21:56Z|        2016-07-10T13:21:56Z| 16-07-11T11:42:01|
|   2016-07-10T13:21:57Z|     2016-07-10T13:21:57Z|        2016-07-10T13:21:57Z| 16-07-11T11:42:01|
|   2016-07-11T04:32:25Z|     2016-07-11T04:32:25Z|        2016-07-11T04:32:25Z| 16-07-11T11:42:01|
|   2016-07-08T23:25:16Z|     2016-07-08T23:25:16Z|        2016-07-08T23:25:16Z| 16-07-11T11:42:01|
|   2016-07-11T06:02:06Z|     2016-07-11T06:02:06Z|        2016-07-11T06:02:06Z| 16-07-11T11:42:01|
|   2016-07-11T08:26:04Z|     2016-07-11T08:26:04Z|        2016-07-11T08:26:04Z| 16-07-11T11:42:01|
|   2016-07-11T11:17:12Z|     2016-07-11T11:17:12Z|        2016-07-11T11:17:12Z| 16-07-11T11:42:01|
|   2016-07-11T09:04:29Z|     2016-07-11T09:04:29Z|        2016-07-11T09:04:29Z| 16-07-11T11:42:01|

```

## 3.C) Check whether created_timestamp and submitted_timestamp are different for some record

```python
import datetime
import time
import calendar
from pyspark.sql.functions import udf,col,count,lit,sum,when
from pyspark.sql.types import LongType

def _convert_dtime_to_tstamp(dt):
    """
    Convert a UTC datetime string of form DD-MM-YYYY HH:MM:SS
    to timestamp
    """
    try:
        d = datetime.datetime.strptime(dt, "%Y-%m-%dT%H:%M:%SZ")
    except ValueError:
        raise ValueError("Specify datetime as DD-MM-YYYY HH:MM:SS")
    return int(calendar.timegm(d.timetuple()))
convertdf = udf(_convert_dtime_to_tstamp,LongType())

df1 = df.filter("data.submitted_timestamp is not null AND data.created_timestamp is not null").select("metadata._id","data.submitted_timestamp","data.created_timestamp")

df2 = df1.withColumn("ts_created",convertdf("created_timestamp"))
df3 = df2.withColumn("ts_submitted",convertdf("submitted_timestamp"))

df4 = df3.groupBy("_id").agg(sum(when(col("ts_created")==col("ts_submitted"), lit(1)).otherwise(lit(0))).alias("same"),sum(when(col("ts_created")!=col("ts_submitted"), lit(1)).otherwise(lit(0))).alias("diff"))
df4.show()

```

### Example output

```text
+----------+----+----+
|       _id|same|diff|
+----------+----+----+
|2919962815|   1|   0|
|2919801561|  15|   0|
|2919730894|  11|   0|
|2919790690|  11|   0|
|2918140511|   9|   0|
|2919776885|  16|   0|
|2919953699|   3|   0|
|2916858643|  16|   0|
|2919930360|   9|   0|
|2919953905|   3|   0|
|2919889646|   6|   0|
|2919973958|  12|   0|
|2919751882|  10|   0|
|2919952690|   5|   0|
|2919816268|   9|   0|
|2919662944|  11|   0|
|2919933484|   3|   0|
|2912509062|  15|   0|
|2918616487|  15|   0|
|2919938434|   9|   0|
+----------+----+----+
```

```python
df3.agg(sum(when(col("ts_created")==col("ts_submitted"), lit(1)).otherwise(lit(0))).alias("same"),sum(when(col("ts_created")!=col("ts_submitted"), lit(1)).otherwise(lit(0))).alias("diff")).show()
```

```text
+-------+----+
|   same|diff|
+-------+----+
|2569982|   0|
+-------+----+
```



`` There is not even a single record where created_timestamp and submitted_timestamp are different``



## 3.D) Comparision among all possible timestamps

```python
dfjuly = sqlContext.read.json("/project/monitoring/archive/panda/enr/panda_job/2016/07/*")
import datetime
from pyspark.sql.functions import udf
from pyspark.sql.types import StringType

def convert(ts):
        ts = ts/1000
        dt = datetime.utcfromtimestamp(ts)
        dayMon = dt.strftime("%y-%m-%dT%H:%M:%S")
        return dayMon 
        
hourudf=udf(convert, StringType())

df2=dfjuly.withColumn("kafka_timestamp_datehour", hourudf("metadata.kafka_timestamp"))
df2=df2.withColumn("flume_timestamp_datehour", hourudf("metadata.flume_timestamp"))
df2=df2.withColumn("timestamp_datehour", hourudf("metadata.timestamp"))

df2.select("data.created_timestamp","data.job_exec_exit_timestamp","data.started_running_timestamp","data.submitted_timestamp","data.task_created_timestamp","flume_timestamp_datehour","kafka_timestamp_datehour","timestamp_datehour").show()

```

```text

+--------------------+-----------------------+-------------------------+--------------------+----------------------+------------------------+------------------------+------------------+
|   created_timestamp|job_exec_exit_timestamp|started_running_timestamp| submitted_timestamp|task_created_timestamp|flume_timestamp_datehour|kafka_timestamp_datehour|timestamp_datehour|
+--------------------+-----------------------+-------------------------+--------------------+----------------------+------------------------+------------------------+------------------+
|2016-07-11T07:39:26Z|                   null|     2016-07-11T08:02:56Z|2016-07-11T07:39:26Z|  2016-07-11T07:39:26Z|       16-07-11T11:40:09|       16-07-11T11:42:06| 16-07-11T11:42:01|
|2016-07-11T10:20:55Z|   2016-07-11T11:28:09Z|     2016-07-11T11:18:42Z|2016-07-11T10:20:55Z|  2016-07-11T10:20:55Z|       16-07-11T11:40:09|       16-07-11T11:42:06| 16-07-11T11:42:01|
|2016-07-10T18:20:29Z|                   null|     2016-07-11T00:21:10Z|2016-07-10T18:20:29Z|  2016-07-10T18:20:29Z|       16-07-11T11:40:09|       16-07-11T11:42:06| 16-07-11T11:42:01|
|2016-07-10T13:21:56Z|   2016-07-11T11:11:08Z|     2016-07-11T11:00:39Z|2016-07-10T13:21:56Z|  2016-07-10T13:21:56Z|       16-07-11T11:40:09|       16-07-11T11:42:06| 16-07-11T11:42:01|
|2016-07-10T13:21:57Z|   2016-07-11T11:13:26Z|     2016-07-11T11:02:53Z|2016-07-10T13:21:57Z|  2016-07-10T13:21:57Z|       16-07-11T11:40:09|       16-07-11T11:42:06| 16-07-11T11:42:01|
|2016-07-11T04:32:25Z|                   null|     2016-07-11T06:00:02Z|2016-07-11T04:32:25Z|  2016-07-11T04:32:25Z|       16-07-11T11:40:09|       16-07-11T11:42:06| 16-07-11T11:42:01|
|2016-07-08T23:25:16Z|                   null|     2016-07-09T13:13:43Z|2016-07-08T23:25:16Z|  2016-07-08T23:25:16Z|       16-07-11T11:40:09|       16-07-11T11:42:06| 16-07-11T11:42:01|
|2016-07-11T06:02:06Z|                   null|     2016-07-11T07:32:41Z|2016-07-11T06:02:06Z|  2016-07-11T06:02:06Z|       16-07-11T11:40:09|       16-07-11T11:42:06| 16-07-11T11:42:01|
|2016-07-11T08:26:04Z|                   null|     2016-07-11T09:04:43Z|2016-07-11T08:26:04Z|  2016-07-11T08:26:04Z|       16-07-11T11:40:09|       16-07-11T11:42:06| 16-07-11T11:42:01|
|2016-07-11T11:17:12Z|   2016-07-11T11:38:08Z|     2016-07-11T11:32:55Z|2016-07-11T11:17:12Z|  2016-07-11T11:17:12Z|       16-07-11T11:40:09|       16-07-11T11:42:06| 16-07-11T11:42:01|
|2016-07-11T09:04:29Z|                   null|     2016-07-11T09:04:47Z|2016-07-11T09:04:29Z|  2016-07-11T09:04:29Z|       16-07-11T11:40:09|       16-07-11T11:42:06| 16-07-11T11:42:01|
|2016-07-08T09:06:08Z|                   null|     2016-07-08T09:30:44Z|2016-07-08T09:06:08Z|  2016-07-08T09:06:08Z|       16-07-11T11:40:09|       16-07-11T11:42:06| 16-07-11T11:42:01|
|2016-07-10T13:26:18Z|                   null|     2016-07-11T08:02:05Z|2016-07-10T13:26:18Z|  2016-07-10T13:26:18Z|       16-07-11T11:40:09|       16-07-11T11:42:06| 16-07-11T11:42:01|
|2016-07-10T13:21:57Z|   2016-07-11T11:10:22Z|     2016-07-11T11:02:50Z|2016-07-10T13:21:57Z|  2016-07-10T13:21:57Z|       16-07-11T11:40:09|       16-07-11T11:42:06| 16-07-11T11:42:01|
|2016-07-10T13:21:57Z|   2016-07-11T11:09:19Z|     2016-07-11T11:02:53Z|2016-07-10T13:21:57Z|  2016-07-10T13:21:57Z|       16-07-11T11:40:09|       16-07-11T11:42:06| 16-07-11T11:42:01|
|2016-07-10T17:17:12Z|   2016-07-11T09:03:08Z|     2016-07-10T18:16:34Z|2016-07-10T17:17:12Z|  2016-07-10T17:17:12Z|       16-07-11T11:40:09|       16-07-11T11:42:06| 16-07-11T11:42:01|
|2016-07-10T13:21:56Z|   2016-07-11T11:15:24Z|     2016-07-11T11:00:40Z|2016-07-10T13:21:56Z|  2016-07-10T13:21:56Z|       16-07-11T11:40:09|       16-07-11T11:42:06| 16-07-11T11:42:01|
|2016-07-10T13:22:02Z|   2016-07-11T11:23:52Z|     2016-07-11T11:10:01Z|2016-07-10T13:22:02Z|  2016-07-10T13:22:02Z|       16-07-11T11:40:09|       16-07-11T11:42:06| 16-07-11T11:42:01|
|2016-07-11T06:48:19Z|                   null|     2016-07-11T10:36:33Z|2016-07-11T06:48:19Z|  2016-07-11T06:48:19Z|       16-07-11T11:40:09|       16-07-11T11:42:06| 16-07-11T11:42:01|
|2016-07-10T12:39:20Z|                   null|     2016-07-10T12:39:33Z|2016-07-10T12:39:20Z|  2016-07-10T12:39:20Z|       16-07-11T11:40:09|       16-07-11T11:42:06| 16-07-11T11:42:01|
+--------------------+-----------------------+-------------------------+--------------------+----------------------+------------------------+------------------------+------------------+

```

``Observation: ``
From the output it can be observed-

- created_timestamp,submitted_timestamp and task_created_timestamp seems to be same

- job_exec_exit_timestamp have some null values




