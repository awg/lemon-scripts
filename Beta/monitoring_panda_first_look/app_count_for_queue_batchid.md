## 5.A) Count how many applications (_id) are there for each queue_name,batchID (batchID is the pilot)

```python
df = sqlContext.read.json("/project/monitoring/archive/panda/enr/panda_job/2016/07/11.tmp")
df.filter("data.atlas_queue_name is not null AND data.batch_id is not null and metadata._id is not null").select("data.atlas_queue_name","data.batch_id","metadata._id").groupBy("atlas_queue_name","batch_id").agg(count("*").alias("count_id")).show()


```
### Example output

```text

+--------------------+--------------------+--------+
|    atlas_queue_name|            batch_id|count_id|
+--------------------+--------------------+--------+
|         TOKYO_MCORE|581403.lcg-ce03.i...|       6|
|         AGLT2_MCORE|gate04.aglt2.org#...|       9|
|    praguelcg2_MCORE|41386397.torque.f...|       8|
|   ANALY_INFN-NAPOLI|13545639.atlas-cr...|       4|
|       ANALY_PIC_SL6|17522339.pbs04.pi...|      17|
|      ANALY_SWT2_CPB|13588842.gk02.atl...|       4|
|    ANALY_INFN-ROMA1|              795648|       7|
|           ANALY_FZU|41387176.torque.f...|       4|
|     ANALY_CERN_SLC6|           826834391|       7|
|      ANALY_MANC_SL6|230570.ce02.tier2...|       4|
|            BNL_PROD|gridgk04.racf.bnl...|      14|
|      ANALY_MWT2_SL6|iut2-gk.mwt2.org#...|       3|
|       ANALY_RAL_SL6|arc-ce03.gridpp.r...|      13|
|       ANALY_LIV_SL6|9973274.hepgrid96...|       7|
|           ANALY_FZU|41387440.torque.f...|       3|
|            BNL_PROD|gridgk06.racf.bnl...|      17|
|     ANALY_BNL_SHORT|gridgk04.racf.bnl...|      10|
|ANALY_QMUL_HIMEM_SL6|             5056362|       7|
|           ANALY_FZK|            17385312|       4|
|     ANALY_ROMANIA07|4830753.tbit07.ni...|      13|
+--------------------+--------------------+--------+

```

## 5.B) Print above output ordered by count_id in descending order

```python

df.filter("data.atlas_queue_name is not null AND data.batch_id is not null and metadata._id is not null").select("data.atlas_queue_name","data.batch_id","metadata._id").groupBy("atlas_queue_name","batch_id").agg(count("*").alias("count_id")).orderBy(col("count_id").desc()).show()
```

```text

+----------------+--------------------+--------+
|atlas_queue_name|            batch_id|count_id|
+----------------+--------------------+--------+
|    NERSC_Edison|             1055732|    1554|
|   ANALY_CONNECT|4507459.cc-mgmt1....|     793|
|   ANALY_CONNECT|4507161.cc-mgmt1....|     685|
|   ANALY_CONNECT|          1587702.m2|     661|
|   ANALY_CONNECT|          1587725.m2|     629|
|   ANALY_CONNECT|4499118.cc-mgmt1....|     591|
|   ANALY_CONNECT|          1587726.m2|     580|
|   ANALY_CONNECT|          1587724.m2|     577|
|   ANALY_CONNECT|4507460.cc-mgmt1....|     568|
|   ANALY_CONNECT|          1587723.m2|     562|
|    NERSC_Edison|             1065687|     548|
|   ANALY_CONNECT|          1587721.m2|     455|
|   ANALY_CONNECT|          1586488.m2|     300|
|   ANALY_CONNECT|          1586695.m2|     300|
|   ANALY_CONNECT|          1586490.m2|     291|
|   ANALY_CONNECT|          1586679.m2|     283|
|   ANALY_CONNECT|          1586489.m2|     269|
|   ANALY_CONNECT|          1586462.m2|     269|
|   ANALY_CONNECT|          1586487.m2|     264|
|   ANALY_CONNECT|          1586506.m2|     262|
+----------------+--------------------+--------+

```

### 5.C) Print output for CERN queue ordered by count_id in descending order

```python
df.filter("data.atlas_queue_name is not null AND data.batch_id is not null and metadata._id is not null AND data.atlas_queue_name LIKE '%CERN%'").select("data.atlas_queue_name","data.batch_id","metadata._id").groupBy("atlas_queue_name","batch_id").agg(count("*").alias("count_id")).orderBy(col("count_id").desc()).show()
```

```text
+----------------+--------+--------+
|atlas_queue_name|batch_id|count_id|
+----------------+--------+--------+
|ANALY_CERN_SHORT|31544420|      47|
|ANALY_CERN_SHORT|31544421|      47|
|ANALY_CERN_SHORT|31544422|      45|
|ANALY_CERN_SHORT|31544419|      43|
|ANALY_CERN_SHORT|31544410|      43|
|ANALY_CERN_SHORT|31544417|      43|
|ANALY_CERN_SHORT|31544412|      41|
|ANALY_CERN_SHORT|31544413|      39|
|ANALY_CERN_SHORT|31544411|      34|
|ANALY_CERN_SHORT|31544406|      25|
|ANALY_CERN_SHORT|31548209|      24|
|ANALY_CERN_SHORT|31548211|      24|
|ANALY_CERN_SHORT|31551222|      24|
|ANALY_CERN_SHORT|31548207|      24|
|ANALY_CERN_SHORT|31548311|      24|
|ANALY_CERN_SHORT|31545046|      24|
|ANALY_CERN_SHORT|31548322|      23|
|ANALY_CERN_SHORT|31548213|      21|
|ANALY_CERN_SHORT|31548314|      21|
|ANALY_CERN_SHORT|31548212|      21|
+----------------+--------+--------+


```

### 5.D) How many applications are there for each CERN queue and batch_id. List out application ids along with total count. Result is ordered by count in descending order.

``` python

rddCount = df.filter("data.atlas_queue_name is not null AND data.batch_id is not null and metadata._id is not null AND data.atlas_queue_name LIKE '%CERN%'").select("data.atlas_queue_name","data.batch_id","metadata._id").map(lambda x:((x[0],x[1]),x[2])).groupByKey().mapValues(len)
rddList = df.filter("data.atlas_queue_name is not null AND data.batch_id is not null and metadata._id is not null AND data.atlas_queue_name LIKE '%CERN%'").select("data.atlas_queue_name","data.batch_id","metadata._id").map(lambda x:((x[0],x[1]),x[2])).groupByKey().mapValues(list)
rddFinal = rddCount.join(rddList)
rddFinal.sortBy(lambda x: x[1][0],False).take(5)

```

### Example output

```text
((u'ANALY_CERN_SHORT', u'31544421'), (47, [u'2919780701', u'2919780684', u'2919780712', u'2919780719', u'2919780724', u'2919780719', u'2919780724', u'2919780726', u'2919780724', u'2919773259', u'2919773272', u'2919773312', u'2919773325', u'2919773312', u'2919773393', u'2919780625', u'2919780643', u'2919780665', u'2919780625', u'2919780684', u'2919773325', u'2919773406', u'2919780590', u'2919773393', u'2919780612', u'2919780612', u'2919780625', u'2919780643', u'2919780665', u'2919780643', u'2919780684', u'2919780701', u'2919780712', u'2919780719', u'2919780701', u'2919780712', u'2919780726', u'2919780726', u'2919773312', u'2919773272', u'2919773393', u'2919773406', u'2919780590', u'2919773406', u'2919780590', u'2919780612', u'2919780665'])),
((u'ANALY_CERN_SHORT', u'31544420'), (47, [u'2919773245', u'2919773257', u'2919773270', u'2919773270', u'2919773310', u'2919773257', u'2919773323', u'2919780588', u'2919780624', u'2919780607', u'2919780624', u'2919780723', u'2919780664', u'2919780698', u'2919780716', u'2919780716', u'2919780698', u'2919780723', u'2919773391', u'2919773310', u'2919773404', u'2919773391', u'2919780588', u'2919780588', u'2919773404', u'2919780664', u'2919780664', u'2919780641', u'2919780710', u'2919780683', u'2919780710', u'2919780716', u'2919780710', u'2919773310', u'2919773270', u'2919773323', u'2919773391', u'2919773404', u'2919780607', u'2919780607', u'2919780624', u'2919780641', u'2919780641', u'2919780683', u'2919780683', u'2919780698', u'2919780723'])),
((u'ANALY_CERN_SHORT', u'31544422'), (45, [u'2919773313', u'2919773394', u'2919780592', u'2919780617', u'2919780649', u'2919780634', u'2919780673', u'2919780649', u'2919780693', u'2919780707', u'2919780714', u'2919780693', u'2919780722', u'2919780722', u'2919780725', u'2919780714', u'2919780725', u'2919780673', u'2919780707', u'2919780736', u'2919780736', u'2919780722', u'2919780736', u'2919773301', u'2919773326', u'2919773394', u'2919773326', u'2919773407', u'2919780617', u'2919780634', u'2919780592', u'2919773313', u'2919773326', u'2919773407', u'2919773394', u'2919780592', u'2919773407', u'2919780634', u'2919780649', u'2919780617', u'2919780673', u'2919780693', u'2919780707', u'2919780714', u'2919780725']))
((u'ANALY_CERN_SHORT', u'31544417'), (43, [u'2919773309', u'2919773322', u'2919773390', u'2919773322', u'2919773403', u'2919780587', u'2919780587', u'2919780622', u'2919780587', u'2919780652', u'2919780677', u'2919780715', u'2919780708', u'2919780715', u'2919780652', u'2919780708', u'2919780696', u'2919773230', u'2919773230', u'2919773244', u'2919773269', u'2919773309', u'2919773256', u'2919773269', u'2919780608', u'2919773403', u'2919780608', u'2919780652', u'2919780638', u'2919780696', u'2919773403', u'2919773309', u'2919773390', u'2919780608', u'2919780622', u'2919780638', u'2919780638', u'2919780622', u'2919780677', u'2919780677', u'2919780696', u'2919780708', u'2919780715']))
((u'ANALY_CERN_SHORT', u'31544410'), (43, [u'2919780697', u'2919780697', u'2919780697', u'2919773212', u'2919773218', u'2919773226', u'2919773218', u'2919773238', u'2919773238', u'2919773246', u'2919773226', u'2919773258', u'2919773258', u'2919773246', u'2919773311', u'2919773324', u'2919773311', u'2919773392', u'2919773405', u'2919773324', u'2919773405', u'2919780610', u'2919780589', u'2919780623', u'2919780610', u'2919780639', u'2919780681', u'2919780639', u'2919780681', u'2919773324', u'2919780589', u'2919773405', u'2919780610', u'2919780663', u'2919780663', u'2919780681', u'2919773392', u'2919780589', u'2919773392', u'2919780623', u'2919780639', u'2919780663', u'2919780623']))


```


### 5.E) Count how many applications (_id) are there for each queue_name,batchID (batchID is the pilot) for CERN

``` python

df = sqlContext.read.json("/project/monitoring/archive/panda/enr/panda_job/2016/07/11.tmp")
df.filter("data.atlas_queue_name is not null AND data.batch_id is not null and metadata._id is not null AND data.atlas_queue_name LIKE '%CERN%'").select("data.atlas_queue_name","data.batch_id","metadata._id").groupBy("atlas_queue_name","batch_id").agg(count("*").alias("count_app")).orderBy(col("count_app").desc()).show()

```

### Example output

```text

+----------------+--------+---------+
|atlas_queue_name|batch_id|count_app|
+----------------+--------+---------+
|ANALY_CERN_SHORT|31544421|       47|
|ANALY_CERN_SHORT|31544420|       47|
|ANALY_CERN_SHORT|31544422|       45|
|ANALY_CERN_SHORT|31544410|       43|
|ANALY_CERN_SHORT|31544419|       43|
|ANALY_CERN_SHORT|31544417|       43|
|ANALY_CERN_SHORT|31544412|       41|
|ANALY_CERN_SHORT|31544413|       39|
|ANALY_CERN_SHORT|31544411|       34|
|ANALY_CERN_SHORT|31544406|       25|
|ANALY_CERN_SHORT|31548311|       24|
|ANALY_CERN_SHORT|31551222|       24|
|ANALY_CERN_SHORT|31548207|       24|
|ANALY_CERN_SHORT|31545046|       24|
|ANALY_CERN_SHORT|31548211|       24|
|ANALY_CERN_SHORT|31548209|       24|
|ANALY_CERN_SHORT|31548322|       23|
|ANALY_CERN_SHORT|31548212|       21|
|ANALY_CERN_SHORT|31548312|       21|
|ANALY_CERN_SHORT|31548215|       21|
+----------------+--------+---------+

```


### 5.F) Count how many DISTINCT applications (_id) are there for each queue_name,batchID (batchID is the pilot) for CERN

``` python

df = sqlContext.read.json("/project/monitoring/archive/panda/enr/panda_job/2016/07/11.tmp")
df.filter("data.atlas_queue_name is not null AND data.batch_id is not null and metadata._id is not null AND data.atlas_queue_name LIKE '%CERN%'").select("data.atlas_queue_name","data.batch_id","metadata._id").distinct().groupBy("atlas_queue_name","batch_id").agg(count("*").alias("count_app")).orderBy(col("count_app").desc()).show()

```

### Example output

```text
+----------------+--------+---------+
|atlas_queue_name|batch_id|count_app|
+----------------+--------+---------+
|ANALY_CERN_SHORT|31544412|       18|
|ANALY_CERN_SHORT|31544420|       17|
|ANALY_CERN_SHORT|31544410|       17|
|ANALY_CERN_SHORT|31544421|       17|
|ANALY_CERN_SHORT|31544417|       17|
|ANALY_CERN_SHORT|31544419|       17|
|ANALY_CERN_SHORT|31544422|       16|
|ANALY_CERN_SHORT|31544413|       14|
|ANALY_CERN_SHORT|31544411|       14|
|ANALY_CERN_SHORT|31551222|       12|
|ANALY_CERN_SHORT|31544406|       12|
|ANALY_CERN_SHORT|31544402|        9|
|ANALY_CERN_SHORT|31548311|        8|
|ANALY_CERN_SHORT|31545046|        8|
|ANALY_CERN_SHORT|31548209|        8|
|ANALY_CERN_SHORT|31548211|        8|
|ANALY_CERN_SHORT|31548322|        8|
|ANALY_CERN_SHORT|31548207|        8|
|ANALY_CERN_SHORT|31548210|        7|
|ANALY_CERN_SHORT|31548215|        7|
+----------------+--------+---------+

```
