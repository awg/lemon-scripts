## 4.A) Count how many distinct name there are for both fields vo_name and wn_host_name

### vo_name


```python
df = sqlContext.read.json("/project/monitoring/archive/panda/enr/panda_job/2016/07/11.tmp")
df.select("data.vo_name").distinct().show()

```

### Example output

```text

+-------+
|vo_name|
+-------+
|  atlas|
|   null|
+-------+

```

 
 ```python
df.select("data.vo_name").distinct().count()
```

### Example output

```text
2
```


### wn_host_name


```python
df = sqlContext.read.json("/project/monitoring/archive/panda/enr/panda_job/2016/07/11.tmp")
df.select("data.wn_host_name").distinct().show()
```

### Example output

```text

+--------------------+
|        wn_host_name|
+--------------------+
|            kiso0051|
| v37-01.gina.sara.nl|
|  b665229bf9.cern.ch|
|slot1_11@acas1808...|
|MPI_f&#195;&#188;...|
|slot1_7@b6a730ab7...|
|    gpc-f113n068-ib0|
|lcg-wn02-02.icepp.jp|
|slot5@c154.karst....|
|slot1_9@acas1384....|
|         c01-006-154|
|             abc-28a|
|slot1_4@batch0664...|
|slot1_29@lcg1925....|
|slot1_4@batch0606...|
|             f9nd023|
|slot11@c100.karst...|
|am94-22.gina.sara.nl|
|slot1_6@acas1554....|
|       wn120.jinr.ru|
+--------------------+


```

 
 ```python
df.select("data.wn_host_name").distinct().count()
```

### Example output

```text
59248

```


## 4.B) count how many atlas and null are there for vo_name

 
 ```python
df = sqlContext.read.json("/project/monitoring/archive/panda/enr/panda_job/2016/07/11.tmp")
df.select("data.vo_name").map(lambda x: (x[0],1)).reduceByKey(lambda x,y: x+y).take(5)
```

### Example output

```text

[(None, 2294022), (u'atlas', 275960)]

```







## 4.C)  Relation between queue_name or site_name with vo_name

 ```python
df = sqlContext.read.json("/project/monitoring/archive/panda/enr/panda_job/2016/07/11.tmp")
df.filter("upper(data.atlas_queue_name)  LIKE '%CERN%'").select("data.site_name","data.atlas_queue_name","data.vo_name").distinct().show(100)
```

### Example output

```text

+---------+--------------------+-------+
|site_name|    atlas_queue_name|vo_name|
+---------+--------------------+-------+
|     null|    ANALY_CERN_SHORT|   null|
|     null|   CERN-PROD-preprod|   null|
|     null|     CERN-PROD_SHORT|   null|
|     null|        CERN-PROD_HI|   null|
|  CERN-P1|CERN-P1_DYNAMIC_M...|  atlas|
|CERN-PROD|ANALY_CERN_GLEXECDEV|  atlas|
|     null|CERN-P1_DYNAMIC_M...|   null|
|     null|CERN-P1_DYNAMIC_M...|   null|
|CERN-PROD|     ANALY_CERN_SLC6|  atlas|
|     null|           CERN-PROD|   null|
|CERN-PROD|    ANALY_CERN_SHORT|  atlas|
|     null|     CERN-PROD_MCORE|   null|
|     null|   ANALY_CERN_XROOTD|   null|
|     null|    ANALY_CERN_CLOUD|   null|
|     null|CERN-PROD-preprod...|   null|
|     null|     ANALY_CERN_SLC6|   null|
|     null|CERN-PROD_PRESERV...|   null|
|CERN-PROD|     CERN-PROD_SHORT|  atlas|
|CERN-PROD|     ANALY_CERN_TEST|  atlas|
|CERN-PROD|        CERN-PROD_HI|  atlas|
|     null|       CERN-P1_MCORE|   null|
|     null|     CERN-PROD_CLOUD|   null|
|     null|             CERN-P1|   null|
|     null|     ANALY_CERN_TEST|   null|
|CERN-PROD|           CERN-PROD|  atlas|
|CERN-PROD|     CERN-PROD_MCORE|  atlas|
|CERN-PROD|    ANALY_CERN_CLOUD|  atlas|
|CERN-PROD|CERN-PROD_PRESERV...|  atlas|
|     null|CERN-PROD_CLOUD_M...|   null|
|     null|ANALY_CERN_GLEXECDEV|   null|
|CERN-PROD|     CERN-PROD_CLOUD|  atlas|
|  CERN-P1|       CERN-P1_MCORE|  atlas|
|CERN-PROD|   CERN-PROD-preprod|  atlas|
+---------+--------------------+-------+


```

``Observation:`` vo_name is null whenever site_name is null and atlas otherwise



## 4.D)  filter by CERN: group by queue name, host_name and count all rows

 ```python
df = sqlContext.read.json("/project/monitoring/archive/panda/enr/panda_job/2016/07/11.tmp")
df.filter("data.atlas_queue_name is not null and upper(data.atlas_queue_name) LIKE '%CERN%'").groupBy("data.atlas_queue_name","data.wn_host_name").agg(count("*").alias("count")).orderBy(col("count").desc()).show()

```

### Example output

```text
+----------------+------------------+-----+
|atlas_queue_name|      wn_host_name|count|
+----------------+------------------+-----+
| ANALY_CERN_SLC6|aipanda036.cern.ch| 5992|
| ANALY_CERN_SLC6|aipanda034.cern.ch| 2930|
| ANALY_CERN_SLC6|aipanda031.cern.ch| 2693|
| ANALY_CERN_SLC6|aipanda030.cern.ch| 2354|
| ANALY_CERN_SLC6|aipanda035.cern.ch| 2174|
| ANALY_CERN_SLC6|aipanda032.cern.ch| 2100|
| ANALY_CERN_SLC6|aipanda037.cern.ch| 1676|
|ANALY_CERN_SHORT|b653c26858.cern.ch|  932|
|ANALY_CERN_SHORT|aipanda036.cern.ch|  929|
|ANALY_CERN_SHORT|b6c136cdd9.cern.ch|  842|
|ANALY_CERN_SHORT|b62d726151.cern.ch|  772|
|ANALY_CERN_SHORT|b6ec0a6320.cern.ch|  772|
|ANALY_CERN_SHORT|b6f3c765b9.cern.ch|  762|
|ANALY_CERN_SHORT|b6718f97e6.cern.ch|  628|
|ANALY_CERN_SHORT|b606d7eca5.cern.ch|  572|
|ANALY_CERN_SHORT|aipanda037.cern.ch|  489|
|ANALY_CERN_SHORT|aipanda032.cern.ch|  486|
|ANALY_CERN_SHORT|aipanda035.cern.ch|  472|
|ANALY_CERN_SHORT|aipanda030.cern.ch|  468|
|ANALY_CERN_SHORT|aipanda034.cern.ch|  458|
+----------------+------------------+-----+



```

