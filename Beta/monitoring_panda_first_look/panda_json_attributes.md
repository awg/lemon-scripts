# Panda json data attributes

## Difference between vo_name and wn_host_name

```python

df = sqlContext.read.json("/project/monitoring/archive/panda/enr/panda_job/2016/07/11.tmp")
df.select("data.vo_name","data.wn_host_name").show()
```

### Example output

```text

+-------+--------------------+
|vo_name|        wn_host_name|
+-------+--------------------+
|  atlas|slot1_2@c-119-18....|
|  atlas|slot1_36@grid230....|
|  atlas|slot1_2@host-172-...|
|  atlas|wn224.datagrid.ce...|
|  atlas|wn264.datagrid.ce...|
|  atlas|          ccwsge0019|
|  atlas|                b109|
|  atlas|  b6fae5be1c.cern.ch|
|  atlas|iberis04.farm.par...|
|  atlas|             f9nd087|
|  atlas|wn1909190.tier2.h...|
|  atlas|slot1_13@acas1949...|
|  atlas|slot1_28@lucille-...|
|  atlas|wn214.datagrid.ce...|
|  atlas|wn238.datagrid.ce...|
|  atlas|slot1_1@lcg1717.g...|
|  atlas|wn136.datagrid.ce...|
|  atlas|wn258.datagrid.ce...|
|  atlas|     slot1_2@node257|
|  atlas|wn-car-090.farm.n...|
+-------+--------------------+

```

