import matplotlib.pyplot as plt
from sys import argv
from numpy import *
from matplotlib.dates import strpdate2num

"""
Plots time vs data counts for a particular entity

Usage: python timevscountperentity.py <cvs file which has the data to be plot>

arg[1]: csv file for whcih the graph is to be plot
"""

ENTITY = ["p01001532822771", "lxbst2316", "b616cf1e04", "b6ad2ad397", "b6f3fca9a4", "b6269aeaec"]

data = loadtxt(
        argv[1], 
        delimiter=",", 
        usecols=(0, 1, 8), 
        dtype={'names': ('entity', 'ts', 'count'), 'formats': ('S15', 'f4', 'i4')},
        converters={1:strpdate2num('%Y-%m-%d')}
        )

def filteronentity(row, entity):
    return row[0] == entity

def getentitydata(entity):
    xp = []
    yp = []
    newdata = data[array([filteronentity(x, entity) for x in data])]
    for i in sort(newdata):
        xp.append(i[1])
        yp.append(i[2])
    return xp, yp




fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2, sharex='col', sharey='row')
fig.suptitle('Plot of number of data count in one day vs time slots', fontsize=14, fontweight='bold')
axes = [ax1, ax2, ax3, ax4]

for i in range(4):
    ax = axes[i]
    entity = ENTITY[i]
    
    ax.set_title(entity)
    xp, yp = getentitydata(entity)
    print xp
    ax.plot_date(xp, yp)
    ax.set_xlabel("date")
    ax.set_ylabel("# of data counts")


plt.show()
#plt.savefig('countvstime.pdf', bbox_inches='tight')
