import matplotlib.pyplot as plt
from sys import argv
from numpy import *
from matplotlib.dates import strpdate2num

"""
Plots frequency distribution of number of entities with a particular count

Usage: python histocount.py <cvs file which has the data to be plot>

arg[1]: csv file for whcih the graph is to be plot

"""


data = loadtxt(
        argv[1], 
        delimiter=",", 
        usecols=(8,), 
        )

plt.hist(data, bins=arange(data.min(), data.max()+1))
plt.ylabel("# of entries corresponding to a count")
plt.xlabel("counts")
plt.yscale("log")
plt.savefig('freqdiscount.jpg', bbox_inches='tight')


