# Calculating Core Seconds/Seconds for an entity/service from LeMon data

As LeMon reports timestamp with each record, we can use this information to
calculate number of seconds an entity was part of a hostgroup. LeMon metric 4111
provides the number of CPUs in the entity. This information can be also be
used to get an estimate of core-seconds per service.

There are two scripts - one for estimating core seconds and other to generate
a list of entities and the number of seconds it was part of a hostgroup.

## ``calc_coreseconds.py``

This script calculates two estimates of the core-seconds per service - the
pessimistic estimate and realitic estimate.

To estimate the core-seconds, the hourly aggregated data for "None" metric (as obtained
by running ``lemon_hourly_aggregattion.py`` for no metric ~ for no '-m' flag ~ metric 0) is processed to gather
number of ``elapsed`` seconds an entity was associated with a hostgroup for every hour.
This is done by subtracting ``min_ts`` from ``max_ts`` and rounding up the number
to user specified seconds. The result for 4111 (CPU count) metric aggregation
is then joined to this and the product for ``elapsed * number_of_cpus`` is calculated.
This is then sum per hostgroup and the service name is derived from the hostgroup name
as the second tier of hostgroup name.

- Pessimistic Estimate - If no 4111 plugin output is available for a row, that row is removed from
  final sum.
- Realistic Estimate - Average number of CPUs for each entity is computed by taking "day" aggregates
  from 4111 aggregation result. This number is used for all those rows for which number
  of CPUs is not available.

### Usage

See the help for the script by passing ``--help`` flag.

As this script depends on the ``hourly_agg_util.py``, you need to pass that file using ``--py-file`` flag to ``spark-submit``.
**Please see [this](../production/lemon_hourly_aggregation/lib/README.md#example-usage)** before running this script

### Output

Text to stdout


## ``entity_hg_seconds.py``

This script computes the number of seconds per ``(entity,hostgroup)``.

To do this, the hourly aggregated data for "None" metric (as obtained
by running ``lemon_hourly_aggregattion.py`` for no metric) is processed to gather
number of ``elapsed`` seconds an entity was associated with a hostgroup for every hour.
This is done by subtracting ``min_ts`` from ``max_ts`` and rounding up the number
to user specified seconds. The sum per ``(entity,hostgroup)`` is then computed.

### Usage

See the help for the script by passing ``--help`` flag.

As this script depends on the ``hourly_agg_util.py``, you need to pass that file using ``--py-file`` flag to ``spark-submit``.
**Please see [this](../production/lemon_hourly_aggregation/lib/README.md#example-usage)** before running this script

### Output

JSON to HDFS with the following schema

```bash
root
 |-- entity: string (nullable = true)
 |-- submitter_hostgroup: string (nullable = true)
 |-- sum_time: long (nullable = true)
```
