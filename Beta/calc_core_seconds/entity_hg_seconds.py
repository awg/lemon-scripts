# Script which calculates number of seconds per entity-HG pair
# Takes the output of "None" aggregation by lemon_hourly_aggregation.py
# Usage: entity_hg_seconds.py --help
# You need to send hourly_agg_util.py to spark-submit using --pyfile switch
from math import ceil
import optparse
import sys

from pyspark import (HiveContext,
                     SparkConf,
                     SparkContext)
from pyspark.sql.functions import sum, udf
from pyspark.sql.types import LongType

from hourly_agg_util import clean_data_for_hour


def _ts_gen(round_=300):
    def _time_associate(min_ts, max_ts, type_):
        """
        Compute max_ts - min_ts and round up round_ seconds
        """
        if type_ != "raw":
            return 3600
        elapsed_ms = max_ts - min_ts
        elapsed_s = elapsed_ms/1000
        round_elapsed_s = ceil(float(elapsed_s)/round_)*round_
        return int(round_elapsed_s)
    return _time_associate


def calc_seconds_per_entity(res_none, output_file, round_up_value):
    """
    Calculate the sum of seconds per (entity, HG) pair
    """
    r_time = clean_data_for_hour(res_none, threshold=30)
    _fn = _ts_gen(round_=round_up_value)
    tudf = udf(_fn, LongType())
    r_time = r_time.withColumn("elapsed", tudf("min_ts", "max_ts", "type"))
    res = (r_time.groupBy("entity", "submitter_hostgroup")
           .agg(sum("elapsed").alias("sum_time"))
           .orderBy("entity", "submitter_hostgroup"))
    res.coalesce(5).write.mode("overwrite").json(output_file)


def main(res_none_file, output_file, round_up_value):
    APP_NAME = "Entity Seconds"
    # Initialize an empty conf so that spark-submit options can fill it
    conf = SparkConf()
    # But we set master explicilty
    conf = conf.setMaster("yarn-client").setAppName(APP_NAME)
    # Build the spark context
    sc = SparkContext(conf=conf)
    sc.setLogLevel("FATAL")
    hiveContext = HiveContext(sc)
    # First read both results to get two dataframes
    res_none = hiveContext.read.json(res_none_file)
    calc_seconds_per_entity(res_none, output_file, round_up_value)


if __name__ == "__main__":
    parser = optparse.OptionParser(
        usage="%prog [options] INPUT_FILE RESULT_FILE")
    parser.add_option("-r", "--round-up-value", type="int", dest="round_up",
                      default=1,
                      help=("Specify the number of seconds to which associat"
                            "ion time will be rounded up to. Default is 1"
                            " which implies no rounding."))
    opts, args = parser.parse_args()
    # Sanity checking of options and arguments
    if len(args) != 2:
        parser.print_help()
        sys.exit(1)
    FILE_NAME = args[0]
    RESULT_NAME = args[1]
    main(FILE_NAME, RESULT_NAME, opts.round_up)
