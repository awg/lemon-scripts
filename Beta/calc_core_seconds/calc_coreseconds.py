# Script which calculates number of core-seconds per "service"
# The service is defined as second level of hostgroup (e.g. bi/inter/adm
# - the service is inter)
# Usage: calc_coreseconds.py metric_4111_result metric_None_result
# You need to send hourly_agg-util.py to spark-submit using --pyfile switch
from math import ceil
import optparse
from pprint import pprint
import sys

from pyspark import (HiveContext,
                     SparkConf,
                     SparkContext)
from pyspark.sql.functions import avg, sum, udf
from pyspark.sql.types import DoubleType, LongType, StringType

from hourly_agg_util import clean_data_for_hour


def _ts_gen(round_=300):
    def _time_associate(min_ts, max_ts, type_):
        """
        Compute max_ts - min_ts and round up round_ seconds
        """
        # For "day" or "hour" the elapsed is full 3600 (num of secs in hour)
        if type_ != "raw":
            return 3600
        elapsed_ms = max_ts - min_ts
        elapsed_s = elapsed_ms/1000
        round_elapsed_s = ceil(float(elapsed_s)/round_)*round_
        return int(round_elapsed_s)
    return _time_associate


def _get_service_name(sh):
    """
    Get the service name from the submitter_hostgroup(sh)
    Use second tier of sh
      E.g. for /bi/inter/adm, the service is "inter"
    Return a string
    """
    submitter_hostgroup = sh.lstrip("/")
    sl = submitter_hostgroup.split("/")
    th = sl[0]
    try:
        svc = sl[1]
    except IndexError:
        svc = th
    return svc


def _cpu_gaps(hour_avg, entity_avg):
    if hour_avg is None:
        return entity_avg
    return hour_avg


def _common_clean(res_4111, res_none, round_):
    """
    Just for DRY
    """
    # Clean the read data
    # For CPU we use threshold of 1 and spread  day entries
    r_cpu = clean_data_for_hour(res_4111, consider_daily=True)
    # For time, no day spread and threshold as 30
    r_time = clean_data_for_hour(res_none, threshold=30)

    # First clean the CPU result. We only want the avg_NumberOfCpus
    r_cpu = r_cpu.select("avg_NumberOfCpus", "entity", "group_interval")
    # Process the time result
    # 1. Compute number of seconds entity was associated with HG (round up to
    #    300s)
    _fn = _ts_gen(round_=round_)
    tudf = udf(_fn, LongType())
    r_time = r_time.withColumn("elapsed", tudf("min_ts", "max_ts", "type"))
    return r_cpu, r_time


def _common_calc(newdf):
    """
    DRY
    """
    newdf = newdf.select(
        "*", (newdf.elapsed * newdf.avg_NumberOfCpus).alias("walls"))
    serviceudf = udf(_get_service_name, StringType())
    newdf = newdf.withColumn("service", serviceudf("submitter_hostgroup"))
    res = newdf.groupBy("service").agg(
        sum("walls").alias("sum_walls")).collect()
    res_json = [x.asDict() for x in res]
    for x in res_json:
        x["sum_walls"] = int(x["sum_walls"])
    return res_json


def calc_seconds_no_extrapolate(res_4111, res_none, round_):
    """
    Calculates the wall seconds without filling gaps
    for the entities that have no CPU data for a given day
    """
    r_cpu, r_time = _common_clean(res_4111, res_none, round_)
    # Join both of them before further processing
    newdf = (r_time.join(
                 r_cpu, [r_time.entity == r_cpu.entity,
                         r_time.group_interval == r_cpu.group_interval])
             .drop(r_cpu.group_interval).drop(r_cpu.entity))
    res_json = _common_calc(newdf)
    print ("Wall-s per service without missing data interpolation "
           "(time rounded up to {0} seconds)".format(round_))
    pprint(res_json)


def calc_seconds_extrapolate(res_4111, res_none, round_):
    """
    Calculates the wall seconds but use average number of CPUs for
    any (entity, hour) tuple for which we cannot calculate
    the number of CPUs
    """
    r_cpu, r_time = _common_clean(res_4111, res_none, round_)
    # As 4111 has a lot of missing values, we can use general average
    # for all 'day' and come up with number of CPUs for each entity.
    # Using outerjoin we can use that average for the time interval
    # when we could not calculate any CPU count
    # Calculate average for each entity
    tdf = (res_4111.filter("type='day'").groupBy("entity")
           .agg(avg("avg_NumberOfCpus").alias("avg_entity_cpu")))
    # Now join newdf and tdf as inner so that we know average cpus
    # for each entity
    # Join both r_time and r_cpu using outer join
    newdf = (r_time.join(
                 r_cpu, [r_time.entity == r_cpu.entity,
                         r_time.group_interval == r_cpu.group_interval],
                 how="outer")
             .drop(r_cpu.group_interval).drop(r_cpu.entity))
    newdf2 = newdf.join(tdf, tdf.entity == newdf.entity).drop(tdf.entity)
    # Now we will compute a new column "avg_cpus" which will have
    # avg_NumberOfCpus value if it is not None or have avg_cpu value
    cpudf = udf(_cpu_gaps, DoubleType())
    newdf2 = newdf2.withColumn(
        "avg_cpu", cpudf("avg_NumberOfCpus", "avg_entity_cpu"))
    # Rename avg_cpu to avg_NumberOfCpus so that we can passit to common
    # function
    newdf2 = newdf2.drop("avg_NumberOfCpus").withColumnRenamed(
        "avg_cpu", "avg_NumberOfCpus")
    res_json = _common_calc(newdf2)
    print ("Wall-s per service with missing data interpolation "
           "(time rounded up to {0} seconds)".format(round_))
    pprint(res_json)


def main(res_4111_file, res_none_file, round_):
    APP_NAME = "World Seconds"
    # Initialize an empty conf so that spark-submit options can fill it
    conf = SparkConf()
    # But we set master explicilty
    conf = conf.setMaster("yarn-client").setAppName(APP_NAME)
    # Build the spark context
    sc = SparkContext(conf=conf)
    sc.setLogLevel("FATAL")
    hiveContext = HiveContext(sc)
    # First read both results to get two dataframes
    res_4111 = hiveContext.read.json(res_4111_file)
    res_none = hiveContext.read.json(res_none_file)
    calc_seconds_no_extrapolate(res_4111, res_none, round_)
    calc_seconds_extrapolate(res_4111, res_none, round_)


if __name__ == "__main__":
    parser = optparse.OptionParser(
        usage="%prog [options] 4111_RESULT None_RESULT")
    parser.add_option("-r", "--round-up-value", type="int", dest="round_up",
                      default=60,
                      help=("Specify the number of seconds to which associat"
                            "ion time will be rounded up to. Default is 60."
                            " Value of 1 implies no rounding."))
    opts, args = parser.parse_args()
    # Sanity checking of options and arguments
    if len(args) != 2:
        parser.print_help()
        sys.exit(1)
    main(args[0], args[1], opts.round_up)
