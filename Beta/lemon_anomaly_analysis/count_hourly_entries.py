# Script that counts hourly aggregated/un-aggregated entries
# for each entity for a given plugin.
# The output generated by this script can be processed and mapped
# so that a composite information about "hourly" aggregates and regular
# entries for the desired plugin
import datetime
import optparse
import sys
import time

from pyspark import (HiveContext,
                     SparkConf,
                     SparkContext)
from pyspark.sql.functions import (col,
                                   count,
                                   countDistinct,
                                   lit,
                                   max,
                                   min,
                                   sum,
                                   udf,
                                   when)
from pyspark.sql.types import (IntegerType,
                               StringType,
                               StructField,
                               StructType)


def _result_name_from_input(inp, plugin_id):
    comp = inp.split("/")
    result_file = "result_count_agg_" + str(plugin_id) + "_"
    try:
        toplevel = comp[-2]
    except IndexError:
        toplevel = "unknown"
    try:
        month = comp[-1]
    except IndexError:
        month = "none"
    if not month:
        month = "none"
    month = month.replace("-", "_").replace(".", "_")
    return result_file + toplevel + "_" + month


def _hourly(ts):
    ts = int(ts)/1000
    d = datetime.datetime(*time.gmtime(int(ts))[:6])
    return d.strftime("%d-%m-%Y %H")

def _count_daily(agg):
    if agg == "daily":
        return 1
    return 0

def _count_none(agg):
    if agg is None:
        return 1
    return 0

def _count_hourly(agg):
    if agg=="hourly":
        return 1
    return 0


def work(inp, metric_id, overwrite=False):
    result_file = _result_name_from_input(inp, metric_id)

    fileschema = StructType([
        StructField("aggregated", StringType()),
        StructField("body", StringType()),
        StructField("entity", StringType()),
        StructField("metric_id", StringType()),
        StructField("metric_name", StringType()),
        StructField("producer", StringType()),
        StructField("submitter_environment", StringType()),
        StructField("submitter_host", StringType()),
        StructField("submitter_hostgroup", StringType()),
        StructField("timestamp", StringType()),
        StructField("toplevel_hostgroup", StringType()),
        StructField("type", StringType()),
        StructField("version", StringType())
    ])

    # Initial BoilerPlate
    conf = SparkConf()
    APP_NAME = ("Hourly aggregated and non aggregated stats for "
                "file {0} and metric {1}").format(inp, metric_id)
    conf = conf.setMaster("yarn-client").setAppName(APP_NAME)
    sc = SparkContext(conf=conf)
    sc.setLogLevel("FATAL")
    hiveContext = HiveContext(sc)
    df=hiveContext.read.json(inp, schema=fileschema)
    hourudf=udf(_hourly, StringType())
    cnoneudf=udf(_count_none, IntegerType())
    chourudf=udf(_count_hourly, IntegerType())
    cdayudf=udf(_count_daily, IntegerType())
    dft=df.filter("timestamp is not null")
    if metric_id:
        dft=dft.filter("metric_id='%s'" % str(metric_id))
    # df2=dft.withColumn("date_hour", hourudf("timestamp")).withColumn("none_count", cnoneudf("aggregated")).withColumn("hour_count", chourudf("aggregated"))
    df2=dft.withColumn("date_hour", hourudf("timestamp"))
    # df3=df2.groupBy("entity", "date_hour").agg(sum("none_count").alias("unaggregated"), sum("hour_count").alias("aggregated"))
    df3 = df2.groupBy("entity", "date_hour").agg(sum(when(col("aggregated").isNull(), lit(1)).otherwise(lit(0))).alias("unaggregated"), sum(when(col("aggregated") == lit("hourly"), lit(1)).otherwise(lit(0))).alias("aggregated"), sum(when(col("aggregated") == lit("daily"), lit(1)).otherwise(lit(0))).alias("aggregated_daily"))
    writer = df3.write
    if overwrite:
        writer = writer.mode("overwrite")
    writer.json(result_file)
    return result_file


def main():
    usage = "usage: %prog [options] FILE"
    p = optparse.OptionParser(usage=usage)
    p.add_option("-m", "--metric-id", dest="metric_id", type="int",
                 help=("Specify the metric ID to process. "
                       "Only one metric ID may be specified. "
                       "If not specified, count over all metrics"))
    p.add_option("-f", "--force", action="store_true", dest="overwrite",
                 help=("Overwrite previous result file if it already exists"
                       ". If this option is not used, pyspark will exit with"
                       " an error. The name of the result file is generated from"
                       " the name of the input file. So if the same input is"
                       " being processed again, use this option"))
    o, a = p.parse_args()
    overwrite = o.overwrite
    metric_id = o.metric_id
    if len(a) > 1:
        print "Please specify only one input file"
        sys.exit(1)
    if not a:
        print "Please specify an input file"
        sys.exit(1)
    res_file = work(a[0], metric_id, overwrite=overwrite)
    print "Result is in {0}".format(res_file)


if __name__ == '__main__':
    main()
