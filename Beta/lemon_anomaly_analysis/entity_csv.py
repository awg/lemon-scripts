# Script which is used to further analyze hours in which no raw entries are received
# Writes a CSV file of the form
# entity,start_ts,end_ts,hours
# for all entities that have unaggregated=0 and aggregated>0
# (Meaning send no raw but send hourly aggregates)
# Continuous time periods are merged and "hours" shows how many
# hours occur in the merged time period
from __future__ import print_function
import calendar
import datetime
import itertools
import json
import sys
import uuid

from pyspark import (HiveContext,
                     SparkConf,
                     SparkContext)
from pyspark.sql.functions import (max,
                                   min,
                                   udf)
from pyspark.sql.types import (LongType,
                               StringType,
                               StructField,
                               StructType)


if len(sys.argv) != 3:
    print("Usage: {0} INP_FILE OP_FILE".format(sys.argv[0]))
    sys.exit(-1)
FILE_NAME = sys.argv[1]
OP_FILE = sys.argv[2]
APP_NAME = "Entities not sending raw entries with timeframes from file: {0}".format(FILE_NAME)
# Initialize an empty conf so that spark-submit options can fill it
conf = SparkConf()
# But we set master explicilty
conf = conf.setMaster("yarn-client").setAppName(APP_NAME)
# Build the spark context
sc = SparkContext(conf=conf)
sc.setLogLevel("FATAL")
hiveContext = HiveContext(sc)

dataframe = hiveContext.read.json(FILE_NAME)
dataframe=dataframe.filter("unaggregated=0").filter("aggregated>0")

def _start_ts_from_hour(hr):
    return int(calendar.timegm(datetime.datetime.strptime(hr, "%d-%m-%Y %H").timetuple()))*1000

def _end_ts_from_hour(hr):
    ts = int(calendar.timegm(datetime.datetime.strptime(hr, "%d-%m-%Y %H").timetuple()))
    ts = ts + 3600
    return ts * 1000

start_ts_udf = udf(_start_ts_from_hour, LongType())
end_ts_udf = udf(_end_ts_from_hour, LongType())

dataframe = dataframe.withColumn("start_ts", start_ts_udf("date_hour"))
dataframe = dataframe.withColumn("end_ts", end_ts_udf("date_hour"))

res = dataframe.select("entity", "start_ts", "end_ts").orderBy("entity", "start_ts").collect()
res = [x.asDict() for x in res]

# Now process continuous time frames to get result
mod_res = []
for ent, g in itertools.groupby(res, key=lambda x: x["entity"]):
    entity = ent
    start_end_times = [(x["start_ts"], x["end_ts"]) for x in g]
    tframes = []
    for k, g in itertools.groupby(itertools.chain(*start_end_times)):
        if len(list(g)) == 1:
            tframes.append(k)
    # Now split the tframes into groups of 2
    tframe = [tframes[i:i+2] for i in xrange(0, len(tframes), 2) ]
    mod_res.extend((entity, tf[0], tf[1], int((tf[1]-tf[0])/(1000*3600))) for tf in tframe)

with open(OP_FILE, "w") as fp:
    for m in mod_res:
        fp.write(",".join((str(z) for z in m)))
        fp.write("\n")
