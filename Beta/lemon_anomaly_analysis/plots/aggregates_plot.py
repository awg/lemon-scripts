import itertools
import datetime
import json
import sys

import matplotlib.pylab as plb
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd


def _process_data(fl):
    with open(fl) as fp:
        cnts = json.load(fp)
    per_no_agg = []
    per_no_unagg = []
    diff_noagg_nounag = []
    for k, g in itertools.groupby(cnts, key=lambda x: x["date_hour"].split(" ")[0]):
        no_agg = dict([(z, None) for z in xrange(0,24)], date=k)
        no_unagg = dict([(z, None) for z in xrange(0,24)], date=k)
        diff_two = dict([(z, None) for z in xrange(0,24)], date=k)
        g = list(g)
        for entry in g:
            hour = int(entry['date_hour'].split(" ")[1])
            no_agg[hour] = (float(entry['count_no_agg'])/entry['count_entities'])*100
            no_unagg[hour] = (float(entry['count_no_unagg'])/entry['count_entities'])*100
            diff_two[hour] = entry['count_no_agg'] / float(entry['count_no_agg'] + entry['count_no_unagg'])*100
        per_no_agg.append(no_agg)
        per_no_unagg.append(no_unagg)
        diff_noagg_nounag.append(diff_two)
    df_per_no_agg = pd.DataFrame(per_no_agg).set_index("date")
    df_per_no_unagg = pd.DataFrame(per_no_unagg).set_index("date")
    df_diff_noagg_nounag = pd.DataFrame(diff_noagg_nounag).set_index("date")
    return df_per_no_agg, df_per_no_unagg, df_diff_noagg_nounag


def _plot_perc_fig(ax, df, title=None, cmap=None, alpha=1.0):
    mat = _get_masked_array(df)
    mesh = ax.pcolormesh(mat, cmap=cmap, edgecolors="w", vmin=0.0, vmax=100.0, alpha=2.4)
    ax.axis('tight')
    # Format
    fig = plt.gcf()
    ax.set_title(title, fontsize=16)
    fig.colorbar(mesh, ax=ax)
    # turn off the frame
    ax.set_frame_on(False)
    # put the major ticks at the middle of each cell
    ax.set_yticks(np.arange(df.shape[0]) + 0.5, minor=False)
    ax.set_xticks(np.arange(df.shape[1]) + 0.5, minor=False)
    # want a more natural, table-like display
    ax.invert_yaxis()
    #ax.xaxis.tick_top()
    #ax.xaxis.set_label_position('top') 
    # Set the labels
    labels = df.columns
    ax.set_xticklabels(labels, minor=False)
    ax.set_yticklabels(df.index, minor=False)
    ax.grid(False)
    # Turn off all the ticks
    for t in ax.xaxis.get_major_ticks():
        t.tick1On = False
        t.tick2On = False
    for t in ax.yaxis.get_major_ticks():
        t.tick1On = False
        t.tick2On = False
    plt.xlabel('Hours', fontsize=14, labelpad=1.8)
    plt.ylabel('Date', fontsize=14)


def _get_masked_array(df):
    mat = df.as_matrix()
    mat = np.ma.masked_invalid(mat)
    return mat


def plot_side_by_side_fig(df1, df2):
    # Plot two images side by side
    cmap = plt.get_cmap('ocean')
    cmap.set_bad(color = 'k', alpha = 1.)
    # Subplot - 1 Not Sending aggregates
    ax1 = plt.subplot(1, 2, 1)
    _plot_perc_fig(ax1, df1, title="% of entities not sending hourly aggregates",
                   cmap=cmap, alpha=2.4)
    # Subplot - 2 Not sending raw
    ax2 = plt.subplot(1, 2, 2)
    _plot_perc_fig(ax2, df2, title="% of entities not sending raw entries",
                   cmap=cmap, alpha=2.4)


def plot_bias(ax, df, title=None, cmap=None, alpha=1.0):
    """
    Plots the bias figure with a custom colormap
    """
    mat = _get_masked_array(df)
    mesh = ax.pcolormesh(mat, cmap=cmap, edgecolors="w",
                         vmin=0.0, vmax=100.0, alpha=2.4)
    ax.axis('tight')
    # Format
    fig = plt.gcf()
    ax.set_title(title, fontsize=16)
    cbar = fig.colorbar(mesh, ax=ax)
    # Custom ticks in colorbar
    cbar.ax.get_yaxis().set_ticks([0, .5, 1.0])
    cbar.ax.get_yaxis().set_ticklabels(['All Raw', 'Tie', 'All Aggregate'])
    #~ for j, lab in enumerate(['$All Raw$','$Tie$','$All Aggregate$']):
        #~ cbar.ax.text(.5, (2 * j + 1) / 8.0, lab, ha='center', va='center')
    
    # turn off the frame
    ax.set_frame_on(False)
    # put the major ticks at the middle of each cell
    ax.set_yticks(np.arange(df.shape[0]) + 0.5, minor=False)
    ax.set_xticks(np.arange(df.shape[1]) + 0.5, minor=False)
    # want a more natural, table-like display
    ax.invert_yaxis()
    #ax.xaxis.tick_top()
    #ax.xaxis.set_label_position('top') 
    # Set the labels
    labels = df.columns
    ax.set_xticklabels(labels, minor=False)
    ax.set_yticklabels(df.index, minor=False)
    ax.grid(False)
    # Turn off all the ticks
    for t in ax.xaxis.get_major_ticks():
        t.tick1On = False
        t.tick2On = False
    for t in ax.yaxis.get_major_ticks():
        t.tick1On = False
        t.tick2On = False
    plt.xlabel('Hours', fontsize=14, labelpad=1.8)
    plt.ylabel('Date', fontsize=14)


def main():
    if len(sys.argv) != 4:
        print "Usage: {0} FILE COMP_GRAPH_NAME BIAS_GRAPH_NAME".format(sys.argv[0])
        sys.exit(-1)
    df_per_no_agg, df_per_no_unagg, df_diff_noagg_nounagg = _process_data(sys.argv[1])
    fig = plt.gcf()
    fig.set_size_inches(20, 12)
    # First Plot
    plot_side_by_side_fig(df_per_no_agg, df_per_no_unagg)
    plt.savefig(sys.argv[2], bbox_inches='tight')
    # Second Plot
    fig = plt.figure()
    fig.set_size_inches(8, 14)
    ax = plt.subplot()
    # Set a colormap, White for 0 and Blue for 1
    cmap = plt.get_cmap('coolwarm')
    cmap.set_bad(color = 'k', alpha = 1.)
    plot_bias(ax, df_diff_noagg_nounagg, title="Profile of entities not sending something", cmap=cmap, alpha=1.0)
    plt.savefig(sys.argv[3], bbox_inches='tight')


if __name__ == "__main__":
    main()
