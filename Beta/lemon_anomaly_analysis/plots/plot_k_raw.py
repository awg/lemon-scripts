import itertools
import datetime
import json
import sys

import matplotlib.pylab as plb
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd


def _plot_perc_fig(ax, df, title=None, cmap=None, alpha=1.0):
    mat = _get_masked_array(df)
    mesh = ax.pcolormesh(mat, cmap=cmap, vmin=0.0, vmax=100.0, alpha=2.4)
    ax.axis('tight')
    # Format
    fig = plt.gcf()
    ax.set_title(title, fontsize=12)
    fig.colorbar(mesh, ax=ax)
    # turn off the frame
    ax.set_frame_on(False)
    # put the major ticks at the middle of each cell
    #ax.set_yticks(np.arange(df.shape[0]) + 0.5, minor=False)
    #ax.set_xticks(np.arange(df.shape[1]) + 0.5, minor=False)
    # want a more natural, table-like display
    ax.invert_yaxis()
    #ax.xaxis.tick_top()
    #ax.xaxis.set_label_position('top') 
    # Set the labels
    #labels = df.columns
    #ax.set_xticklabels(labels, minor=False)
    #ax.set_yticklabels(df.index, minor=False)
    ax.grid(False)
    # Turn off all the ticks
    for t in ax.xaxis.get_major_ticks():
        t.tick1On = False
        t.tick2On = False
    for t in ax.yaxis.get_major_ticks():
        t.tick1On = False
        t.tick2On = False
    #plt.xlabel('Hours', fontsize=14, labelpad=1.8)
    #plt.ylabel('Date', fontsize=14)


def _get_masked_array(df):
    mat = df.as_matrix()
    mat = np.ma.masked_invalid(mat)
    return mat


def _process_data(fl):
    with open(fl) as fp:
        cnts = json.load(fp)
    per_k0 = []
    per_k1 = []
    per_k3 = []
    per_k6 = []
    per_k9 = []
    per_k12 = []
    for k, g in itertools.groupby(cnts, key=lambda x: x["date_hour"].split(" ")[0]):
        k0 = dict([(z, None) for z in xrange(0,24)], date=k)
        k1 = dict([(z, None) for z in xrange(0,24)], date=k)
        k3 = dict([(z, None) for z in xrange(0,24)], date=k)
        k6 = dict([(z, None) for z in xrange(0,24)], date=k)
        k9 = dict([(z, None) for z in xrange(0,24)], date=k)
        k12 = dict([(z, None) for z in xrange(0,24)], date=k)
        g = list(g)
        for entry in g:
            hour = int(entry['date_hour'].split(" ")[1])
            k0[hour] = (float(entry['count_no_unagg'])/entry['count_entities'])*100
            k1[hour] = (float(entry['count_num_unagg_1'])/entry['count_entities'])*100
            k3[hour] = (float(entry['count_num_unagg_3'])/entry['count_entities'])*100
            k6[hour] = (float(entry['count_num_unagg_6'])/entry['count_entities'])*100
            k9[hour] = (float(entry['count_num_unagg_9'])/entry['count_entities'])*100
            k12[hour] = (float(entry['count_num_unagg_12'])/entry['count_entities'])*100
        per_k0.append(k0)
        per_k1.append(k1)
        per_k3.append(k3)
        per_k6.append(k6)
        per_k9.append(k9)
        per_k12.append(k12)
    df_per_k0 = pd.DataFrame(per_k0).set_index("date")
    df_per_k1 = pd.DataFrame(per_k1).set_index("date")
    df_per_k3 = pd.DataFrame(per_k3).set_index("date")
    df_per_k6 = pd.DataFrame(per_k6).set_index("date")
    df_per_k9 = pd.DataFrame(per_k9).set_index("date")
    df_per_k12 = pd.DataFrame(per_k12).set_index("date")
    return df_per_k0, df_per_k1, df_per_k3, df_per_k6, df_per_k9, df_per_k12


def main():
    if len(sys.argv) != 3:
        print "Usage: {0} FILE GRAPH_NAME".format(sys.argv[0])
        sys.exit(-1)
    df_k0, df_k1, df_k3, df_k6, df_k9, df_k12 = _process_data(sys.argv[1])
    fig = plt.gcf()
    fig.set_size_inches(20, 12)
    # First Plot
    # Plot two images side by side
    cmap = plt.get_cmap('ocean')
    cmap.set_bad(color = 'k', alpha = 1.)
    # Subplot - 1 k0
    ax1 = plt.subplot(3, 2, 1)
    _plot_perc_fig(ax1, df_k0, title="% of entities sending 0 raw entries per hour",
                   cmap=cmap, alpha=2.4)
    # Subplot - 2 k1
    ax2 = plt.subplot(3, 2, 2)
    _plot_perc_fig(ax2, df_k1, title="% of entities sending<=1 raw entries per hour",
                   cmap=cmap, alpha=2.4)
    # Subplot - 3 k3
    ax3 = plt.subplot(3, 2, 3)
    _plot_perc_fig(ax3, df_k3, title="% of entities sending<=3 raw entries per hour",
                   cmap=cmap, alpha=2.4)
    # Subplot - 4 k6
    ax4 = plt.subplot(3, 2, 4)
    _plot_perc_fig(ax4, df_k6, title="% of entities sending<=6 raw entries per hour",
                   cmap=cmap, alpha=2.4)
    # Subplot - 5 k9
    ax5 = plt.subplot(3, 2, 5)
    _plot_perc_fig(ax5, df_k9, title="% of entities sending<=9 raw entries per hour",
                   cmap=cmap, alpha=2.4)
    # Subplot - 6 k12
    ax6 = plt.subplot(3, 2, 6)
    _plot_perc_fig(ax6, df_k12, title="% of entities sending<=12 raw entries per hour",
                   cmap=cmap, alpha=2.4)
    plt.savefig(sys.argv[2], bbox_inches='tight')


if __name__ == "__main__":
    main()
