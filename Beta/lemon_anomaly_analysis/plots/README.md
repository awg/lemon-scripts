# Plotting data obtained from Spark scripts

This is a collection of matplotlib scripts to plot the data obtained
from [``process_hourly_count``](../README.md#process_hourly_count)

## ``aggregate_plots.py``

Plots the [comparison](comparison_sample.png) and [bias](bias_sample.png) graph for the given data.

### Usage

```bash
python RESULT_FILE COMP_GRAPH_NAME BIAS_GRAPH_NAME
```

## ``plot_k_raw.py``

Plots the [comparison graph](plot_k_sample.png) for varying threshold

### Usage

```bash
python plot_k_raw.py RESULT_FILE GRAPH_NAME
```
