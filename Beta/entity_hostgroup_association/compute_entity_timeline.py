# Script that reads data from lemon output and computes entity timeline
# i.e. timeframes of entity associations with hostgroups
# It can write outputs to HDFS as well as local filesystems.
# The output is in the form of CSV
# entity,hostgroup,start_tstamp in ms,end_tstamp in ms,start_dtime,end_dtime
# where start_dtime and end_dtime are ISO-8601 representation of UTC times
# denoted by start_tstamp and end_tstamp resp. The list is sorted by entity
# and start_tstamp
from __future__ import print_function
import datetime
import optparse
import sys
import time

from pyspark import (HiveContext,
                     SparkConf,
                     SparkContext)
from pyspark.sql import Window
from pyspark.sql.functions import lag, lead, max, udf
from pyspark.sql.types import LongType, StringType, StructField, StructType


APP_NAME = "Entity Hostgroup Timeline"


def save_result_to_hdfs(res, filename="entity_lifecycle"):
    # Reorder columns and change names
    res2 = res.select(
               res.entity,
               res.submitter_hostgroup.alias("hostgroup"),
               res.first_seen.alias("start_tstamp"),
               res.last_seen.alias("end_tstamp"),
               res.first_seen_dtime.alias("start_dtime"),
               res.last_seen_dtime.alias("end_dtime"))
    (res2.coalesce(1).write.options(header='true')
     .format('com.databricks.spark.csv').save(filename))


def save_result_local(res, filename="entity_lifecycle.csv"):
    csv_res = [(x.entity, x.submitter_hostgroup, str(x.first_seen),
                str(x.last_seen), x.first_seen_dtime, x.last_seen_dtime)
               for x in res.collect()]
    with open(filename, "w") as fp:
        fp.write(
            "entity,hostgroup,start_tstamp,end_tstamp,start_dtime,end_dtime")
        fp.write("\n")
        for r in csv_res:
            fp.write(','.join(r))
            fp.write("\n")


if __name__ == '__main__':
    parser = optparse.OptionParser(
        usage="%prog [-options] FILE1 [FILE2]")
    parser.add_option("-r", "--result-file", dest="result_file",
                      help=("Specify the name of the result file"
                            ". The file will be overwritten if it exists"
                            ". REQUIRED"))
    parser.add_option("-l", "--local-save", dest="local_save",
                      action="store_true", default=False,
                      help=("Store the result CSV on local file system"
                            " rather than HDFS. Default False"))
    opts, args = parser.parse_args()
    if not opts.result_file:
        print("Please specify --result-file")
        sys.exit(1)
    if not args:
        print("Please specify atleast one input")
        parser.print_help()
        sys.exit(1)
    # All files after script name will be parsed
    FILE_NAMES = args
    result_name = opts.result_file
    # Initialize an empty conf so that spark-submit options can fill it
    conf = SparkConf()
    # But we set master explicilty
    conf = conf.setMaster("yarn-client").setAppName(APP_NAME)
    # Build the spark context
    sc = SparkContext(conf=conf)
    sc.setLogLevel("FATAL")
    hiveContext = HiveContext(sc)
    # Define our schema to save a pass of the file (to determine schema)
    fileschema = StructType([
        StructField("aggregated", StringType()),
        StructField("body", StringType()),
        StructField("entity", StringType()),
        StructField("metric_id", StringType()),
        StructField("metric_name", StringType()),
        StructField("producer", StringType()),
        StructField("submitter_environment", StringType()),
        StructField("submitter_host", StringType()),
        StructField("submitter_hostgroup", StringType()),
        StructField("timestamp", StringType()),
        StructField("toplevel_hostgroup", StringType()),
        StructField("type", StringType()),
        StructField("version", StringType())
    ])

    df = hiveContext.read.json(*FILE_NAMES, schema=fileschema)
    # We don't want aggregations producing oscillations in the output, so
    # we remove them. Otherwise for daily aggregations, everyday at 1200 UTC,
    # we will have an entry for a metric for EACH hostgroup to which the entity
    # has belonged during the day. This leads to a situation where multiple
    # hostgroups are reported for same time (1200 UTC)
    df = df.filter("aggregated is null").filter("timestamp is not null")
    df = df.select("entity", df.timestamp.cast('long').alias('timestamp'),
                   "submitter_hostgroup")
    # NOTE: As we have to find out point in time when the switch in hostgroup
    # occurred, we use a window of current row and preicous row and store
    # previous hostgroup and timestamp along with current hostgroup and
    # timestamp
    windowspec = Window.partitionBy("entity").orderBy(
        df["timestamp"].asc(), df["submitter_hostgroup"])
    # If we don't specify default here, null columns are crated which don't
    # work well with filter
    # So we specify a default as an impossible string
    df2 = df.withColumn("prev_hg", lag(
        "submitter_hostgroup", 1, default="%$#$@$@").over(windowspec))
    df2 = df2.withColumn("prev_ts", lag("timestamp", 1).over(windowspec))
    # Now we select only rows for which current and previous hostgroups are
    # not same. This also selects the first occurrence of every entity as
    # previous is null for those. However it does not select last occurrence
    # if hostgroups are same
    df2 = df2.filter(df2["prev_hg"] != df2["submitter_hostgroup"])
    # Now timestamp shows the first_seen time, so we duplicate the column
    duplicateudf = udf(lambda x: x, LongType())
    df3 = df2.withColumn("first_seen", duplicateudf("timestamp"))
    # Now we know when a host started belonging to a hostgroup, but we don't
    # know till when it was associated with that HG
    # So we take a window of two rows (current and next), and chose prev_ts
    # of next row, which shows the last timestamp of this hostgroup
    df3 = df3.withColumn("last_seen_ts", lead("prev_ts", 1).over(windowspec))

    def _ts_convert(ts):
        # Change timestamp to human readable times
        if ts is None:
            return
        ts = int(ts)/1000
        d = datetime.datetime(*time.gmtime(int(ts))[:6])
        return d.isoformat()

    def _set_ts(ts, other_ts):
        # Set ts if it is None
        if ts is None:
            return other_ts
        return ts

    tsconvertudf = udf(_ts_convert, StringType())
    tsudf = udf(_set_ts, LongType())
    # Now we have first_seen and last_seen for all entitites and hostgroups
    # except possibly for the last entity hostgroup pairing
    # To select that, we select max timestamp for each entity and join it with
    # df3. Then if last_seen is null, we set it to max_timestamp
    df_t = (df.groupBy("entity").agg(max("timestamp").alias("max_timestamp"))
            .withColumnRenamed("entity", "grp_entity"))
    df4 = (df3.join(df_t, df_t["grp_entity"] == df3["entity"])
           .select("entity", "submitter_hostgroup", "first_seen",
                   "last_seen_ts", "max_timestamp"))
    df4 = df4.withColumn("first_seen_dtime", tsconvertudf("first_seen"))
    df4 = df4.withColumn("last_seen", tsudf("last_seen_ts", "max_timestamp"))
    df4 = df4.withColumn("last_seen_dtime", tsconvertudf("last_seen"))
    df5 = (df4.select(
               "entity", "submitter_hostgroup", "first_seen",
               "first_seen_dtime", "last_seen", "last_seen_dtime")
           .orderBy(df4["entity"], df4["first_seen"].asc()))
    if opts.local_save:
        save_result_local(df5, filename=result_name)
        print("Result stored locally at {0}".format(result_name))
    else:
        save_result_to_hdfs(df5, filename=result_name)
        print("Result stored in HDFS at {0}".format(result_name))
