# Program that takes a timestamp and either a hostgroup or an entity as input
# and outputs the hostgroup it belonged to (if entity is input) or
# list of entities in the hostgroup (if hostgroup is input) at THAT point
# in time. If an optional ending timestamp or datetime is given,
# it performs the search in the time window
import calendar
import datetime
import optparse
import sys


class NewParser(optparse.OptionParser):
    def format_epilog(self, formatter):
        return self.epilog


def _convert_dtime_to_tstamp(dt):
    """
    Convert a UTC datetime string of form DD-MM-YYYY HH:MM:SS
    to timestamp
    """
    try:
        d = datetime.datetime.strptime(dt, "%d-%m-%Y %H:%M:%S")
    except ValueError:
        raise ValueError("Specify datetime as DD-MM-YYYY HH:MM:SS")
    return int(calendar.timegm(d.timetuple()))


def parse_args():
    """Parse the arguments"""
    parser = NewParser(
        usage="usage: %prog [options] CSV_FILE",
        epilog=("Where CSV_FILE is a CSV file of the form \nentity,"
                "hostgroup,start_ts,end_ts,start_dtime,end_dtime \n\nNote: "
                "that the first line of the file is always ignored\n"))
    parser.add_option(
        "-s", "--start-timestamp", dest="s_tstamp", type="int",
        help=("Specify the starting timetsamp (in seconds) as an integer."
              " If only start timestamp/datetime is given, program will run"
              " for that instant of time only. To specify a window,"
              " specify end timestamp/datetime as well"))
    parser.add_option(
        "-S", "--start-datetime", dest="s_dtime",
        help=("Specify the datetime UTC string "
              "(in form of DD-MM-YYYY HH:MM:SS)"
              ". Only one of '--start-datetime' and '--start-timestamp'"
              " can be specified"))
    parser.add_option(
        "-n", "--entity", dest="entity", help="Search for this entity")
    parser.add_option(
        "-g", "--hostgroup", dest="hostgroup",
        help=("Search for this hostgroup. Only one out of "
              "'--entity' or '--hostgroup' can be specified"))
    parser.add_option(
        "-e", "--end-timestamp", dest="e_tstamp", type="int",
        help=("Specify the ending timetsamp (in seconds) as an integer."
              " If no end timestamp/datetime is given, program will run"
              " for the instant of time specified by start timestamp/"
              "datetime. To specify a window, specify end timestamp/"
              "datetime as well"))
    parser.add_option(
        "-E", "--end-datetime", dest="e_dtime",
        help=("Specify the end datetime UTC string (in form of"
              " DD-MM-YYYY HH:MM:SS)"
              ". Only one of '--end-datetime' and '--end-timestamp'"
              " can be specified"))
    o, a = parser.parse_args()
    if o.s_tstamp and o.s_dtime:
        msg = "Only one out of '-s' and '-S' can be specified"
        print msg
        sys.exit(-1)
    if o.e_tstamp and o.e_dtime:
        msg = "Only one out of '-e' and '-E' can be specified"
        print msg
        sys.exit(-1)
    if not (o.s_tstamp or o.s_dtime):
        print "Please specify a start timestamp or datetime to search"
        parser.print_help()
        sys.exit(-1)
    if o.entity and o.hostgroup:
        msg = ("Only one of '--entity' and '--hostgroup' can be specified "
               "at a time")
        print msg
        sys.exit(-1)
    if not (o.entity or o.hostgroup):
        print "Please specify an entity or a hostgroup to search for."
        parser.print_help()
        sys.exit(-1)
    if not a:
        print "Please specify a CSV file to process"
        sys.exit(-1)
    if len(a) > 1:
        print "Please specify only one CSV file"
        sys.exit(-1)
    opts = {"end_tstamp": None, "entity": None, "hostgroup": None}
    if o.s_tstamp:
        opts["start_tstamp"] = o.s_tstamp
    else:
        opts["start_tstamp"] = _convert_dtime_to_tstamp(o.s_dtime)
    if o.e_tstamp:
        opts["end_tstamp"] = o.e_tstamp
    elif o.e_dtime:
        opts["end_tstamp"] = _convert_dtime_to_tstamp(o.e_dtime)
    opts["entity"] = o.entity
    opts["hostgroup"] = o.hostgroup
    return opts, a[0]


def _pick_values_from_line(line):
    """
    Returns a tuple of form
    (entity, hg, start_ts (in s), end_ts (in s), start_dtime, end_dtime)
    when presented with a line of CSV file
    """
    l = line.split(",")
    return (l[0], l[1], int(l[2])/1000, int(l[3])/1000, l[4], l[5])


def process_csv_file(csv_file):
    """
    Reads the CSV file into a list of tuples of form
    (entity, hg, start_ts (in s), end_ts (in s), start_dtime, end_dtime)
    The list is sorted by entity and start_ts
    """
    with open(csv_file) as fp:
        res = fp.readlines()
    # Remove trailing newlines
    res = map(lambda x: x.strip("\n"), res)
    # Remove first line which just lists schema
    res = res[1:]
    new_res = map(_pick_values_from_line, res)
    new_res = sorted(new_res, key=lambda x: (x[0], x[2]))
    return new_res


def _find_hostgroup(entries, start_tstamp, end_tstamp):
    """
    Perform the actual list filteration to find which hostgroups
    the entity belonged to
    Returns a tuple
    (result, lower_bound, upper_bound)
    Where -
    * result - Is a list of entries of the form (entity, hg, ststamp, etstamp)
    * lower_bound is the closest entry found to the left of window
    * upper_bound is the closest entry found to the right of window
    lower_bound and upper_bound are only returned if result is empty
    Note that result will be empty and lower_bound, upper_bound will be None
    if the entity cannot be found in the list
    """
    lower_bound = upper_bound = None
    # A simple condition is to find all entries that
    # 1. Ended after start_tstamp
    # 2. Started before end_tstamp
    # Take an intersection of the two
    end_after_start = filter(lambda x: x[3] >= start_tstamp, entries)
    start_before_end = filter(lambda x: x[2] <= end_tstamp, entries)
    result = set(end_after_start).intersection(set(start_before_end))
    # result is now a set, sort it again based on timestamp
    # (as entity will be same)
    result = sorted(result, key=lambda x: x[2])
    if not result:
        # If no entry could be found, we need to find neighbours
        try:
            lower_bound = filter(
                lambda x: x[3] < start_tstamp, entries)[-1]
        except IndexError:
            lower_bound = None
        try:
            upper_bound = filter(
                lambda x: x[2] > end_tstamp, entries)[0]
        except IndexError:
            upper_bound = None
    return result, lower_bound, upper_bound


def _find_entity(entries, start_tstamp, end_tstamp):
    """
    Perform the actual list filteration to find entities occuring
    in given time frame.
    Returns a list of entity names
    """
    # A simple condition is to find all entries that
    # 1. Ended after start_tstamp
    # 2. Started before end_tstamp
    # Take an intersection of the two
    end_after_start = filter(lambda x: x[3] >= start_tstamp, entries)
    start_before_end = filter(lambda x: x[2] <= end_tstamp, entries)
    result = set(end_after_start).intersection(set(start_before_end))
    # result is now a set, sort it again based on entity
    result = sorted(result, key=lambda x: x[0])
    result = [x[0] for x in result]
    return result


def find_hostgroup(entries, entity, start_tstamp, end_tstamp):
    """
    Find and return the hostgroup(s) to which an entity belonged
    between start_tstamp and end_tstamp.
    If end_tstamp is None, returns the hostgroup to which entity
    belonged exactly at start_tstamp. If no hostgroup matches, returns the
    closest matches
    """
    entity_entries = filter(lambda x: x[0] == entity, entries)
    if not entity_entries:
        print "Entity: {0} does not occur in the file".format(entity)
    else:
        result, lower, upper = _find_hostgroup(
            entity_entries, start_tstamp, end_tstamp)
        if not result:
            print "The entity did not belong to any hostgroups for given time"
            if lower or upper:
                print "Closest matches are:"
                if lower:
                    msg = ("Before this time; Entity:{0} belonged to "
                           "Hostgroup: {1} from {2} to "
                           "{3}").format(entity, lower[1], lower[4],
                                         lower[5])
                    print msg
                if upper:
                    msg = ("After this time; Entity:{0} belonged to "
                           "Hostgroup:{1} from {2} to "
                           "{3}").format(entity, upper[1], upper[4],
                                         upper[5])
                    print msg
        else:
            for r in result:
                msg = ("Entity: {0} was in HostGroup: {1} "
                       "from {2} to {3}").format(entity, r[1], r[4], r[5])
                print msg


def find_entity(entries, hostgroup, start_tstamp, end_tstamp):
    """
    Finds and returns a list of entitites which belonged to
    a particular host group for a given time window
    Returns None if the given hostgroup cannot be found
    """
    hg_entries = filter(lambda x: x[1] == hostgroup, entries)
    if not hg_entries:
        print "HostGroup:{0} does not occur in the file".format(hostgroup)
    else:
        result = _find_entity(hg_entries, start_tstamp, end_tstamp)
        if not result:
            print "No entities belonged to this hostgroup for given time"
        else:
            print "\n".join(result)
            print "The hostgroup had {0} entities for given time".format(
                len(result))


def work(csv_file, start_tstamp=None, end_tstamp=None, entity=None,
         hostgroup=None):
    """
    Do the actual work
    """
    entries = process_csv_file(csv_file)
    if start_tstamp is None:
        raise ValueError(
            "Start Tiemstamp cannot be None")
    if end_tstamp is None:
        end_tstamp = start_tstamp
    if start_tstamp > end_tstamp:
        raise ValueError(
            "Start Timestamp cannot be more than Ending Timestamp")
    if entity:
        find_hostgroup(entries, entity, start_tstamp, end_tstamp)
    elif hostgroup:
        find_entity(entries, hostgroup, start_tstamp, end_tstamp)


def main():
    opts, csv_file = parse_args()
    work(csv_file, **opts)


if __name__ == '__main__':
    main()
