difference-ts.scala
===================

Description
-----------

Scala script to find out first difference in timestamps for a `metric_id` per entity

How to execute script
----------------------
```bash
spark-submit <script_name>
```

* Scala files are not to be run directly run from shell. Commands have to be copied into spark-shell.

Input/Output
------------
#### Input
```json
{"timestamp":"1454482770000","body":"{\"TimeInterval\":299.6,\"PercIRQ\":0.0,\"CounterDiscrepancies\":0.0,\"PercNice\":0.0,\"PercIdle\":99.8,\"PercSoftIRQ\":0.0,\"PercUser\":0.12,\"PercIOWait\":0.0,\"PercSystem\":0.08}","toplevel_hostgroup":"hadoop","entity":"lxfsrb39a08","producer":"lemon","submitter_host":"lxfsrb39a08.cern.ch","metric_id":"9011","submitter_environment":"rtb3","type":"metric","metric_name":"CPUutil","submitter_hostgroup":"hadoop/eos1","version":"3.0"}
```


#### Output
```json
114.0, 300.0, 300.0, 300.0, 300.0, 300.0, 300.0, 300.0, 300.0, 300.0, 300.0, 300.0, 300.0, 300.0, 300.0, 300.0, 300.0, 300.0, 300.0, 300.0
```

Parameters
----------

* APP_NAME: Any string
* CPU_METRIC_ID: List of metric_id to be aggregated
* DATA_FILE: Input file from hdfs
* RESULT_PATH: Output filename


