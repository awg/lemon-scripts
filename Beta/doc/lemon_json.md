LeMon record format
----------------------

A sample LeMon record looks like this

```json
{
"body": JSON_STRING,
"entity": "b674075109",
"submitter_host": "b674075109.cern.ch",
"metric_id": "METRIC_ID",
"submitter_environment": "qa",
"type": "metric",
"version": "3.0",
"timestamp": "1463227200000",
"toplevel_hostgroup": "bi",
"producer": "lemon",
"aggregated": "daily",
"metric_name": "METRIC_NAME",
"submitter_hostgroup":"bi/batch/gridworker/aishare/share/shareshort"
}
```

where
* ``body`` is a JSON string dependent on the METRIC_ID (Each metric has a different body)
* ``aggregated`` can be None (for raw entries), ``"daily"`` (for daily aggregates) or ``hourly`` (for hourly aggregates)
* ``entity`` and ``submitter_host`` __may__ be different (Need to check this)

All metric bodies can be seen in [this](metric_bodies_printable) file
