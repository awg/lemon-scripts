lemon-mult-format.py
===================

Description
-----------

Pyspark script for extracting information of `metric_id` 13184 with parameterized argument and saving the output in csv format.
Timestamp is in UTC. Aggregations are done on UTC time.

How to execute script
----------------------
```bash
spark-submit --master yarn-client <script_name>
```

Input/Output
------------
#### Input
```json
{"timestamp":"1454292056000","body":"{\"TimeInterval\":299.5,\"PercIRQ\":0.0,\"PercGuestNice\":0.0,\"CounterDiscrepancies\":0.0,\"PercNice\":0.02,\"PercIdle\":99.32,\"PercSoftIRQ\":0.0,\"PercUser\":0.4,\"PercIOWait\":0.1,\"PercSteal\":0.0,\"PercGuest\":0.0,\"PercSystem\":0.15}","toplevel_hostgroup":"bi","entity":"lxbst2016","producer":"lemon","submitter_host":"lxbst2016.cern.ch","metric_id":"13184","submitter_environment":"qa","type":"metric","metric_name":"cpu_utilization","submitter_hostgroup":"bi/batch/gridworker/mpi/cfd","version":"3.0"}
{"timestamp":"1454303899000","body":"{\"TimeInterval\":299.44,\"PercIRQ\":0.0,\"PercGuestNice\":0.0,\"CounterDiscrepancies\":0.0,\"PercNice\":0.02,\"PercIdle\":99.6,\"PercSoftIRQ\":0.0,\"PercUser\":0.1,\"PercIOWait\":0.12,\"PercSteal\":0.0,\"PercGuest\":0.0,\"PercSystem\":0.15}","toplevel_hostgroup":"bi","entity":"lxbst2023","producer":"lemon","submitter_host":"lxbst2023.cern.ch","metric_id":"13184","submitter_environment":"qa","type":"metric","metric_name":"cpu_utilization","submitter_hostgroup":"bi/batch/gridworker/mpi/eng","version":"3.0"}
{"timestamp":"1454333624000","body":"{\"TimeInterval\":299.38,\"PercIRQ\":0.0,\"PercGuestNice\":0.0,\"CounterDiscrepancies\":0.0,\"PercNice\":95.48,\"PercIdle\":0.0,\"PercSoftIRQ\":0.03,\"PercUser\":0.18,\"PercIOWait\":0.0,\"PercSteal\":0.2,\"PercGuest\":0.0,\"PercSystem\":4.1}","toplevel_hostgroup":"bi","entity":"b6202cf432","producer":"lemon","submitter_host":"b6202cf432.cern.ch","metric_id":"13184","submitter_environment":"qa","type":"metric","metric_name":"cpu_utilization","submitter_hostgroup":"bi/condor/gridworker/share","version":"3.0"}
{"timestamp":"1454365919000","body":"{\"TimeInterval\":299.46,\"PercIRQ\":0.0,\"PercGuestNice\":0.0,\"CounterDiscrepancies\":0.0,\"PercNice\":0.02,\"PercIdle\":99.53,\"PercSoftIRQ\":0.0,\"PercUser\":0.1,\"PercIOWait\":0.12,\"PercSteal\":0.0,\"PercGuest\":0.0,\"PercSystem\":0.22}","toplevel_hostgroup":"bi","entity":"lxbst2005","producer":"lemon","submitter_host":"lxbst2005.cern.ch","metric_id":"13184","submitter_environment":"qa","type":"metric","metric_name":"cpu_utilization","submitter_hostgroup":"bi/batch/gridworker/mpi/cfd","version":"3.0"}
{"timestamp":"1454391046000","body":"{\"TimeInterval\":299.46,\"PercIRQ\":0.0,\"PercGuestNice\":0.0,\"CounterDiscrepancies\":0.0,\"PercNice\":0.02,\"PercIdle\":99.67,\"PercSoftIRQ\":0.0,\"PercUser\":0.15,\"PercIOWait\":0.1,\"PercSteal\":0.0,\"PercGuest\":0.0,\"PercSystem\":0.07}","toplevel_hostgroup":"bi","entity":"lxbst2030","producer":"lemon","submitter_host":"lxbst2030.cern.ch","metric_id":"13184","submitter_environment":"qa","type":"metric","metric_name":"cpu_utilization","submitter_hostgroup":"bi/batch/gridworker/mpi/eng","version":"3.0"}
```

#### Output

entity_name, aggregation time, metric_id, extra_info, (min, max, average, standard_deviation, count) for each value from the list specified

```
b6a780db7e,2016-03-06 13 +0000,13184,,0.11,0.3,0.148333333333,0.0351666666667,13,67.19,77.84,72.7866666667,111.789066667,13,7.6,8.19,7.85833333
333,0.534366666667,13,11.53,18.91,15.9666666667,77.9680666667,13,0.75,3.83,1.84083333333,10.1780916667,13,0.03,0.1,0.0641666666667,0.0058916666
6667,13,0.0,0.0,0.0,0.0,13,0.98,1.87,1.33333333333,0.905666666667,13,0.0,0.0,0.0,0.0,13,0.0,0.0,0.0,0.0,13
b675db012c,2016-03-19 13 +0000,13184,,0.05,1.71,0.2225,2.435225,13,82.7,94.92,90.085,243.7951,13,0.91,1.5,1.17,0.4058,13,2.98,14.32,7.11,209.44
26,13,0.06,1.23,0.300833333333,1.25789166667,13,0.01,0.11,0.0416666666667,0.00856666666667,13,0.0,0.0,0.0,0.0,13,0.72,1.55,1.06916666667,0.6740
91666667,13,0.0,0.0,0.0,0.0,13,0.0,0.0,0.0,0.0,13
b6191a67b1,2016-03-17 02 +0000,13184,,0.25,4.28,0.665833333333,14.3606916667,13,48.11,81.95,64.7483333333,1076.41436667,13,1.04,1.99,1.46416666
667,0.873091666667,13,13.47,41.7,29.1575,972.932225,13,1.06,7.88,3.1175,51.758225,13,0.13,0.27,0.199166666667,0.0230916666667,13,0.0,0.0,0.0,0.
0,13,0.39,0.98,0.6475,0.305225,13,0.0,0.0,0.0,0.0,13,0.0,0.0,0.0,0.0,13
b6f2020780,2016-03-26 21 +0000,13184,,0.05,0.83,0.1525,0.560625,12,0.01,0.01,0.01,0.0,12,0.04,0.22,0.105,0.0335,12,98.67,99.63,99.4425,0.836225
,12,0.23,0.37,0.291666666667,0.0209666666667,12,0.0,0.0,0.0,0.0,12,0.0,0.0,0.0,0.0,12,0.0,0.0,0.0,0.0,12,0.0,0.0,0.0,0.0,12,0.0,0.0,0.0,0.0,12
b6d20d59c9,2016-03-17 00 +0000,13184,,0.08,1.79,0.379166666667,2.47389166667,13,12.05,61.74,39.6866666667,4973.24346667,13,0.54,1.38,0.9025,0.8
72625,13,32.45,81.18,54.3175,4557.836225,13,2.29,12.75,4.63666666667,91.9260666667,13,0.0,0.01,0.000833333333333,9.16666666667e-05,13,0.0,0.0,0
.0,0.0,13,0.0,0.16,0.075,0.0425,13,0.0,0.0,0.0,0.0,13,0.0,0.0,0.0,0.0,13
```

Parameters
----------

* APP_NAME: Any string
* CPU_METRIC_ID: A dictionary with ```metric_id``` as key and list of values to be picked up from body for aggregation. Currently implemented for 13184
* DATA_FILE: Input file from hdfs
* RESULT_PATH: Output filename
* INTERVAL: Aggregation interval (string) from implemented values. Possible values are ```"weekly"```, ```"daily"```, ```"hourly"```

