lemon-mult-format.py
===================

Description
-----------

Pyspark script for extracting multiple `metric_id`s and saving in a file in csv format.

How to execute script
----------------------
```bash
spark-submit --master yarn-client <script_name>
```

Input/Output
------------
#### Input
```json
{"timestamp":"1454482770000","body":"{\"TimeInterval\":299.6,\"PercIRQ\":0.0,\"CounterDiscrepancies\":0.0,\"PercNice\":0.0,\"PercIdle\":99.8,\"PercSoftIRQ\":0.0,\"PercUser\":0.12,\"PercIOWait\":0.0,\"PercSystem\":0.08}","toplevel_hostgroup":"hadoop","entity":"lxfsrb39a08","producer":"lemon","submitter_host":"lxfsrb39a08.cern.ch","metric_id":"9011","submitter_environment":"rtb3","type":"metric","metric_name":"CPUutil","submitter_hostgroup":"hadoop/eos1","version":"3.0"}
{"timestamp":"1458514464000","body":"{\"TimeInterval\":298.1,\"PercIRQ\":0.0,\"CounterDiscrepancies\":0.0,\"PercNice\":0.0,\"PercIdle\":98.58,\"PercSoftIRQ\":0.01,\"PercUser\":0.83,\"PercIOWait\":0.1,\"PercSystem\":0.48}","toplevel_hostgroup":"bi","entity":"aiadm702","producer":"lemon","submitter_host":"aiadm702.cern.ch","metric_id":"9011","type":"metric","submitter_environment":"qa","metric_name":"CPUutil","submitter_hostgroup":"bi/inter/adm/login","version":"3.0"}
{"timestamp":"1457148668000","body":"{\"TimeInterval\":297.47,\"PercIRQ\":0.0,\"CounterDiscrepancies\":0.0,\"PercNice\":0.0,\"PercIdle\":98.41,\"PercSoftIRQ\":0.0,\"PercUser\":1.0,\"PercIOWait\":0.1,\"PercSystem\":0.48}","toplevel_hostgroup":"bi","entity":"aimadm100","producer":"lemon","submitter_host":"aimadm100.cern.ch","metric_id":"9011","type":"metric","submitter_environment":"qa","metric_name":"CPUutil","submitter_hostgroup":"bi/inter/adm/multi","version":"3.0"}
{"timestamp":"1457884830000","body":"{\"TimeInterval\":299.62,\"PercIRQ\":0.0,\"CounterDiscrepancies\":0.0,\"PercNice\":0.07,\"PercIdle\":98.61,\"PercSoftIRQ\":0.0,\"PercUser\":0.3,\"PercIOWait\":0.61,\"PercSystem\":0.41}","toplevel_hostgroup":"bi","entity":"aiadm070","producer":"lemon","submitter_host":"aiadm070.cern.ch","metric_id":"9011","submitter_environment":"qa","type":"metric","metric_name":"CPUutil","submitter_hostgroup":"bi/inter/adm/login","version":"3.0"}
```


#### Output
```json
b6ce243f42,2016-03-29 05 +0000,9011,,34.79,46.17,43.2783333333,140.245966667,13,0.79,1.84,1.1075,0.920225
b68af4ade0,2016-03-31 08 +0000,9011,,10.08,13.91,11.0633333333,10.0316666667,13,0.61,1.24,0.941666666667,0.428966666667
b62b4c5ad6,2016-03-17 08 +0000,9011,,23.45,26.09,25.6166666667,5.48546666667,13,0.37,0.57,0.415,0.0345
b631d4b22c,2016-03-23 12 +0000,9011,,5.16,14.7321875,7.46408482143,99.4003024344,14,0.57,5.51,1.49776785714,20.4378811136
b66a20ef05,2016-03-21 03 +0000,9011,,10.49,11.02,10.8428205128,0.215257692308,13,0.54,0.8,0.693461538462,0.0822817307692
b64ace2fb1,2016-03-25 15 +0000,9011,,0.0,8.16,1.15583333333,79.1086916667,13,0.0,0.91,0.095,0.7603
b6f2d21b8f,2016-03-06 04 +0000,9011,,98.58,99.1,98.8375,0.388825,13,0.81,1.23,1.01666666667,0.251866666667
b61e074f3a,2016-03-07 05 +0000,9011,,99.54,99.76,99.6566666667,0.0548666666667,13,0.16,0.25,0.209166666667,0.00689166666667
```

Parameters
----------

* APP_NAME: Any string
* CPU_METRIC_IDs: List of metric_id (strings) to be aggregated. Currently supports only 9011 and 9028
* DATA_FILE: Input file from hdfs
* RESULT_PATH: Output filename
* INTERVAL: Aggregation interval. Chose from 'hourly', 'daily' or 'weekly'.
 

