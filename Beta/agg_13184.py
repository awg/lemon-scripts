# Script which aggregates metric 13184 over user specified interval (hourly, daily, weekly, monthly and yearly)
# USAGE: script file_name TIME_DURATION
from __future__ import print_function
import datetime
import itertools
import json
import optparse
import sys
import time
import uuid

from pyspark import (HiveContext,
                     SparkConf,
                     SparkContext)
from pyspark.sql.functions import (avg,
                                   count,
                                   max,
                                   min,
                                   udf)
from pyspark.sql.types import (ArrayType,
                               FloatType,
                               IntegerType,
                               StringType,
                               StructField,
                               StructType)


APP_NAME = "Aggregate results by entity and modified hostgroup"
EXPECTED_TIME_DURATIONS = ("hourly", "daily", "weekly")
BODY_SCHEMA = StructType([StructField("TimeInterval", FloatType(), True),
                          StructField("PercIRQ", FloatType(), True),
                          StructField("PercGuestNice", FloatType(), True),
                          StructField("CounterDiscrepancies", FloatType(), True),
                          StructField("PercNice", FloatType(), True),
                          StructField("PercIdle", FloatType(), True),
                          StructField("PercSoftIRQ", FloatType(), True),
                          StructField("PercUser", FloatType(), True),
                          StructField("PercIOWait", FloatType(), True),
                          StructField("PercSteal", FloatType(), True),
                          StructField("PercGuest", FloatType(), True),
                          StructField("PercSystem", FloatType(), True)])
EXCLUDED_VALUES = ["TimeInterval"]


def main(file_names, options):
    # Initialize an empty conf so that spark-submit options can fill it
    conf = SparkConf()
    # But we set master explicilty
    conf = conf.setMaster("yarn-client").setAppName(APP_NAME)
    # Build the spark context
    sc = SparkContext(conf=conf)
    sc.setLogLevel("FATAL")
    hiveContext = HiveContext(sc)
    work(hiveContext, file_names, options["result_file"], write_mode=options["mode"],
         time_duration=opts["time_duration"], repartition_number=opts["num_partitions"])


def _remove_toplevel_hostgroup(sh, th):
    sh = sh.lstrip("/")
    if sh.startswith(th):
        submitter_hostgroup = sh[len(th):]
    else:
        submitter_hostgroup = sh
    submitter_hostgroup = submitter_hostgroup.lstrip("/")
    return submitter_hostgroup


def work(context, file_names, result_file, write_mode="",
         time_duration="daily", repartition_number=None):
    """
    Function that does the real work of filtering and mashing
    """
    fileschema = StructType([
        StructField("aggregated", StringType()),
        StructField("body", StringType()),
        StructField("entity", StringType()),
        StructField("metric_id", StringType()),
        StructField("metric_name", StringType()),
        StructField("producer", StringType()),
        StructField("submitter_environment", StringType()),
        StructField("submitter_host", StringType()),
        StructField("submitter_hostgroup", StringType()),
        StructField("timestamp", StringType()),
        StructField("toplevel_hostgroup", StringType()),
        StructField("type", StringType()),
        StructField("version", StringType())
    ])
    dataframe = context.read.json(*file_names, schema=fileschema)
    # First filter only results of metric 13184
    dataframe = dataframe.filter(dataframe["metric_id"]=="13184")
    # Also consider only entries that have None for aggregated (We don't want daily or hourly
    # to skew the result (howsoever slightly)
    dataframe = dataframe.filter("aggregated is null")
    # Divide the timestamp by the interval so that all timestamps within an interval have the same value
    # The following function is defined within this fucntion so that "time_duration"
    # does not need to be passed (as that is not possible in pyspark)
    def _custom_timestamp_forgrouping(ts):
        """
        Function that converts a datetime to a custom string suitable for grouping
        and display based on time duration
        :param ts: The unix timetsamp (in ms)
        Returns a string
        If time_interval is "hourly", returns DD-MM-YYYY HH
        If time_interval is "daily" return DD-MM-YYYY
        If time interval is "weekly" returns YYYY Week:W
        """
        # Convert timestamp to seconds
        ts = int(ts)/1000
        # Convert the unix ts to UTC datetime (no time zone info), but this naive object
        # is always in UTC
        d = datetime.datetime(*time.gmtime(int(ts))[:6])
        if time_duration == "weekly":
            # Return as Year, Week #
            cal = d.date().isocalendar()
            year_string = d.strftime("%Y")
            return "{0} Week:{1}".format(year_string, cal[1])
        if time_duration == "daily":
            return d.strftime("%d-%m-%Y")
        if time_duration == "hourly":
            return d.strftime("%d-%m-%Y %H")
        # Default return ISO Format
        return d.isoformat()
    tsudf = udf(_custom_timestamp_forgrouping, StringType())
    dataframe = dataframe.withColumn("group_interval", tsudf(dataframe["timestamp"]))
    # Decode the body as a separate column
    bodyudf = udf(lambda body: json.loads(body), BODY_SCHEMA)
    dataframe = dataframe.withColumn("body_decoded", bodyudf(dataframe["body"]))
    # Modify the submitter_hostgroup so that it does not have toplevel_hostgroup
    hgroupudf = udf(_remove_toplevel_hostgroup, StringType())
    dataframe = dataframe.withColumn("submitter_subhostgroup", hgroupudf(dataframe["submitter_hostgroup"], dataframe["toplevel_hostgroup"]))
    res = dataframe.groupBy(dataframe["entity"], dataframe["submitter_subhostgroup"], dataframe["toplevel_hostgroup"], dataframe["group_interval"])
    # As body_decoded has all the entries, we will run summary functions directly on them. For this
    # we will define columns on which we don't want summary and ignore them
    list_of_agg = itertools.chain([max("body_decoded." + z).alias("max_" + z) for z in [x.name for x in BODY_SCHEMA.fields if x.name not in EXCLUDED_VALUES]],
                                  [min("body_decoded." + z).alias("min_" + z) for z in [x.name for x in BODY_SCHEMA.fields if x.name not in EXCLUDED_VALUES]],
                                  [avg("body_decoded." + z).alias("avg_" + z) for z in [x.name for x in BODY_SCHEMA.fields if x.name not in EXCLUDED_VALUES]],
                                  [count("*").alias("num_entries")])
    # Apply and coalesce
    result_dataframe = res.agg(*list_of_agg)
    if repartition_number and repartition_number > 0:
        result_dataframe = result_dataframe.coalesce(repartition_number)
    if write_mode == "append":
        result_dataframe.write.mode("append").json(result_file)
    else:
        result_dataframe.write.json(result_file)


def parse_args(parser):
    """
    Parses user supplied options and arguments.
    Returns a tuple (opts, arg) where
      - opts - Is a dict with following keys
        - "mode": String. None for default or "append"
        - "time_duration": String. "hourly", "daily", "weekly".
        - "num_partitions": Integer. 0 For no number
        - "result_file": String. The name of the result file
      - args - Is a list of filenames to be processed
    """
    o, a = parser.parse_args()
    # Sanity checking for args
    opts = {"mode": None, "num_partitions": 0, "result_file": "", "time_duration": "daily"}
    if o.num_partitions:
        try:
            num_part = int(o.num_partitions)
        except ValueError:
            pass
        else:
            if num_part > 0:
                opts["num_partitions"] = num_part
            if num_part > 20:
                opts["num_partitions"] = 20
    if o.result_file:
        opts["result_file"] = o.result_file
        opts["mode"] = "append"
    else:
        opts["result_file"] = "result_" + uuid.uuid4().get_hex()
    if o.time_duration:
        try:
            TIME_DURATION = o.time_duration.lower() 
        except AttributeError:
            TIME_DURATION = ""
    else:
        TIME_DURATION = "daily"    
    if TIME_DURATION not in EXPECTED_TIME_DURATIONS:
        print("Changed unsupported duration {0} to 'daily'".format(TIME_DURATION), file=sys.stderr)
        TIME_DURATION = "daily"
    opts["time_duration"] = TIME_DURATION
    return opts, a


if __name__ == '__main__': 
    parser = optparse.OptionParser()
    parser.add_option("-n", "--num-partitions", dest="num_partitions",
                   help=("Specify the number of partitions of the output data. "
                         "Should be a non-zero positive integer. Maximum possible value is 20"))
    parser.add_option("-r", "--result-file", dest="result_file",
                   help=("Specify the result file name. If this is specified, the output will be written in"
                         " append mode. If not specified, a random name will be chosen for result"))
    parser.add_option("-t", "--time-duration", dest="time_duration",
                      help=("Specify the time duration over which aggregation should happen."
                            " Can be one of - hourly, daily, weekly. Default 'daily'"))
    opts, args = parse_args(parser)
    if len(args) == 0:
        parser.print_help()
        sys.exit(-1)
    FILE_NAMES = args
    # Result Name computation
    main(FILE_NAMES, opts)
    print("Result is in in {0}".format(opts["result_file"]))
