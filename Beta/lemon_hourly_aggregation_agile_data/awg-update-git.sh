#!/bin/bash - 
#===============================================================================
#
#          FILE: awg-update-git.sh
# 
#         USAGE: ./awg-update-git.sh
# 
#   DESCRIPTION: update git repository
# 
#       OPTIONS: ---
#  REQUIREMENTS: git
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Luca Menichetti (lmeniche), luca.menichetti@cern.ch
#  ORGANIZATION: CERN
#       CREATED: 16/06/2016 14:30
#      REVISION: ---
#===============================================================================

cd $(dirname $0)

GIT_BRANCH="master"

git remote -v update > /dev/null 2>&1

git checkout $GIT_BRANCH > /dev/null 2>&1

status=$(git status)

if [[ $status == *"Your branch is behind 'origin/$GIT_BRANCH'"* ]]
then
    echo "Pulling changes for branch $GIT_BRANCH" > /dev/null
    git pull origin $GIT_BRANCH > /dev/null 2>&1
    # if a jar is present and maven has to run
    # cd $SPARK_PROJECT
    # mvn clean package -DskipTests
    # cd -
else
    echo "Already up-to-date." > /dev/null
fi
