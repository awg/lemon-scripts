## Lemon Hourly Aggregation and Join
    It computes hourly aggregation of each metric, from LEMON data and then join the aggregated data of all the metrics into a single table with all 
    the attributes. 

Following files are used for aggregation and join of data:
1. [``lemon_hourly_aggregation_join.py``](#lemon_agg_join) - The spark job that performs the aggregation and join on lemon data. It does aggregate
of each metric and then join them on common attributes. 

2. [``hourly-agg-join-acrontab.sh``](#acrontab_agg_join) - The shell script that can be used to perform hourly aggregations for ``cloud_compute`` 
hostgroups and metrics. This can be automated via [acron] (http://cnlart.web.cern.ch/cnlart/221/node28.html)

3. [``awg-update-git.sh``](#awg_update_git) - A special shell script that can be used to update this repository. It is a good practice to execute this script before executing any ``production/`` or ``Beta/`` code.


## <a name="lemon_agg_join">``lemon_hourly_aggregation_join.py``</a>

This pyspark script takes the monthly LeMon data file as input. It also accepts a host of other options.
For full usage details, run

```bash
lemon_hourly_aggregation_join.py --help
```

The script reads all LeMon records from the data file and groups them by
``(entity,hostgroup,aggregated,date_hour,*METRIC_SPECIFIC_ATTR)``

Further the script join the aggregates of all metrics into one dataset (table) with all the attributes, grouped by 
``(entity,hostname,project,group_interval)``
where ``hostname`` and ``project`` are metric attributes. ``Entity`` and ``group_interval`` are common attributes.

It deduces the name of HG and the MONTH and YEAR from the input file name.
The result is written to a file whose name depends of METRICS, HG and YEAR_MONTH and ends in
``.tmp``. This result can be moved to a ``.tmp``-less name by using ``hdfs dfs -mv``
(For full information, read the help associated with ``-T`` option)

### Supported Metrics

This script supports many LeMon metrics which have ``hostname`` and ``project`` attributes as the final joined data of aggregated metric is grouped by hostname and project. A list of all metrics supported can be printed
by passing the ``-L`` option

e.g. Supported metrics are 13504,13505,13506 and 13507

#### Adding new metrics

To add new metrics, the python script must be modified by adding the schema of the desired metric to the script. There are four dictionaries in the script that hold metric related values:
* ``BODY_SCHEMA`` - Holds the schema of the metric. Add the new metric ID (as string) as key and the schema (``StructType()``) as value to this dictionary. See existing schemas for examples. Any new metric MUST have an entry here or it won't be recognized by program. Metric must have hostname and project attributes.
* ``EXCLUDED_VALUES`` - Holds the names of attributes of body that must be excluded from aggregation for the metric. Add the new metric ID (as string) as key and a list of strings (attributes names) as values. If no values are to be excluded you can omit the key or add empty list ``[]``
* ``GROUPING_VALUES`` - Specify custom attributes that should also be part of grouping while aggregating. Add the new metric ID (as string) as key and a list of strings (attributes names) as values. In this script hostname and project are added for all the metrics.
* ``COMMON_VALUES`` - There are some metrics which have common attributes. Specify all such attributes here. Add the metric ID (as string) as key and a list of strings (common attributes names) as values. e.g. Metric 13506 and 13507 have 'read_bytes and 'write_bytes' as common attributes.      

### Output

The script outputs JSON to HDFS. The scripts first compute aggregate of all metrics and then join them based on some common attributes.
The schema of the output JSON depends on the metrics for which joining of aggregated metric data is being performed.
However, irrespective of the metric, there are some common attributes in the output.

# ```output of an aggregation of metric ```

```
root
 |-- entity: string (nullable = true)
 |-- group_interval_MetricId: string (nullable = true)
 |-- num_entries_MetricId: long (nullable = false)
 |-- min_ts_MetricId: long (nullable = true)
 |-- max_ts_MetricId: long (nullable = true)
 |-- type_MetricId: string (nullable = true)
 |-- submitter_hostgroup_MetricId: string (nullable = true)
```
All the above attributes (except entity) are suffixed with metricId to distinguish them from other metrics attributes. 
Entity is not suffixed with metricId because it is the part of join attribute.

 ``type`` can be one out of ``"raw"``, ``"hour"`` or ``"day"`` depending on what type
of LeMon records have been used to perform the aggregation.

There will be one line per ``entity``,``submitter_hostgroup``,``group_interval``, ``type``, ``CUSTOM_ATTR``
where ``CUSTOM_ATTR`` are some special attributes which are added for certain metrics

The schema of the result can be viewed by passing the ``-S`` option, which will not perform any aggregation
but just print the schema of the proposed output.

# ```output after join ```

```
root
 |-- entity: string (nullable = true)
 |-- hostname: string (nullable = true)
 |-- project: string (nullable = true)
 |-- group_interval: string (nullable = true)
```
join is perfrmed on the above four attributes where ``hostname`` and ``project`` are metric specific grouping attribute.


#### Output file name

The script will write result to a file ending in ``.tmp``. This can be renamed to actual file if the script runs correctly. This is to prevent job failures from erasing existing data when ``-o/--overwrite`` option is used.

By passing ``-T``, a tree like structure for output file can be enforced. Also by passing ``-p`` we can specify an HDFS path prefix to which output will be written.
Some examples of output file names for various input parameters are given here for clarity:

##### Using ``-p`` but no ``-T`` - Flat name in prefix HDFS path
```
$ spark-submit lemon_hourly_aggregation_join.py -m 13504,13505 -p lemon_aggregates /project/itmon/archive/lemon/cloud_compute/2016-10
Result is in: lemon_aggregates/result_agg_13504_13505_hourly_cloud_compute_2016_10.tmp
```

##### Using ``-T`` but no ``-p`` - Hierarchical name in current HDFS path
```
$ spark-submit lemon_hourly_aggregation.py -m 13504,13505 -T /project/itmon/archive/lemon/cloud_compute/2016-10
Result is in: ./cloud_compute/2016_10/hourly_13504_13505.tmp
```

##### Using ``-T`` and ``-p`` - Hierarchical name in prefix HDFS path
```
$ spark-submit lemon_hourly_aggregation.py -T -p /project/awg/lemon-aggregates -m 13504,13505 /project/itmon/archive/lemon/cloud_compute/2016-10
Result is in: /project/awg/lemon-aggregates/cloud_compute/2016_10/hourly_13504_13505.tmp
```

## <a name="acrontab_agg_join">``hourly-agg-join-acrontab.sh``</a>

The shell script which can formulate ``spark-submit`` commands for automated execution.

### Details

This shell script reads two files to figure out the hostgroups and the metrics that are to be processed

1. ``metrics.txt`` - Each line in the file denotes metric ID to be processed. A comma (,) separated string is generated using metric Ids. 
                     e.g. metricId1,metricId2,...metricIdn
2. ``hostgroups.txt`` - Each line in the file denotes one hostgroup to be processed. For metric Ids in (13504,13505,13506 and 13507) it is cloud_compute.

This script will run the lemon aggregation pyspark job for all metrics and hostgroup gathered from above files.
The pyspark job is run for the current month and a number of preceeding months controlled by script variable ``PREV_MONTHS`` within the shell script.
If successful, the script moves the output HDFS file from .tmp to the filename without .tmp
This ensures that a failure does not leave the output file blank.

The command formulated by the script is something like
```bash
spark-submit lemon_hourly_aggregation_join.py -m metricId1,metricId2,...metricIdn /project/itmon/archive/lemon/HOSTGROUP/YEAR-MONTH
```

The script creates ``spark_log/`` subdirectory (under this directory) and stores the job's stdout and stderr
under a file ``lemon-agg-HG-metricId1_metricId2_...metricIdn-MONTH_YEAR``.

For error reporting, the script reads e-mail addresses from ``subscribers.txt`` and sends a notification e-mail in case
any job fails.



### Running through acron

As always, the first step should be to update the repository so that code changes can be pulled. This can be done by calling
the [``awg-update-git.sh``](./awg-update-git.sh) script.

#### Sample acron line

```bash
1 0 * * 1 awgjobsubmit.cern.ch cd /home/awgmgr/lemon-scripts/Beta/lemon_hourly_aggregation_agile_data ; bash awg-update-git.sh ; bash hourly-agg-join-acrontab.sh
```

