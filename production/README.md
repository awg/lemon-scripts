All production ready code goes here.

## ``awg-update-git.sh``

A special shell script that can be used to update this repository. It is a good practice to execute this script *before* executing any ``production/`` or ``Beta/`` code.

## Command example

```bash
1 0 * * 1 awg-jobsubmit.cern.ch cd /home/awgmgr/lemon-scripts/production ; bash awg-update-git.sh ; bash lemon_hourly_aggregation/hourly-agg-acrontab.sh
```
