#!/bin/bash - 
#===============================================================================
#
#          FILE: hourly-agg-join-target-month.sh
# 
#         USAGE: ./hourly-agg-join-target-month.sh YYYY-MM
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Luca Menichetti, Ajay Kumar
#  ORGANIZATION: CERN
#       CREATED: 19/12/2016 17:48
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

cd $(dirname $0)

TARGET_MONTH=${1-$(date +'%Y-%m')}
FAIL_LOGS=()

if [[ "$TARGET_MONTH" =~ 20[0-9][0-9]-[0-1][0-9] ]]; then

    # Read metrics and hostgroups
    mapfile -t METRICS < "$PWD/metrics.txt"
    mapfile -t HGS < "$PWD/hostgroups.txt"

    DATE=$(date +'%F')
    SECOND_SINCE_EPOCH_UTC=$(date +'%s')

    OUTPUT_LOGS_FOLDER="$PWD/spark_log"

    # Jars and libs
    LIBS_FOLDER="$PWD"
    SPARK_JOB="$LIBS_FOLDER/lemon_hourly_aggregation_join.py"

    # Variables for the SPARK job
    HDFS_OUTPUT_PREFIX="/project/awg/lemon-aggregates"
    LOGLEVEL="WARN"

    SPARK_OPTS='--driver-class-path=/usr/lib/hive/lib/* --driver-java-options="-Dspark.executor.extraClassPath=/usr/lib/hive/lib/*"'

    COMMIT_ID=$(git show -s | grep commit | awk '{print "Git commit id: "$2}') 2>&1

    mkdir -p $OUTPUT_LOGS_FOLDER 2>/dev/null

    # Job parameters
    for hg in ${HGS[@]}; do
        STR_METRIC_OPTION=""
        COMMA=","
        for metric in ${METRICS[@]}; do
            STR_METRIC_OPTION=$STR_METRIC_OPTION$COMMA$metric
        done; 
        STR_METRIC_OPTION=${STR_METRIC_OPTION:1}

        HDFS_INPUT="/project/itmon/archive/lemon/$hg/$TARGET_MONTH"
        STR_METRIC_OPTION_MOD=${STR_METRIC_OPTION//[,]/_}

        output_job_file="$OUTPUT_LOGS_FOLDER/lemon-agg-$hg-$STR_METRIC_OPTION_MOD-$TARGET_MONTH"
        echo $COMMIT_ID >> $output_job_file 2>&1
        echo "Target month:" $TARGET_MONTH >> $output_job_file 2>&1
        echo "Execution timestamp: $SECOND_SINCE_EPOCH_UTC" >> $output_job_file 2>&1
        echo "STR_METRIC_OPTION $STR_METRIC_OPTION" >> $output_job_file 2>&1

        CMD="spark-submit $SPARK_OPTS $SPARK_JOB -T -p $HDFS_OUTPUT_PREFIX -l $LOGLEVEL -o "
        CMD=$CMD"-m $STR_METRIC_OPTION "
        CMD=$CMD"$HDFS_INPUT ";
        
        echo "COMMAND:" $CMD >> $output_job_file 2>&1
        $CMD >> $output_job_file 2>&1
        RET_CODE=$?
        if [ "$RET_CODE" != "0" ]; then
            FAIL_LOGS+=($output_job_file)
        else
            # Command succeeded, move to new file
            # Get the output file name
            exp="Result is in: (([^ ]+)\.tmp)"
            if [[ `cat $output_job_file` =~ $exp ]]; then
                orig_name="${BASH_REMATCH[1]}"
                new_name="${BASH_REMATCH[2]}"
                old_name=$new_name$".old"
                if hdfs dfs -test -e $new_name >> $output_job_file 2>&1 ; then
                    # code for new_name exists
                    hdfs dfs -mv $new_name $old_name >> $output_job_file 2>&1
                    hdfs dfs -mv $orig_name $new_name >> $output_job_file 2>&1
                    hdfs dfs -rm -r $old_name >> $output_job_file 2>&1
                else
                    # code for new_name does not exists
                    hdfs dfs -mv $orig_name $new_name >> $output_job_file 2>&1
                fi
            fi
        fi
    done;

else
    FAIL_LOGS+=("Impossible_to_recognize_target_month_to_process:_$TARGET_MONTH")
fi

EMAILS=$(cat subscribers.txt)
if [ ${#FAIL_LOGS[@]} -ne 0 ]; then
    (echo "Some of lemon hourly aggregations failed for target month $TARGET_MONTH." && echo "Here you can find a short errors list:" && for fl in ${FAIL_LOGS[@]}; do echo "$fl"; done;) | mail -s "ERROR: LeMon hourly aggregation $TARGET_MONTH" $EMAILS
fi

