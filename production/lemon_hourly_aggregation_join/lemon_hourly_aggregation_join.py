# Script which aggregates all entries for given metric over an hourly period for
# each entity, hostgroup and entry type (raw, hourly or daily aggregate)
# and then join all the aggrgated metrics into a single dataset which is 
# grouped by entity,hostname,project and group_interval
# The name of the result file is automatically inferred from input
# USAGE INSTRUCTIONS: script file_name --help
"""
Changelog:

1.0
----
- Initial versioned program
"""
from __future__ import print_function
import calendar
import datetime
import itertools
import json
import optparse
import os
import sys
import time

from pyspark import (HiveContext, SparkConf, SparkContext)
from pyspark.sql import Window
from pyspark.sql.functions import (avg, count, col, lit, max, min, rowNumber, udf)
from pyspark.sql.types import (ArrayType, FloatType, IntegerType, LongType, StringType, StructField, StructType)

VERSION= "1.0"
# Specify the schema of the metric body here.
# This is in form of a dictionary with *String* metric ID as key
# and StructType for the Schema as Value. Be sure to include all attributes
# metric must have hostname and project attributes 
BODY_SCHEMA = {
    "13504": StructType(
        [StructField("hostname", StringType(), True),
         StructField("project", StringType(), True),
         StructField("state", FloatType(), True),
         StructField("cpu_utilization", FloatType(), True)]
    ),
    "13505": StructType(
        [StructField("hostname", StringType(), True),
         StructField("project", StringType(), True),
         StructField("memory_actual", FloatType(), True),
         StructField("memory_unused", FloatType(), True)]
    ),
    "13506": StructType(
        [StructField("hostname", StringType(), True),
         StructField("project", StringType(), True),
         StructField("read_requests", FloatType(), True),
         StructField("read_bytes", FloatType(), True),
         StructField("write_requests", FloatType(), True),
         StructField("write_bytes", FloatType(), True),
         StructField("err", FloatType(), True)]
    ),
    "13507": StructType(
        [StructField("hostname", StringType(), True),
         StructField("project", StringType(), True),
         StructField("read_bytes", FloatType(), True),
         StructField("read_packets", FloatType(), True),
         StructField("read_error", FloatType(), True),
         StructField("read_drop", FloatType(), True),
         StructField("write_bytes", FloatType(), True),
         StructField("write_packets", FloatType(), True),
         StructField("write_error", FloatType(), True),
         StructField("write_drop", FloatType(), True)]
    )
}

# Specify all attributes of body that you don't want in the output.
# This is also a dictionary with *String* metric ID as key and a list
# of attribute names as values
EXCLUDED_VALUES = {}
# Specify custom groupng values. This is necessary for certain plugins
# which have additional elements in body over which grouping must be done.
# This is also in form of a dictionary with *String* metric ID as key
# and a list of attribute names as value
GROUPING_VALUES = {
    "13504": ["hostname", "project"],
    "13505": ["hostname", "project"],
    "13506": ["hostname", "project"],
    "13507": ["hostname", "project"]
}
#specify all the common attributes that multiple metrics may have in the body.
# As all the attributes of all metrics have to be included in the final joined data
# So its better to suffix these attributes with metricID so that they can be identified easily.
# This is also in form of a dictionary with *String* metric ID as key
# and a list of attribute names(common) as value
COMMON_VALUES = {
    "13506": ["read_bytes", "write_bytes"],
    "13507": ["read_bytes", "write_bytes"]
}


def main(file_name, options, result_name, hiveContext):
    result_df = work(hiveContext, file_name, result_name, write_mode=options["write_mode"],
                     metric_id=options["metric_id"], schema=options["schema"])
    return result_df


add = lambda x, y: x+y


def _hour_value(ts):
    if ts is None:
        return str(None)
    # Convert timestamp to seconds
    ts = int(ts)/1000
    # Convert the unix ts to UTC datetime (no time zone info), but this naive
    # object is always in UTC
    d = datetime.datetime(*time.gmtime(int(ts))[:6])
    return d.strftime("%d-%m-%Y %H")


def _hour_start_ts(hr):
    d = datetime.datetime.strptime(hr, "%d-%m-%Y %H")
    return calendar.timegm(d.timetuple())*1000


def _hour_end_ts(hr):
    d = datetime.datetime.strptime(hr, "%d-%m-%Y %H")
    return (calendar.timegm(d.timetuple()) + 3599)*1000


def _get_raw_aggregates(dataframe, metric_id):
    """
    Returns computed hourly aggregates for every entity and hostgroup pair
    from raw data
    The dataframe columns returned are
    entity, hostgroup, group_interval, num_entries, min_ts, max_ts, type
    min_*, max_*, avg_* where * is list of columns in BODY_SCHEMA[metric_id]
    Here - type is always raw
    """
    list_of_agg = [count("*").alias("num_entries"),
                   min("timestamp").alias("min_ts"),
                   max("timestamp").alias("max_ts")]
    result_df = _common_aggregation(dataframe, metric_id, list_of_agg)
    return result_df


def _common_aggregation(dataframe, metric_id, list_of_agg):
    grouping_fields = [dataframe.entity, dataframe.submitter_hostgroup,
                       dataframe.group_interval, dataframe.aggregated]
    if metric_id is not None:
        # Add any fields that may be plugin specific
        grouping_fields.extend(
            [getattr(dataframe.body_decoded, x).alias(x)
             for x in GROUPING_VALUES.get(str(metric_id), [])])
    res = dataframe.groupBy(*grouping_fields)
    if metric_id is not None:
        _excludes = itertools.chain(EXCLUDED_VALUES.get(str(metric_id), []),
                                    GROUPING_VALUES.get(str(metric_id), []))
        # itertools.chain is an iterator so once we move beyond a value, we
        # lose it. So cast it into a list
        _excludes = list(_excludes)
        _fields = [x.name for x in BODY_SCHEMA[str(metric_id)].fields
                   if x.name not in _excludes]
        # As body_decoded has all the entries, we will run summary functions
        # directly on them. For this we will define columns on which we don't
        # want summary and ignore them
        list_of_agg.extend(itertools.chain(
            [max("body_decoded." + z).alias("max_" + z) for z in _fields],
            [min("body_decoded." + z).alias("min_" + z) for z in _fields],
            [avg("body_decoded." + z).alias("avg_" + z) for z in _fields]))
    # Apply and coalesce
    result_dataframe = res.agg(*list_of_agg)
    return result_dataframe


def _set_type(agg):
    if agg == "hourly":
        return "hour"
    elif agg == "daily":
        return "day"
    return "raw"


def work(context, file_name, result_file, write_mode="error",
         metric_id=None, schema=False):
    """
    Function that does the real work of filtering and mashing
    """
    fileschema = StructType([
        StructField("aggregated", StringType()),
        StructField("body", StringType()),
        StructField("entity", StringType()),
        StructField("metric_id", StringType()),
        StructField("metric_name", StringType()),
        StructField("producer", StringType()),
        StructField("submitter_environment", StringType()),
        StructField("submitter_host", StringType()),
        StructField("submitter_hostgroup", StringType()),
        StructField("timestamp", StringType()),
        StructField("toplevel_hostgroup", StringType()),
        StructField("type", StringType()),
        StructField("version", StringType())
    ])
    dataframe = context.read.json(file_name, schema=fileschema)
    # Remove faulty entries
    dataframe = dataframe.filter("timestamp is not null")
    # Also remove all entries for which metric_name is None
    # These are in minority and have body as a list of numbers (strings)
    # rather than a dict (or a list of integers/floats) which are not
    # decoded properly
    dataframe = dataframe.filter("metric_name != ''")
    dataframe = dataframe.withColumn(
        "timestamp", dataframe["timestamp"].cast(LongType()))
    # Define an hour UDF for grouping
    hourudf = udf(_hour_value, StringType())
    dataframe = dataframe.withColumn("group_interval", hourudf("timestamp"))
    if metric_id is not None: 
        dataframe = dataframe.filter("metric_id='{0}'".format(metric_id))
        # Decode the body as a separate column. This is only done if we know
        # the metric ID
        bodyudf = udf(lambda body: json.loads(body), BODY_SCHEMA[str(metric_id)])
        dataframe = dataframe.withColumn("body_decoded",
                                         bodyudf("body"))
    # Get aggregated raw entries
    result = _get_raw_aggregates(dataframe, metric_id)
    typeudf = udf(_set_type, StringType())
    result = result.withColumn("type", typeudf("aggregated")).drop("aggregated")
    # If only schema is to be printed, do that
    #if schema:
        #result.printSchema()
    #else:
        #result.write.mode(write_mode).json(result_file)
    return result


def _build_result_name(inp, opts):
    """
    Depending on opts["tree_structure"], the result can have two names:
    1. If set to True, the output filename is a sub-directory structure
    HG/YEAR_MONTH/hourly_METRIC.tmp
    2. If set to False(default), the filename is simply
    result_agg_METRIC_hourly_HG_YEAR_MONTH.tmp
    In both cases all uppercase parameters are derived from input
    - HG - The toplevel hostgroup from input filename.
           ("bi", "aimon", "alice" etc.). This is the name of the parent
           directory of the input file. E.g. for input like
           /project/itmon/archive/lemon/bi/2016-07, the filename is 2016-07
           and parent is "bi" so HG="bi"
    - METRIC -  metric IDs (-m) 
    - YEAR_MONTH - Is derived from the name of the input file (last component
                   of the input file). All ".", "-" in filename are changed
                   to "_" to get the name. E.g. if filename is 2016-07.tmp,
                   the YEAR_MONTH value will be "2016_07_tmp"
    
    If user specifies an optional path prefix then the result will be in that
    path but name will be as above
    Note that the filename will essentially end in .tmp. The user can
    move the .tmp file to original name file if job runs without error.
    Returns a tuple file_name, toplevel HG name and month year name
    """
    comp = inp.split("/")
    if not opts["tree_structure"]:
        result_file = "result_agg_" + str(opts["metric_id"]) + "_hourly_"
    else:
        result_file = "hourly_" + str(opts["metric_id"])
    try:
        toplevel = comp[-2]
    except IndexError:
        toplevel = "unknown"
    try:
        month = comp[-1]
    except IndexError:
        month = "none"
    if not month:
        month = "none"
    month_year = month.replace("-", "_").replace(".", "_")
    if not opts["tree_structure"]:
        file_name = result_file + toplevel + "_" + month_year
    else:
        file_name = os.path.join(toplevel, month_year, result_file)
    file_name = file_name + ".tmp"
    file_name = os.path.join(opts["path_prefix"],file_name)
    return (file_name, toplevel, month_year)


if __name__ == '__main__': 
    parser = optparse.OptionParser(
        usage="%prog [-options] FILE", version=VERSION,
        epilog=("The name of the result file is automatically "
                  "inferred from options and input file"))
    parser.add_option("-m", "--metric-id", type="string", dest="metric_id",
                      help=("Specify the comma (,) separated values of metric IDs to be processed. "
                            "If not specified, It will not perform aggregation and joining"))
    parser.add_option(
        "-o", "--overwrite", dest="overwrite", action="store_true",
        help=("Overwrite the result file if it exists. Default is to"
              " raise an error"))
    parser.add_option(
        "-S", "--schema-print", dest="schema", action="store_true",
        default=False,
        help=("Only printthe schema of the output JSON."
              " Does no aggregation."))
    parser.add_option(
        "-L", "--list-plugins", dest="plugin_list", action="store_true",
        default=False,
        help=("printall supported plugins as a CSV"))
    parser.add_option(
        "-p", "--prefix", dest="path_prefix", default=".",
        help="Specify the HDFS path prefix to which the output will be saved")
    parser.add_option(
        "-l", "--log-level", dest="log_level", default="WARN",
        help=("Set the logging level. Values can be one out of those"
              " expected by context.setLogLevel, i.e. "
              "ALL, DEBUG, ERROR, FATAL, INFO, OFF, TRACE, WARN"
              ". Default WARN. Values are case insensitive"))
    parser.add_option(
        "-T", "--tree", dest="tree", default=False,
        action="store_true",
        help=("Store the result in directory tree. If this option is"
              " specified, the result will be saved in a hierarchical"
              " path HG/YEAR_MONTH/hourly_METRIC.tmp. If this is not"
              " specified (default), the result will be stored in"
              " a file called result_agg_METRIC_hourly_HG_YEAR_MONTH.tmp"
              ". NOTE: The filename will always end in .tmp"))
    opts, args = parser.parse_args()
    # If only supported plugins to be printed, that is easy
    if opts.plugin_list:
        sorted_plugin_list = sorted([int(x) for x in BODY_SCHEMA.keys()])
        print("The supported plugins are: {0}".format(
            ",".join(str(x) for x in sorted_plugin_list)))
        sys.exit(0)
    # Sanity checking of options and arguments
    try:
        FILE_NAME = args[0]
    except IndexError:
        print("Please specify a file name")
        parser.print_help()
        sys.exit(-1)
    if len(args) > 1:
        print("Please specify a single file name")
        sys.exit(-1)
    opt = {"write_mode": "error", "metric_id": None, "num_raw": None,
           "schema": opts.schema, "path_prefix": opts.path_prefix,
           "tree_structure": opts.tree, "loglevel": opts.log_level.upper()}
    if opts.overwrite:
        opt["write_mode"] = "overwrite"
    metric_list = []
    if opts.metric_id is not None:
        print("opts.metric_id is : {0}".format(opts.metric_id))
        metric_list = opts.metric_id.split(',')
        # Avoid -m 13504,13504 i.e. multiple definition of same metric
        if len(metric_list) > len(set(metric_list)):
                print("Please specifiy unique metrics IDs")
                sys.exit(-1)
        #print("len of metric_list is :{0}".format(len(metric_list)))
        #opt["metric_id"] = opts.metric_id
    else:
        print("Please specifiy comma separated values of metric IDs e.g. -m metricId1,metricId2,...metricIdn")
        sys.exit(-1)
    if len(metric_list) < 2:
        print("Please specify at least two metric IDs")
        sys.exit(-1)
    for metric in metric_list:
        print(metric)
        if str(metric) not in (BODY_SCHEMA.keys()):
                print("{0} is not a supported metric Id.Please specify a supported metric ID".format(metric))
                print("Supported Metric IDs - " + ", ".join(BODY_SCHEMA.keys()))
                sys.exit(-1)
    #if opt["metric_id"] and str(opt["metric_id"]) not in (BODY_SCHEMA.keys()):
        #print("Please specify a supported metric ID")
        #print("Supported Metric IDs - " + ", ".join(BODY_SCHEMA.keys()))
        #sys.exit(-1)
    if opt["loglevel"] not in (
            "ALL", "DEBUG", "ERROR", "FATAL", "INFO", "OFF",
            "TRACE", "WARN"):
        opt["loglevel"] = "WARN"
    #for metric in metric_list:
        #opt["metric_id"] = metric
        #print("call main() with metric id as {0}".format(opt["metric_id"]))
    # '_' separeated metric string which is be used to build result name. It is like metricId1_metricId2_...metricIdn
    result_metrics = ''
    for metric in metric_list:
        result_metrics += metric + '_'
    print("metric Ids to be included in rname :{0}".format(result_metrics))
    opt["metric_id"] = result_metrics[0:len(result_metrics)-1]
    #print("metric Ids to be included in rname :{0}".format(opt["metric_id"]))
    #Result Name computation
    rname, hg, mnth_yr = _build_result_name(FILE_NAME, opt)
    print("after returning from build_result name values are : rname is {0},hg is {1},mnth_yr is {2}".format(rname,hg,mnth_yr))
    opt["display"] = {"hostgroup": hg, "year_month": mnth_yr}
    APP_NAME = ("Lemon-Hrly-Agg-Join: Metric-{2} for '{0}' from '{1}'").format(
        opt["display"].get("hostgroup", "None"),
        opt["display"].get("year_month", "Unknown"),
        str(opt["metric_id"])
    )
    # " and input:{0}").format(file_name, options["metric_id"])
    # Initialize an empty conf so that spark-submit options can fill it
    conf = SparkConf()
    # But we set master explicilty
    conf = conf.setAppName(APP_NAME)
    # Build the spark context
    sc = SparkContext(conf=conf)
    sc.setLogLevel(opt["loglevel"])
    hiveContext = HiveContext(sc)
    # Dictionary of aggregated result of each metricId and their resulted join
    dict_df = {}
    # Compute aggregation of each metric, assign aggregation result to dictionary and
    # Rename submitter_hostgroup,type,min_ts,max_ts and num_entries of aggregated result after appending "_$MetricID"
    for metric in metric_list:
        opt["metric_id"] = metric
        dict_df["df_{0}".format(metric)] = main(FILE_NAME, opt, rname, hiveContext)
        dict_df["df_{0}".format(metric)] = dict_df["df_{0}".format(metric)]\
            .withColumnRenamed("submitter_hostgroup","submitter_hostgroup_{0}".format(metric))\
            .withColumnRenamed("num_entries","num_entries_{0}".format(metric))\
            .withColumnRenamed("min_ts","min_ts_{0}".format(metric))\
            .withColumnRenamed("max_ts","max_ts_{0}".format(metric))\
            .withColumnRenamed("type","type_{0}".format(metric))
    # Suffix metric's attributes defined in COMMON_VALUES with metric ID. 
    for metric in COMMON_VALUES.keys():
        print("metric is {0} and metric_list is {1}".format(metric, metric_list))
        if metric in metric_list:
            for val in COMMON_VALUES[metric]:
                print("inside for val is {0} metric is {1}".format(val,metric))
                maxval = "max_"+val
                newmaxval = maxval + "_{0}".format(metric)
                minval = "min_"+val
                newminval = minval + "_{0}".format(metric)
                avgval = "avg_"+val
                newavgval = avgval + "_{0}".format(metric)
                dict_df["df_{0}".format(metric)]=dict_df["df_{0}".format(metric)]\
                    .withColumnRenamed(maxval,newmaxval)\
                    .withColumnRenamed(minval,newminval)\
                    .withColumnRenamed(avgval,newavgval)
    for metric in metric_list:
        dict_df["df_{0}".format(metric)].printSchema()
    # grouping attributes of joined data
    list_aggregates = ['entity','hostname','project','group_interval']
    #df_result = dict_df["df_13504"].join(dict_df["df_13505"],list_aggregates)
    dict_df["df_result"] = dict_df["df_{0}".format(metric_list[0])]
    # join aggregated result of all metrics to form a single dataset
    for metric in metric_list[1:]:
        dict_df["df_result"] = dict_df["df_result"].join(dict_df["df_{0}".format(metric)], list_aggregates)
    if not opt["schema"]:
        #write df_result to file
        top_value = dict_df["df_result"]\
            .filter("hostname is not null and entity is not null")\
            .select("hostname", "entity").distinct()\
            .map(lambda row: ((row[0], row[1]), 1)).reduceByKey(add)\
            .map(lambda x: (x[0][0], (x[0][1], x[1]))).groupByKey().mapValues(len)\
            .map(lambda x: (x[1])).top(1)
        if len(top_value) == 0:
            print("ERROR: cannot calculate top value")
            sc.stop()
            exit(1)
        max_value = top_value[0]
        print("max_value is {0}".format(max_value))
        if max_value > 1:
            #write result to file with both entity and hostname
            print("don't drop entity")
            dict_df["df_result"].write.mode(opt["write_mode"]).json(rname)
        else:
            # There is one to one relationship between entity and hostname, hence drop entity and write resulted dataframe to file
            print("drop entity")
            dict_df["df_result"] = dict_df["df_result"].drop("entity")
            dict_df["df_result"].write.mode(opt["write_mode"]).json(rname)
        print("Result is in: {0}".format(rname))
    else:
        print("Resulted schema is:")
        dict_df["df_result"].printSchema()
