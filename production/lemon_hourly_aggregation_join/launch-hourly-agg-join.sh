#!/bin/bash - 
#===============================================================================
#
#          FILE: launch-hourly-agg-join.sh
# 
#         USAGE: ./launch-hourly-agg-join.sh 2016 09
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Luca Menichetti (lmeniche), luca.menichetti@cern.ch
#  ORGANIZATION: CERN
#       CREATED: 11/08/2016 16:54
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

cd $(dirname $0)

YYYY=$1
MM=$2

spark-submit --driver-class-path=/usr/lib/hive/lib/* --driver-java-options="-Dspark.executor.extraClassPath=/usr/lib/hive/lib/*" --master yarn-client lemon_hourly_aggregation_join.py -T -p /project/awg/lemon-aggregates -l WARN -o -m 13504,13505,13506,13507 /project/itmon/archive/lemon/cloud_compute/$YYYY-$MM
