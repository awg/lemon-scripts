#!/bin/bash - 
#===============================================================================
#
#          FILE: hourly-agg-target-month.sh
# 
#         USAGE: ./hourly-agg-target-month.sh YYYY-MM
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#     ARGUMENTS: ---
#  REQUIREMENTS: acrontab
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Luca Menichetti, Dheeraj Gupta
#  ORGANIZATION: CERN
#       CREATED: 14/12/2016 10:51
#      REVISION: ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

cd $(dirname $0)

TARGET_MONTH=${1-$(date +'%Y-%m')}
FAIL_LOGS=()

if [[ "$TARGET_MONTH" =~ 20[0-9][0-9]-[0-1][0-9] ]]; then
    
    # Read metrics and hostgroups
    mapfile -t METRICS < "$PWD/metrics.txt"
    mapfile -t HGS < "$PWD/hostgroups.txt"

    DATE=$(date +'%F')
    SECOND_SINCE_EPOCH_UTC=$(date +'%s')
    OUTPUT_LOGS_FOLDER="$PWD/spark_log"
    COMMIT_ID=$(git show -s | grep commit | awk '{print "Git commit id: "$2}') 2>&1

    # Jars and libs
    LIBS_FOLDER="$PWD/lib"
    SPARK_JOB="$LIBS_FOLDER/lemon_hourly_aggregation.py"

    # Variables for the SPARK job
    HDFS_OUTPUT_PREFIX="/project/awg/lemon-aggregates"
    LOGLEVEL="WARN"
    SPARK_OPTS='--driver-class-path=/usr/lib/hive/lib/* --driver-java-options="-Dspark.executor.extraClassPath=/usr/lib/hive/lib/*"'

    mkdir -p $OUTPUT_LOGS_FOLDER 2>/dev/null

    # Job parameters
    for hg in ${HGS[@]}; do

        HDFS_INPUT="/project/itmon/archive/lemon/$hg/$TARGET_MONTH"
        
        for metric in ${METRICS[@]}; do
            
            output_job_file="$OUTPUT_LOGS_FOLDER/lemon-agg-$hg-$metric-$TARGET_MONTH"
            echo $COMMIT_ID > $output_job_file 2>&1
            echo "Target month:" $TARGET_MONTH >> $output_job_file 2>&1
            echo "Execution timestamp: $SECOND_SINCE_EPOCH_UTC" >> $output_job_file 2>&1
            CMD="spark-submit $SPARK_OPTS $SPARK_JOB -T -p $HDFS_OUTPUT_PREFIX -l $LOGLEVEL -o "
            if [ "$metric" != "0" ]; then
                CMD=$CMD"-m $metric "
            fi
            CMD=$CMD"$HDFS_INPUT ";
            echo "COMMAND:" $CMD >> $output_job_file 2>&1
            $CMD >> $output_job_file 2>&1
            RET_CODE=$?
            if [ "$RET_CODE" != "0" ]; then
                FAIL_LOGS+=("ERROR while processing metric=$metric for hostgroup=$hg (period: $TARGET_MONTH), please check log file $output_job_file (ret cod $RET_CODE)")
            else
                # Command succeeded, move to new file
                # Get the output file name
                exp="Result is in: (([^ ]+)\.tmp)"
                if [[ `cat $output_job_file` =~ $exp ]]; then
                    orig_name="${BASH_REMATCH[1]}"
                    new_name="${BASH_REMATCH[2]}"
                    old_name=$new_name$".old"
                    if hdfs dfs -test -e $new_name >> $output_job_file 2>&1 ; then
                        # code for new_name exists
                        hdfs dfs -mv $new_name $old_name >> $output_job_file 2>&1
                        hdfs dfs -mv $orig_name $new_name >> $output_job_file 2>&1
                        hdfs dfs -rm -r $old_name >> $output_job_file 2>&1
                    else
                        # code for new_name does not exists
                        hdfs dfs -mv $orig_name $new_name >> $output_job_file 2>&1
                    fi
                fi
            fi
        done;
    done;
    
else
    FAIL_LOGS+=("Impossible to recognize target month to process: $TARGET_MONTH")
fi

EMAILS=$(cat subscribers.txt)
if [ ${#FAIL_LOGS[@]} -ne 0 ]; then
  (echo "Some of lemon hourly aggregations failed for target month $TARGET_MONTH." && echo "Here you can find a short errors list:" && for fl in ${FAIL_LOGS[@]}; do echo "$fl"; done;) | mail -s "ERROR: LeMon hourly aggregation $TARGET_MONTH" $EMAILS
fi