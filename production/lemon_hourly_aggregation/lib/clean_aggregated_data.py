# Python script which is used to clean the result of hourly aggregation
# and make it usable. This is a front-end to houlry_agg_util.py.
# It does two things
# 1. Picks one entry per entity, HG, group_interval, CUSTOM_ATTR
# 2. Adds an attribute group_interval_tstamp which is the unix timestamp
#    of group_interval (assumed to be in UTC)
#
# To be able to work for all metrics, it needs a list of extra grouping
# attributes (the same as supplied to lemon_hourly_aggregation.py)
# This script will not work for any metric that has a custom grouping
# attribute unless that metric's custom attributes are specified using
# the CUSTOM_ATTR dictionary in this file too.
#
# hourly_agg_util.py needs to be passed as --py-files to spark-submit and
# also added to the PYTHONPATH for this to work
# See all usage options by passing --help
import optparse
import sys

from pyspark import (HiveContext,
                     SparkConf,
                     SparkContext)

import hourly_agg_util as util


CUSTOM_ATTR = {"13183": ["DiskName"],
               "9208": ["InterfaceName"]}


def main(filename, metric_id, result_name, threshold=4, daily=False):
    APP_NAME = "Clean-Lemon-Aggs"
    # Initialize an empty conf so that spark-submit options can fill it
    conf = SparkConf()
    # But we set master explicilty
    conf = conf.setMaster("yarn-client").setAppName(APP_NAME)
    # Build the spark context
    sc = SparkContext(conf=conf)
    sc.setLogLevel("FATAL")
    hiveContext = HiveContext(sc)

    df = hiveContext.read.json(filename)
    custom_attr = CUSTOM_ATTR.get(str(metric_id), None)
    # Pick one line per grouping
    result = util.clean_data_for_hour(
        df, threshold=threshold, extra_grouping_attr=custom_attr,
        consider_daily=daily)
    # Add group_interval_tstamp to result
    result = util.augment_group_interval(result)
    result.write.json(result_name)


if __name__ == '__main__':
    parser = optparse.OptionParser(
        usage="%prog [-options] FILE")
    parser.add_option(
        "-m", "--metric-id", dest="metric_id", type="int",
        help=("Specify the metric ID for which the FILE has aggregated values."
              "This will be the same as supplied to lemon_hourly_aggregation"
              " which generated the FILE. If not specified, None is assumed"
              " which means this is a general time association, not tied"
              " to any metric"))
    parser.add_option("-t", "--threshold", type="int", dest="threshold",
                      default=4,
                      help=("Specify the minimum number of raw entries"
                            " in an hour so that raw aggregates are picked"
                            " over hourly aggregates. Default 4"))
    parser.add_option(
        "-r", "--result-name", dest="result_name",
        help="Specify the output file name. REQUIRED.")
    parser.add_option(
        "-d", "--use-daily", dest="daily", action="store_true",
        default=False,
        help=("Use daily aggregate valus for an hour if both raw and "
              "hourly are missing. Default False. Only specify this option"
              " for metrics that send very few raw or hourly entries"))
    opts, args = parser.parse_args()
    # Sanity checking of options and arguments
    if not opts.metric_id:
        metric_id = 0
    else:
        metric_id = opts.metric_id
    if not opts.result_name:
        print "Please specify an output HDFS file name"
        sys.exit(1)
    try:
        FILE_NAME = args[0]
    except IndexError:
        print("Please specify a file name")
        parser.print_help()
        sys.exit(1)
    if len(args) > 1:
        print("Please specify a single file name")
        sys.exit(1)
    main(FILE_NAME, metric_id, opts.result_name, threshold=opts.threshold,
         daily=opts.daily)
