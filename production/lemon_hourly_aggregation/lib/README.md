# LeMon Hourly aggregation

LeMon data can be aggregated per metric. Due to missing data-points, it is better
to aggregate the data by the hour and later use this to calculate more varied aggregations (day/week).

There are three files in this directory

1. [``lemon_hourly_aggregation.py``](#lemon_agg) - The spark job that performs the aggregation on lemon data. The result of this job is not directly usable as aggregates are performed per ``type``. Therefore, there may be one entry for ``raw``, one for ``daily`` and one for ``hourly`` aggregates.
In practice, only one of these entries need to be picked
2. [``hourly_agg_util.py``](#util) - Python library module that allows manipulation of aggregated results in order to make them usable. This is intended to be used from within Python scripts that need to work with lemon aggregated data.
3. [``clean_aggregated_data.py``](#clean) - A spark job intended as a front-end to ``hourly_agg_util.py``. It also makes the lemon aggregates usable and writes the result to a new file which can be read in other spark jobs.

## <a name="lemon_agg">``lemon_hourly_aggregation.py``</a>

This pyspark script takes the monthly LeMon data file as input. It also accepts a host of other options.
For full usage details, run

```bash
lemon_hourly_aggregation.py --help
```

The script reads all LeMon records from the data file and groups them by
``(entity,hostgroup,aggregated,date_hour,*METRIC_SPECIFIC_ATTR)``
It deduces the name of HG and the MONTH and YEAR from the input file name.
The result is written to a file whose name depends of METRIC, HG and YEAR_MONTH and ends in
``.tmp``. This result can be moved to a ``.tmp``-less name by using ``hdfs dfs -mv``
(For full information, read the help associated with ``-T`` option)

### Supported Metrics

This script supports many LeMon metrics. A list of all metrics supported can be printed
by passing the ``-L`` option

#### Adding new metrics

To add new metrics, the python script must be modified by adding the schema of the desired metric to the script. There are three dictionaries in the script that hold metric related values:
* ``BODY_SCHEMA`` - Holds the schema of the metric. Add the new metric ID (as string) as key and the schema (``StructType()``) as value to this dictionary. See existing schemas for examples. Any new metric MUST have an entry here or it won't be recognized by program.
* ``EXCLUDED_VALUES`` - Holds the names of attributes of body that must be excluded from aggregation for the metric. Add the new metric ID (as string) as key and a list of strings (attributes names) as values. If no values are to be excluded you can omit the key or add empty list ``[]``
* ``GROUPING_VALUES`` - Specify custom attributes that should also be part of grouping while aggregating. E.g. DiskName for disk plugin (aggregation should be made over disk name too so that there is one line per disk per entity). Add the new metric ID (as string) as key and a list of strings (attributes names) as values. If the metric does not need any special grouping attributes, don't add the key to this dictionary.

### Output

The script outputs JSON to HDFS. The schema of the output JSON depends on the metric for which aggregation is being performed.
However, irrespective of the metric, there are some common attributes in the output. These are:

```
root
 |-- entity: string (nullable = true)
 |-- submitter_hostgroup: string (nullable = true)
 |-- group_interval: string (nullable = true)
 |-- num_entries: long (nullable = false)
 |-- min_ts: long (nullable = true)
 |-- max_ts: long (nullable = true)
 |-- type: string (nullable = true)
```
where ``type`` can be one out of ``"raw"``, ``"hour"`` or ``"day"`` depending on what type
of LeMon records have been used to perform the aggregation.

There will be one line per ``entity``,``submitter_hostgroup``,``group_interval``, ``type``, ``CUSTOM_ATTR``
where ``CUSTOM_ATTR`` are some special attributes which are added for certain metrics

The schema of the result can be viewed by passing the ``-S`` option, which will not perform any aggregation
but just print the schema of the proposed output.

#### Output file name

The script will write result to a file ending in ``.tmp``. This can be renamed to actual file if the script runs correctly. This is to prevent job failures from erasing existing data when ``-o/--overwrite`` option is used.

By passing ``-T``, a tree like structure for output file can be enforced. Also by passing ``-p`` we can specify an HDFS path prefix to which output will be written.
Some examples of output file names for various input parameters are given here for clarity:

##### Using ``-p`` but no ``-T`` - Flat name in prefix HDFS path
```
$ spark-submit lemon_hourly_aggregation.py -m 13184 -p lemon_aggregates /project/itmon/archive/lemon/bi/2016-08
Result is in: lemon_aggregates/result_agg_13184_hourly_bi_2016_08.tmp
```

##### Using ``-T`` but no ``-p`` - Hierarchical name in current HDFS path
```
$ spark-submit lemon_hourly_aggregation.py -m 13184 /project/itmon/archive/lemon/bi/2016-08 -T
Result is in: ./bi/2016_08/hourly_13184.tmp
```

##### Using ``-T`` and ``-p`` - Hierarchical name in prefix HDFS path
```
$ spark-submit lemon_hourly_aggregation.py -T -p /project/awg/lemon-aggregates -m 13184 /project/itmon/archive/lemon/bi/2016-08
Result is in: /project/awg/lemon-aggregates/bi/2016_08/hourly_13184.tmp
```
### <a name="use_result"/>Using the result</a>

Since the ouput has one line per ``entity``,``submitter_hostgroup``,``group_interval``, ``type`` (and also ``CUSTOM_ATTR``
if applicable), it is recommended that the output data be "cleaned" before using it so that for every
``entity``,``submitter_hostgroup``,``group_interval`` (and also ``CUSTOM_ATTR`` if applicable) we pick only one
line of most appropriate ``type``.
For example consider the sample dataframe given below

```bash
+----------+--------------+----+---------------------------------+------------------+-----------+
|entity    |group_interval|type|submitter_hostgroup              |PercSystem        |num_entries|
+----------+--------------+----+---------------------------------+------------------+-----------+
|b6242eb0de|25-06-2016 00 |raw |bi/batch/gridworker/aishare/share|24.54             |12         |
|b6242eb0de|25-06-2016 00 |hour|bi/batch/gridworker/aishare/share|22.46             |1          |
|b6242eb0de|25-06-2016 00 |day |bi/batch/gridworker/aishare/share|18.00             |1          |
...
```
Only one entry of these three should be picked as they are for same entity, hostrgoup and interval.

To make such tasks simpler, a library module [``hourly_agg_util.py``](#hourly_agg_utilpy) is shipped.



## <a name="util">``hourly_agg_util.py``</a>

This is a utility module which can be used to make the process of using the hourly aggregation results
easier.

It has a number of functions which can directly be called for the read results dataframe. Each function in the
[module](hourly_agg_util.py) is documented inline.

### Functions

#### ``clean_data_for_hour``

This is the most basic function which is **expected to be called for every result** before the result
is put to use. It resolves the sort of conflicts shown [above](#use_result).
Calling functions can specify the threshold of ``num_entries`` that should occur in ``raw`` aggregates
for it to be chosen. If that threshold is not reached, ``hour`` aggregate will be chosen instead.
If ``hour`` does not occur, then ``raw`` will be chosen anyways.

#### ``augment_group_interval``

This function adds the UTC timestamp for group_interval to the dataframe. This is useful if you want to
sort or filter by timestamp. The timestamp corresponds to first second of group interval. So if ``group_interval="01-04-2016 00"``,
the timestamp will be calculate for datetime ``"01-04-2016 00:00:00"``

#### ``filter_by_group_interval``

Filter out rows in the dataframe that have group_interval greater or equal to ``start`` interval but *less* than ``end`` interval.

### Example Usage

A sample function that works on results of aggregation of swap plugin (9022)

```python
import hourly_agg_util as util

def process(df, threshold, start_date_hour, end_date_hour):
    res = util.clean_data_for_hour(df, threshold=threshold)
    res2 = util.filter_by_group_interval(res, start_date_hour, end_date_hour)
    res3 = res2.select(
        "entity", "submitter_hostgroup", "group_interval", "type",
        "group_interval_tstamp",
        res2.avg_AvgSwapReadsPerSec.alias("AvgSwapReadsPerSec"),
        res2.avg_AvgSwapWritesPerSec.alias("AvgSwapWritesPerSec"))
    return res3
```

To submit the spark job that uses the utility module, we need to pass the ``hourly_agg_util.py`` file
to spark-submit using ``--py-files hourly_agg_util.py`` and also add the **PATH** of this file to the
``PYTHONPATH`` so that it is correctly picked up.

```bash
$ export PYTHONPATH=$PYTHONPATH:path/to/production/lemon_hourly_aggregation/lib/
$ spark-submit --py-files path/to/production/lemon_hourly_aggregation/lib/hourly_agg_util.py sample_prog.py lemon_aggregates/bi/2016_06/hourly_9022 lemon_aggregates/bi/2016_07/hourly_9022
```

## <a name="clean">``clean_aggregated_data.py``</a>

This python script is like a front-end to the ``hourly_agg_util.py``. If you are not using Python but still need the cleaned data, you can
call this script and it will do the following for you:
1. Call the ``clean_data_for_hour`` so that your input lemon aggregated data becomes usable
2. Call the ``augment_group_interval`` function so that the cleaned data has group_interval timestamps to make it easier to sort
The result is written to a custom HDFS file which is specified during invokation.

The ``CUSTOM_ATTR`` dictionary must be updated everytime ``GROUPING_VALUES`` dictionary of ``lemon_hourly_aggregation.py`` is updated otherwise this script won't generate correct
results for the said metric.

### Usage

This script also relies on ``hourly_agg_util.py`` and therefore the usage instructions for that module are valid here too.
Broadly it means that you must add the location of ``hourly_agg_util.py`` to ``PYTHONPATH`` and pass ``hourly_agg_util.py``
to ``spark-submit`` using ``--py-files`` switch.

Please pass ``--help`` to the script to see example usage

```bash
$ PYTHONPATH=/afs/cern.ch/user/d/dgupta/lemon-scripts/production/lemon_hourly_aggregation/lib/ spark-submit --py-files=/afs/cern.ch/user/d/dgupta/lemon-scripts/production/lemon_hourly_aggregation/lib/hourly_agg_util.py clean_aggregated_data.py --help
```

### Example Usage

```bash
$ PYTHONPATH=/afs/cern.ch/user/d/dgupta/lemon-scripts/production/lemon_hourly_aggregation/lib/ spark-submit --py-files=/afs/cern.ch/user/d/dgupta/lemon-scripts/production/lemon_hourly_aggregation/lib/hourly_agg_util.py clean_aggregated_data.py -m 13183 lemon_aggregates/bi/2016_06/hourly_13183 -r cleaned_13183
```

```bash
$ PYTHONPATH=/afs/cern.ch/user/d/dgupta/lemon-scripts/production/lemon_hourly_aggregation/lib/ spark-submit --py-files=/afs/cern.ch/user/d/dgupta/lemon-scripts/production/lemon_hourly_aggregation/lib/hourly_agg_util.py clean_aggregated_data.py -m 13184 lemon_aggregates/bi/2016_06/hourly_13184 -r cleaned_13184
```
