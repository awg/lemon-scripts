#!/bin/bash -
#===============================================================================
#
#          FILE: hourly-agg-acrontab.sh
#
#         USAGE: ./hourly-agg-acrontab.sh
#
#   DESCRIPTION: acrontab script for performing hourly aggregation
#
#       OPTIONS: ---
#     ARGUMENTS: ---
#  REQUIREMENTS: acrontab
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Dheeraj Gupta (dgupta), dheeraj.gupta@cern.ch
#  ORGANIZATION: CERN
#       CREATED: 28/06/2016 16:15
#      REVISION: ---
#===============================================================================
# How many months besides the current month should the script be run for
PREV_MONTHS=0

cd $(dirname $0)

# Read metrics and hostgroups
mapfile -t METRICS < "$PWD/metrics.txt"
mapfile -t HGS < "$PWD/hostgroups.txt"

DATE=$(date +'%F')
SECOND_SINCE_EPOCH_UTC=$(date +'%s')

OUTPUT_LOGS_FOLDER="$PWD/spark_log"

# Jars and libs
LIBS_FOLDER="$PWD/lib"
SPARK_JOB="$LIBS_FOLDER/lemon_hourly_aggregation.py"

# Variables for the SPARK job
HDFS_OUTPUT_PREFIX="/project/awg/lemon-aggregates.tmp"
LOGLEVEL="WARN"
# be very careful with the following because the quotes can mess up how the spark-submit receives these.
SPARK_OPTS='--driver-class-path=/usr/lib/hive/lib/* --driver-java-options="-Dspark.executor.extraClassPath=/usr/lib/hive/lib/*" --master yarn-client'

mkdir -p $OUTPUT_LOGS_FOLDER
FAIL_LOGS=()
# Job parameters
for hg in ${HGS[@]}; do
  for metric in ${METRICS[@]}; do
    x=0;
    while [ "$x" -le "$PREV_MONTHS" ]; do
    MONTH_YEAR=$(date --date="$(date +%Y-%m-15) -$x month" +%Y-%m)
    HDFS_INPUT="/project/itmon/archive/lemon/$hg/$MONTH_YEAR"
      output_job_file="$OUTPUT_LOGS_FOLDER/lemon-agg-$hg-$metric-$MONTH_YEAR"
      # Writing commit and execution time into "output_job_file"
      git show -s | grep commit | awk '{print "Git commit id: "$2}' > $output_job_file 2>&1
      echo "Execution timestamp: $SECOND_SINCE_EPOCH_UTC" >> $output_job_file 2>&1
      # klist -f >> $output_job_file 2>&1
      # hadoop fs -ls >> $output_job_file 2>&1
      # We set the hive and pythonpath explicitly because otherwise job may fail
      CMD="spark-submit $SPARK_OPTS $SPARK_JOB -T -p $HDFS_OUTPUT_PREFIX -l $LOGLEVEL -o "
      if [ "$metric" != "0" ]; then
        CMD=$CMD"-m $metric "
      fi
      CMD=$CMD"$HDFS_INPUT ";
      #~ echo $CMD; echo;
      echo "COMMAND:" $CMD >> $output_job_file 2>&1
      $CMD >> $output_job_file 2>&1
      RET_CODE=$?
      if [ "$RET_CODE" != "0" ]; then
        FAIL_LOGS+=($output_job_file)
      else
        # Command succeeded, move to new file
        # Get the output file name
        exp="Result is in: (([^ ]+)\.tmp)"
        if [[ `cat $output_job_file` =~ $exp ]]; then
          orig_name="${BASH_REMATCH[1]}"
          new_name="${BASH_REMATCH[2]}"
          old_name=$new_name$".old"
          if hdfs dfs -test -e $new_name >> $output_job_file 2>&1 ; then
            # code for new_name exists
            hdfs dfs -mv $new_name $old_name >> $output_job_file 2>&1
            hdfs dfs -mv $orig_name $new_name >> $output_job_file 2>&1
            hdfs dfs -rm -r $old_name >> $output_job_file 2>&1
          else
            # code for new_name does not exists
            hdfs dfs -mv $orig_name $new_name >> $output_job_file 2>&1
          fi
        fi
      fi
      x=$((x=x+1))
    done;
  done;
done;

EMAILS=$(cat subscribers.txt)
if [ ${#FAIL_LOGS[@]} -ne 0 ]; then
  (echo "Some of lemon hourly aggregations failed" && echo "Please check the following log files:" && for fl in ${FAIL_LOGS[@]}; do echo "$fl"; done;) | mail -s "ERROR: LeMon hourly aggregation $DATE" $EMAILS
fi
