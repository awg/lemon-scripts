"""
Accept a series of hourly aggregated 13183 files and extract avg disk metrics.
If a time window is given, only metrics for that window are extracted.
Writes result to result_13183_UUID
Needs hourly_agg_util to be passed as --py-file to spark-submit
To do so you need to add the location of hourly_agg_util.py to PYTHONPATH
"""
import optparse
import sys
from uuid import uuid4

from pyspark import (HiveContext,
                     SparkConf,
                     SparkContext)

import hourly_agg_util as util


def process(df, opts):
    res = util.clean_data_for_hour(df, threshold=opts.threshold,
                                   extra_grouping_attr=["DiskName"])
    if opts.start_hour and opts.end_hour:
        res2 = util.filter_by_group_interval(res, opts.start_hour,
                                             opts.end_hour)
    else:
        res2 = util.augment_group_interval(res)
    # We won't average this per entity. Will retain the (entity,hostgroup) pair
    res3 = res2.select(
        "entity", "submitter_hostgroup", "group_interval", "type",
        "group_interval_tstamp",
        res2.DiskName.alias("diskname"),
        res2.avg_DiskReadRate.alias("DiskReadRate"),
        res2.avg_DiskWriteOpRate.alias("DiskWriteOpRate"),
        res2.avg_DiskWriteRate.alias("DiskWriteRate"),
        res2.avg_DiskWriteOpMergedRate.alias("DiskWriteOpMergedRate"),
        res2.avg_DiskReadOpMergedRate.alias("DiskReadOpMergedRate"),
        res2.avg_DiskReadOpRate.alias("DiskReadOpRate"))
    return res3


def main(filenames, output_file, opts):
    APP_NAME = "Process-13184-aggs"
    # Initialize an empty conf so that spark-submit options can fill it
    conf = SparkConf()
    # But we set master explicilty
    conf = conf.setMaster("yarn-client").setAppName(APP_NAME)
    # Build the spark context
    sc = SparkContext(conf=conf)
    sc.setLogLevel("FATAL")
    hiveContext = HiveContext(sc)
    # Read al files into diff dataframe and then union them
    fl_iter = iter(filenames)
    fname = fl_iter.next()
    df = hiveContext.read.json(fname)
    while True:
        try:
            fname2 = fl_iter.next()
        except StopIteration:
            break
        else:
            df2 = hiveContext.read.json(fname2)
            df = df.unionAll(df2)
    res = process(df, opts)
    res.write.json(output_file)
    print "Result is in {0}".format(output_file)


if __name__ == "__main__":
    parser = optparse.OptionParser(
        usage="%prog [options] INPUT_FILE [INPUT_FILE2]...")
    parser.add_option("-t", "--threshold", type="int", dest="threshold",
                      default=4,
                      help=("Specify the threshold to consider for"
                            " taking hour or raw aggregates. Default 4"))
    parser.add_option("-S", "--start-hour", dest="start_hour",
                      help="Specify start hour as DD-MM-YYYY HH. REQUIRED")
    parser.add_option("-E", "--end-hour", dest="end_hour",
                      help=("Specify end hour as DD-MM-YYYY HH. REQUIRED"
                            ". The filter will be < this hour and not <="))
    opts, args = parser.parse_args()
    # Sanity checking of options and arguments
    if len(args) < 1:
        print "Specify atleast one input file"
        parser.print_help()
        sys.exit(1)
    if ((opts.start_hour and not opts.end_hour)
        or (not opts.start_hour and opts.end_hour)):
        parser.print_help()
        sys.exit(1)
    RESULT_NAME = "result_13183_" + uuid4().get_hex()
    FILE_NAMES = args
    main(FILE_NAMES, RESULT_NAME, opts)

