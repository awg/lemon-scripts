# Notebooks for Lemon analysis

This folder has all the jupyter notebooks. The code here can be considered Beta.

## Executing 

The notebooks can be imported into any Jupyter + PySpark setup and executed directly. However notebooks tagged as ``Dependent on x`` need special handling

### Executing ``Depends on x`` notebooks

These notebooks use library code from a third party python file and hence need that python file/egg/module to be available in the Jupyter + PySpark setup.
To execute these notebooks, we ned to pass the relevant module to pyspark notebook at startup phase. This is done by adding the path to the module to ``PYTHONPATH`` and passing the python-file using ``--py-files`` switch. An example of startup script which passes a custom python file is

```bash
klist
if [ $? != 0 ]; then
        echo "ERROR: kinit first!"
        exit 1
fi

. /afs/cern.ch/sw/lcg/external/gcc/4.9/x86_64-slc6-gcc49-opt/setup.sh
. /afs/cern.ch/sw/lcg/app/releases/ROOT/6.04.14/x86_64-slc6-gcc49-opt/root/bin/thisroot.sh
PYTHONPATH=/root/lemon-scripts/production/lemon_hourly_aggregation/lib PYSPARK_PYTHON=/etc/spark/python PYSPARK_DRIVER_PYTHON=/opt/anaconda/bin/jupyter PYSPARK_DRIVER_PYTHON_OPTS='notebook --port=8888' pyspark --name "$(klist | grep "Default principal:" | awk -F": " '{print $2}' | awk -F"@" '{print $1}')'s Jupyter" --driver-class-path '/usr/lib/hive/lib/*' --driver-java-options '-Dspark.executor.extraClassPath=/usr/lib/hive/lib/*' --packages com.databricks:spark-csv_2.10:1.4.0 --py-files /root/lemon-scripts/production/lemon_hourly_aggregation/lib/hourly_agg_util.py
```
## Notebook List

* ``core_seconds_csv`` - Notebook which calculates an optimistic estimate of core-seconds in each service. It uses lemon data for 4111 and entity CSV to compute the numbers.
* ``CPU_Plots`` (Depends on ``hourly_agg_util.py``)  - Plot the CPU metrics vs date-hour for a given entity for a given lemon hourly aggregation output file
* ``Disk_Plots`` (Depends on ``hourly_agg_util.py``)  - Plot the disk metrics vs date-hour for a given entity for a given lemon hourly aggregation output file. All disks are plotted on the same plot
