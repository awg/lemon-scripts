SPARK SCRIPTS FOR LEMON DATA SCRAPPING
======================================

This is a repository of all the code for LeMon data scrapping.

All experimental code and ideas are in ``Beta/`` while all code that is deployable using acron (and the scripts to do so) is in ``production/``. ``Notebooks/`` has all the Jupyter notebooks.

How to execute a pyspark script
--------------------------------------

* Command
```bash
spark-submit --driver-class-path '/usr/lib/hive/lib/*' --driver-java-options '-Dspark.executor.extraClassPath=/usr/lib/hive/lib/*' <script_name>
```

* For enabling dynamic allocation in YARN
```bash
spark-submit --master yarn-client --conf spark.dynamicAllocation.enabled=true --conf spark.dynamicAllocation.minExecutors='1' --conf spark.dynamicAllocation.maxExecutors='64' --conf spark.dynamicAllocation.initialExecutors='1' --conf spark.shuffle.service.enabled=true --driver-class-path '/usr/lib/hive/lib/*' --driver-java-options '-Dspark.executor.extraClassPath=/usr/lib/hive/lib/* <script_name>'
```
* Scala files are not to be run directly run from shell. Commands have to be copied into spark-shell.
